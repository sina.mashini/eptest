import React, { FC, useState } from "react";
import Page from "$components/layout/Page";
import {
  LoadingData,
  ModalComponent,
  MessageComponent,
  RoundedButton,
} from "bp-components";
import { NextPage } from "next";
import { createWebVariables } from "$gqlQueryTypes/createWeb";
import CreateWebinarComponent from "$components/webinar/CreateWebinar";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
import store from "store";
import fetch from 'isomorphic-unfetch'


const CreateWebinar: NextPage<FC> = () => {
  const router = useRouter();
  const [successModal, setSuccessModal] = useState(false);
  const [message, setMessage] = useState("");
  const [type, setType] = useState<"success" | "error" | "warning" | "info">("success");
  const { t } = useTranslation();

  const buttons = (
    <>
      <RoundedButton
        variant="contained"
        color="primary"
        label={t("خانه")}
        onClick={() => {
          router.push("/");
          setSuccessModal(false);
        }}
      />
      <RoundedButton
        variant="contained"
        color="primary"
        label={t("وبینارهای من")}
        onClick={() => {
          //TODO create MyWebinars page
          alert("going to 'My Webinars' page");
          setSuccessModal(false);
        }}
      />
    </>
  );
  const [loading, setLoading] = useState<boolean>(false);

  const createWebinarMutation = async (webinarItems: createWebVariables) => {
    setLoading(true);
    await createWebinar({ items: webinarItems.items }).then(result =>{      
      if(result.createWebinars[0]._id)
        {         
          setType("success");
          setMessage(t("وبینار با موفقیت ثبت گردید و بعد از تایید مدیر منتشر خواهد شد"))
        }
      if(result.status === 401)
        {
          setType("error");
          setMessage(t("باید برای ثبت یا ویرایش وبینار ابتدا وارد وارد حساب کاربری خود شوید"));
        }
        setLoading(false);
        setSuccessModal(true);
    }).catch(()=>{
      setLoading(false);
      setType("error");
      setMessage(t("خطا در سرور"));
      setSuccessModal(true);
    }      
    )

  }

  return (
    <Page>
      <LoadingData loading={loading}>
        {() => {
          return (
            <>
              <ModalComponent open={successModal} closeModal={() => setSuccessModal(false)}>
                <MessageComponent
                  message={message}
                  buttons={buttons}
                  type={type}
                />
              </ModalComponent>
              <CreateWebinarComponent
                createWebinarMutation={createWebinarMutation}
              />
            </>
          );
        }}
      </LoadingData>
    </Page>
  );
};


const createWebinar = async (variables) =>
{
  let data;
  await fetch('/api/webinar/createWebinar',{
    method:"POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'authorization': store.get("token")
  },
  body: JSON.stringify({
    variables
  }),
  }).then(response => {
    data = response;
  })
  const res = await data.json();
  return res 
}

export default CreateWebinar;