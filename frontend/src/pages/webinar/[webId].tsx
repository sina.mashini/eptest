import React, { FC } from "react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import {
  VideoCards,
  VideoCardProps,
  WebinarDescSection,
  OtherFileCardProps,
  OtherFileCards,
  WebinarCardProps,
  WebinarCarousel,
  LoadingData,
} from "bp-components";
import Page from "$components/layout/Page";
import { useWebinarsIds } from "../../functions/getWebinarId";
import { withApollo } from "$withApollo";
import { GET_WEBINARS } from "$queries";
import { useQuery } from "@apollo/react-hooks";
import { getWebinars, getWebinarsVariables } from "$gqlQueryTypes/getWebinars";
import Link from "next/link";
import { useOrganiztionContext } from "../../context/OrganizationContext";

const useWebinar = (webinarId) => {
  const { loading, error, data } = useQuery<getWebinars, getWebinarsVariables>(
    GET_WEBINARS,
    {
      variables: {
        Ids: webinarId,
      },
    }
  );
  return { loading, error, data };
};

const renderWebinarLink = (children: JSX.Element, id: string) => {
  return (
    <Link href="/webinar/[webId]" as={`/webinar/${id}`}>
      {children}
    </Link>
  );
};

const renderPresenterLink = (children: JSX.Element, id: string) => {
  return (
    <Link href="/presenter/[presenterId]" as={`/presenter/${id}`}>
      {children}
    </Link>
  );
};

const filterVideosAndFiles = (attachments, loading) => {
  let files: OtherFileCardProps[] = [];
  let videos: VideoCardProps[] = [];
  let videosAndFiles = { files, videos };
  if (loading) return videosAndFiles;

  attachments.forEach((attachment) => {
    if (attachment.Kind === "video") {
      videos.push({
        desc: attachment.Title,
        duration: attachment.Duration,
        video: attachment.Media?._id,
      });
    } else {
      files.push({
        title: attachment.Title,
        type: attachment.format,
        src: attachment.Media?._id,
        image:
          attachment.Media?.blobType == "Image"
            ? attachment.Media?._id
            : attachment.Thumbnail,
      });
    }
  });

  return (videosAndFiles = { files, videos });
};

const useWebinars = (ids, idsIsLoading, currentWebId) => {
  let newIds: any = [];
  if (ids) {
    ids.map((id) => {
      if (id != currentWebId) newIds.push(id);
    });
  }
  const { loading, error, data } = useQuery<getWebinars, getWebinarsVariables>(
    GET_WEBINARS,
    {
      variables: {
        Ids: newIds,
      },
      skip: idsIsLoading,
    }
  );
  let webinars;
  if (loading === false) {
    webinars = createWebinarData(data?.getWebinars);
  }
  return { loading, error, data: webinars };
};

const createdescDataPresenter = (descData) => {
  let profileImage = "";
  let title = "";
  let affiliation = "";
  let presenterId = "";
  if (descData) {
    (profileImage = descData?.Presenters[0]?.Image?._id),
      (title = descData?.Presenters[0]?.title),
      (affiliation = descData?.Presenters[0]?.affiliation),
      (presenterId = descData?.Presenters[0]?._id);
  }
  return { profileImage, title, affiliation, presenterId };
};

const createWebinarData = (items) => {
  let webinars: WebinarCardProps[] = [];

  if (items) {
    webinars = items.map(function (item) {
      return {
        id: item._id,
        image: item.WebinarImage?._id,
        date: item.presentDate,
        endDate: item.presentEndDate,
        name: item.title,
        presenter: item.Presenters[0]?.title,
        presenterImage: item.Presenters[0]?.Image?._id,
        presenterId: item.Presenters[0]?._id,
        keywords: item.keywords,
        webinarLink: item.webinarLink,
        presenterLink: renderPresenterLink,
        link: renderWebinarLink,
      };
    });
  }
  return webinars;
};

const Webinar: NextPage<FC> = () => {
  const router = useRouter();
  const { organizationId } = useOrganiztionContext();
  const webinarMetaData = useWebinar(router.query.webId);
  const descData = webinarMetaData.data?.getWebinars[0];
  const descDatapresenter = createdescDataPresenter(descData);
  const attachments = filterVideosAndFiles(
    webinarMetaData.data?.getWebinars[0].Attachment,
    webinarMetaData.loading
  );

  const webinarIds = useWebinarsIds(organizationId);
  const webinars = useWebinars(
    webinarIds.ids,
    webinarIds.loading,
    router.query.webId
  );
  const loading =
    webinarIds.loading || webinarMetaData.loading || webinars.loading;

  return (
    <Page>
      <LoadingData loading={loading}>
        {() => {
          return (
            <>
              <WebinarDescSection
                title={descData?.title}
                image={descData?.WebinarImage?._id}
                prsenterImage={descDatapresenter.profileImage}
                prsenterName={descDatapresenter.title}
                prsenterEducation={descDatapresenter.affiliation}
                keywords={descData?.keywords}
                description={descData?.description}
                loading={webinarMetaData.loading}
                presenterLink={renderPresenterLink}
                startDate={descData?.presentDate}
                endDate={descData?.presentEndDate}
                presenterId={descDatapresenter.presenterId}
                webinarLink={descData?.webinarLink}
              />
              <VideoCards videos={attachments.videos} />
              <OtherFileCards files={attachments.files} />
              <WebinarCarousel Webinars={webinars.data} />
            </>
          );
        }}
      </LoadingData>
    </Page>
  );
};

export default withApollo(Webinar);
