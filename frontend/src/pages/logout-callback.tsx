import { useEffect, useState } from 'react';
import Router from 'next/router';
import { LoadingData } from "bp-components";
// import { includeDefaultNamespaces } from '../../i18n';
import {useOrganiztionContext} from "../context/OrganizationContext";
import createConfig from '../components/auth/util/config';

const LogoutCallback = () => {
  console.log("fgfffdsfsdfsdfsfgsgfd");
  const { SSOSettings } = useOrganiztionContext();
  const [loading] = useState(true);
  useEffect(() => {
    process();
  }, []);

  const process = () => {    
    const config = createConfig(SSOSettings);
    Router.push(config.homePage);
  };

  return <>{loading ? <LoadingData loading = {loading}> 
    {()=> {return (<></>)}}</LoadingData>: ''}</>;
};

// LogoutCallback.getInitialProps = () => {
//   return {
//     namespacesRequired: includeDefaultNamespaces(['front']),
//   };
// };

export default LogoutCallback;