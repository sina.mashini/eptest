import React, { FC } from "react";
import { NextPage } from "next";
import { withApollo } from "$withApollo";
import { GET_WEBINARS } from "$queries";
import Page from "$components/layout/Page";
import {
  WebinarCardProps,
  UpComingWebinarsCarousel,
  LoadingData,
} from "bp-components";
import Link from "next/link";
import { useWebinarsIds } from "../functions/getWebinarId";
import moment from "jalali-moment";
import { Scheduler } from "../components/Scheduler/Scheduler";
import { BannerComponent } from "../components/banner/Banner";
import { PresenterComponet } from "../components/presenter/Presenter";
import { useTranslation } from "react-i18next";
import { useOrganiztionContext } from "../context/OrganizationContext";
import { getWebinars, getWebinarsVariables } from "$gqlQueryTypes/getWebinars";
import { useQuery } from "@apollo/react-hooks";


const renderPresenterLink = (children: JSX.Element, id: string) => {
  return (
    <Link href="/presenter/[presenterId]" as={`/presenter/${id}`}>
      {children}
    </Link>
  );
};

const renderWebinarLink = (children: JSX.Element, id: string) => {
  return (
    <Link href="/webinar/[webId]" as={`/webinar/${id}`}>
      {children}
    </Link>
  );
};

const useWebinar = (ids, idsIsLoading) => {
  const { loading, error, data } = useQuery<getWebinars, getWebinarsVariables>(
    GET_WEBINARS,
    {
      variables: {
        Ids: ids,
      },
      skip: idsIsLoading,
    }
  );
  let webinars;
  if (loading == false) {
    webinars = createWebinarData(data?.getWebinars);
  }
  return { loading, error, data: webinars };
};

const createWebinarData = (items) => {
  let newWebinars: WebinarCardProps[] = [];
  let oldWebinars: WebinarCardProps[] = [];

  if (items) {
    items.forEach((item?) => {
      if (moment().isBefore(moment(item?.presentDate))) {
        newWebinars.push({
          id: item._id,
          image: item.WebinarImage?._id,
          date: item.presentDate,
          name: item.title,
          presenter: item.Presenters[0]?.title,
          presenterImage: item.Presenters[0]?.Image?._id,
          presenterId: item.Presenters[0]?._id,
          keywords: item.keywords,
          endDate: item.presentEndDate,
          presenterLink: renderPresenterLink,
          link: renderWebinarLink,
        });
      } else {
        oldWebinars.push({
          id: item._id,
          image: item.WebinarImage?._id,
          date: item.presentDate,
          name: item.title,
          presenter: item.Presenters[0]?.title,
          presenterImage: item.Presenters[0]?.Image?._id,
          presenterId: item.Presenters[0]?._id,
          keywords: item.keywords,
          endDate: item.presentEndDate,
          presenterLink: renderPresenterLink,
          link: renderWebinarLink,
        });
      }
    });
    return { oldWebinars, newWebinars };
  }
};

const Home: NextPage<FC> = () => {
  const { organizationId } = useOrganiztionContext();
  const webinarIds = useWebinarsIds(organizationId);
  const { t } = useTranslation();
  const webinars = useWebinar(webinarIds.ids, webinarIds.loading);
  const loading = webinarIds.loading || webinars.loading;
  return (
    <Page>
      <LoadingData loading={loading}>
        {() => {
          return (
            <>
              <BannerComponent />
              <Scheduler />
              <UpComingWebinarsCarousel
                Webinars={webinars.data.newWebinars}
                title={t("وبینارهای آینده")}
                color="#ededed"
              />
              <PresenterComponet />
              <UpComingWebinarsCarousel
                Webinars={webinars.data.oldWebinars}
                title={t("وبینارهای گذشته")}
                color="#ededed"
              />   
            </>
          );
        }}
      </LoadingData>
    </Page>
  );
};

export default withApollo(Home);
