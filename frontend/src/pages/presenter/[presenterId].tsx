import React, { FC } from "react";
import { NextPage } from "next";
import {
  PresenterDescSection,
  PresenterPageWebinars,
  WebinarCardProps,
} from "bp-components";
import { useRouter } from "next/router";
import Page from "$components/layout/Page";
import Link from "next/link";
import { withApollo } from "$withApollo";
import { GET_PRESENTER, GET_Ids, GET_WEBINARS } from "$queries";
import { useQuery } from "@apollo/react-hooks";
import {
  getPresenters,
  getPresentersVariables,
} from "$gqlQueryTypes/getPresenters";
import { getIds, getIdsVariables } from "$gqlQueryTypes/getIds";
import { useOrganiztionContext } from "../../context/OrganizationContext";
import { getWebinars, getWebinarsVariables } from "$gqlQueryTypes/getWebinars";

const renderWebinarLink = (children: JSX.Element, id: string) => {
  return (
    <Link href="/webinar/[webId]" as={`/webinar/${id}`}>
      {children}
    </Link>
  );
};

const useSpecificWebinar = (presentrId, orgId) => {
  let { data } = useQuery<getIds, getIdsVariables>(GET_Ids, {
    variables: {
      query: {
        bool: {
          must: [
            { match: { "Webinar.Presenters": presentrId } },
            { match: { "Webinar.Organization": orgId } },
            { match: { "schema": "Webinar" } },
            { match: { "Webinar.PublishStatus": 2 } }
          ]
        }
      },
      offset: 0,
      limit: 10,
    },
  });
  const ids = data?.search.items.map((item) => {
    return item._id;
  });
  return ids;
};

const usePresenter = (presenterId) => {
  const { loading, error, data } = useQuery<
    getPresenters,
    getPresentersVariables
  >(GET_PRESENTER, {
    variables: {
      PresenterIds: presenterId,
    },
  });

  return { loading, error, data };
};

const useWebinars = (id) => {
  const { data } = useQuery<getWebinars, getWebinarsVariables>(GET_WEBINARS, {
    variables: {
      Ids: id,
    },
  });

  let webinars = createWebinarData(data?.getWebinars);

  return { data: webinars };
};

const createWebinarData = (items) => {
  let webinars: WebinarCardProps[] = [];
  if (items) {
    webinars = items.map(function (item) {
      return {
        id: item._id,
        image: item.WebinarImage?._id,
        date: item.presentDate,
        name: item.title,
        presenter: item.Presenters[0].title,
        presenterImage: item.Presenters[0].Image?._id,
        keywords: item.keywords,
        link: renderWebinarLink,
      };
    });
  }
  return webinars;
};

const Presenter: NextPage<FC> = () => {
  const { organizationId } = useOrganiztionContext();
  const router = useRouter();
  const presenterMetaData = usePresenter(router.query.presenterId);
  const descData = presenterMetaData.data?.getPresenters[0];
  const webinarId = useSpecificWebinar(
    router.query.presenterId,
    organizationId
  );
  const webinars = useWebinars(webinarId);

  return (
    <Page>
      <PresenterDescSection
        prsenterUniversity={descData?.affiliation}
        prsenterImage={descData?.Image?._id}
        prsenterName={descData?.title}
        prsenterEducation={descData?.fieldOfStudy}
        description={descData?.biography}
      />
      <PresenterPageWebinars webinars={webinars.data} />
    </Page>
  );
};

export default withApollo(Presenter);
