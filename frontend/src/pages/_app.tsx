import React from "react";
import App from "next/app";
import "typeface-roboto";
import "$components/layout/global.css";
import "@iin/typeface-iran-sans";
import "@iin/typeface-iran";
import "@iin/typeface-iran-kharazmi";
import "@iin/typeface-iran-rounded";
import "@iin/typeface-iran-sharp";
import "@iin/typeface-iran-yekan";
import { AuthProvider } from "../context/AuthContext";
import { ApolloProvider } from "@apollo/react-hooks";
import { getApolloClient } from "../lib/apollo/getApolloClient";
import I18nProvider from '../context/I18Provider';
import TransformContext from '../context/TransformContext'
import { OrganizationProvider } from "../context/OrganizationContext";


export default class MyApp extends App {
  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentNode!.removeChild(jssStyles);
    }
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <ApolloProvider client={getApolloClient({})}>
        <OrganizationProvider organization = {this.props.pageProps.server}>
              <I18nProvider>
                  <AuthProvider>
                    <TransformContext>
                      <Component {...pageProps} />
                    </TransformContext>
                  </AuthProvider>
             </I18nProvider>
        </OrganizationProvider>
      </ApolloProvider>
    );
  }
}

MyApp.getInitialProps = async (appContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  let appProps = await App.getInitialProps(appContext);
  const protocol = process.env.NODE_ENV === 'production' ? 'https' : 'http';
  const apiUrl = process.browser
    ? `${protocol}://${window.location.host}`
    : `${protocol}://${appContext.ctx.req?.headers.host}`;
  appProps.pageProps.server = apiUrl;
  return { ...appProps };
};
