import { GraphQLClient } from 'graphql-request';
import store from 'store';


const createVariables = (domain)=>
{
    return{ 
        query: {
          bool: {
            must: [
              {
                match: { "Organization.Domain": domain }
              }
            ]
          }
        }
      }
}

const query = `
  query($query:JSON!){
    search(query:$query, limit:10, offset:0)
    {
      items{
        ... on Organization{
          Administrators{
            Email
          }
        }
      }
    }
  }
`


export const isAdmin = async (domain: string) => 
{ 
  const org = (process.env.NODE_ENV === "development") ? "edupack.netlify.app": domain;
  const client = new GraphQLClient(`${process.env.SERVER_ADDRESS_GRAPGQL}`);
  const variables = createVariables(org);
  const result = await client.request(query, variables);
  const email = store.get("email");
  let isAdmin = false;
  result.search.items.map((item) => {
    item.Administrators.map(admin =>{
      if( email === admin.Email )
        isAdmin = true
        return;
    })    
  })
  return isAdmin; 
}

export default isAdmin