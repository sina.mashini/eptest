import {NextApiHandler, NextApiRequest, NextApiResponse} from 'next';
import { verify } from 'jsonwebtoken'
import store from 'store';


const isAuthenticated = (fn: NextApiHandler) => async (
    req: NextApiRequest,
    res: NextApiResponse
) => {    
   const secret = publicKeyDetection(req.headers.origin);// to check the realm and get the public key
   verify(req.headers.authorization!, secret, {algorithms: ['RS256'] }, async function(err, decoded) {    
        if(!err && decoded) {
          saveUser(decoded);
          return await fn(req, res);
        }        
        removeUser();
        res.status(401).json({message: "Sorry you are not authenticated!"})
      });
};


const saveUser = (user) =>
{
   store.set('email', user.email);
   store.set('name' , user.given_name);
   store.set('family', user.given_name)
   store.set('username', user.preferred_username);
}  

const removeUser = () =>
{
  store.remove('name');
  store.remove('family')
  store.remove('email');
  store.remove('username');
}

const publicKeyDetection= (org) =>
{
    // this config strongly depends on the realm
    let secret = '';
    switch (org) {
        case "http://localhost:4000":
            secret = process.env.ACCOUNTREALMSECRET!
        case "https://edupack.netlify.app":
            secret = process.env.ACCOUNTREALMSECRET!
        case "https://edupack.vercel.app":
            secret = process.env.ACCOUNTREALMSECRET!
        case "https://edupack.avidcloud.io":
            secret = process.env.ACCOUNTREALMSECRET!
        default:
            secret = process.env.ACCOUNTREALMSECRET!
        }
    const result =
    `-----BEGIN PUBLIC KEY-----
` +
secret+`
-----END PUBLIC KEY-----`;
    return result;
}

export default isAuthenticated;