import { GraphQLClient } from 'graphql-request';
import {NextApiRequest, NextApiResponse} from 'next';
import isAuthenticated  from  '../auth/isAuthenticated';

export const createWebinar = async (
  req: NextApiRequest, 
  res: NextApiResponse) => {
  const client = new GraphQLClient(`${process.env.SERVER_ADDRESS_GRAPGQL}`);
  const variables = req.body.variables;
  const result = await client.request(query, variables);
  return res.status(200).json(result); 
}

const query = `
  mutation createWeb($items: [CreateWebinars_WebinarItems!]!) {
    createWebinars(items: $items) {
      _id
      title
      Presenters {
        _id
        title
      }
      parentId
      presentDate
      presentEndDate
      description
      Organization {
        _id
      }
      WebinarImage {
        _id
      }
      Attachment {
        Title
        Media {
          _id
        }
      }
    }
  }
`;

export default isAuthenticated(createWebinar);