import React, { FC } from "react";
import Page from "$components/layout/Page";
import { withApollo } from "$withApollo";
import { NextPage } from "next";
import AdminDashboard from "$components/admin/AdminDashboard";

const Dashboard: NextPage<FC> = () => {

  return (
    <Page>
        <AdminDashboard />
    </Page>
  );
};

export default withApollo(Dashboard);