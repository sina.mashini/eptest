import React, { useEffect } from "react";
import i18n from "i18next";
import { I18nextProvider } from "react-i18next";
import Locize from "i18next-locize-backend";
import Cache from "i18next-localstorage-cache";
import {useOrganiztionContext} from "./OrganizationContext";
// import setting from "../util/language/config";

const I18nProvider: React.FunctionComponent = (props) => {
  const { defaultlang } = useOrganiztionContext();

  useEffect(() => {
    const language = defaultlang;

    i18n
      .use(Locize)
      .use(Cache)
      .init({
        lng: language, // default lang
        debug: false,
        fallbackLng: language,
        ns: ["edupack"], // our namespsce
        defaultNS: "edupack", // our namespsce
        saveMissing: false,
        initImmediate: false,
        keySeparator: false,
        backend: {
          loadPath: "/static/locales/{{lng}}/{{ns}}.json",
          //  projectId: 'aa3af940-fb30-410a-a2e6-94c887e24fd0',
          //  apiKey: '809727fe-ebe0-461b-9036-6066d841ed71',
          //  referenceLng: defaultLanguage
        },
        react: {
          useSuspense: false,
        },
      });
  }, [defaultlang]);
  return <I18nextProvider i18n={i18n}>{props.children}</I18nextProvider>;
};

export default I18nProvider;
