import React, { createContext, useState } from 'react';
const defaultContext: any = {
 authAlert: false,
 setAuthAlert: () => {},
 addCartAlert: undefined,
 setAddCartAlert: () => {},
 checkoutAlert: undefined,
 setCheckoutAlert: () => {},
};
const AlertContext = createContext(defaultContext);
export default AlertContext;
export const AlertProvider = (props: any) => {
 const [authAlert, setAuthAlert] = useState<boolean>(defaultContext.authAlert);
 const [addCartAlert, setAddCartAlert] = useState<string>(defaultContext.addCartAlert);
 const [checkoutAlert, setCheckoutAlert] = useState<string>(
 defaultContext.addCartAlert,
 );
 return (
 <AlertContext.Provider
 value={{
 authAlert,
 setAuthAlert,
 addCartAlert,
 setAddCartAlert,
 checkoutAlert,
 setCheckoutAlert,
 }}
 >
 {props.children}
 </AlertContext.Provider>
 );
};