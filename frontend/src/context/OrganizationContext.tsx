import React, { createContext, useState, useContext, useEffect } from "react";
import { LoadingData, RtlProvider } from "bp-components";
import { getOrganizationId } from "../functions/getOrganizationId";
import { getOrganizationData } from "../functions/getOrganizationData";
import { ThemeProvider as SCThemeProvider } from "styled-components";
import muiTheme , { jss } from "$components/theme/muiTheme";
import { Jss } from 'jss';
import { StylesProvider } from '@material-ui/styles';
import { CssBaseline } from '@material-ui/core';
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import store from "store";

interface organizationConfig {
  organizationId: string;
  title: string; 
  Languages: any;
  Logo: string;
  SSOSettings: any;
  Favicon: string;
  defaultlang: string;
  defaultDir: string;
  changeLanguage: (language: string, dir: string) => any,
}

let OrganizationContext = createContext<organizationConfig | undefined>(undefined)

export default OrganizationContext;

export const createOrganization = (params) => {
  OrganizationContext = createContext(params);
};

export const createOrgData = (orgData) => {
  let organiztion:organizationConfig | any  = {}
  if (orgData) {
   organiztion = {
    organizationId: orgData._id,
    title: orgData.title,
    Languages: orgData.Languages,
    Logo: orgData.Logo,
    Favicon: orgData.Favicon,
    SSOSettings: orgData.SSOSettings
    }
   
  }
  return organiztion;
};

export const OrganizationProvider = (props: any) => {  
  const [defaultlang, setDefaultlang] = useState("fa");
  const [defaultDir, setDefaultDir] = useState("rtl");
  const org = (process.env.NODE_ENV ===  "development") ? "edupack.netlify.app" : props.organization;
  
  const orgId = getOrganizationId(org);
  const orgData = getOrganizationData(orgId.id, orgId.loading);
  const { organizationId, title, Languages, Logo, Favicon, SSOSettings} = createOrgData(orgData.data);

  useEffect(() => {
    if(Languages){
      let defaultLang = Languages[0].Locale;
      let defaultDir =  Languages[0].Direction;
      if(store.get("defaultLang") && store.get("defaultDir"))
      {
        defaultLang = store.get("defaultLang");
        defaultDir = store.get("defaultDir");
      }
      setDefaultlang(defaultLang) 
      setDefaultDir(defaultDir)  
    }
   },[Languages])

  const changeLanguage = (language: string, dir: string) => {
    const lang = Languages.find((item) => {
      return item.Locale === language;
    });
    if (lang) {
      store.set("defaultLang", language);
      store.set("defaultDir", dir);
      setDefaultDir(dir);
      setDefaultlang(language);
    }
  };

  return (
    <body dir={defaultDir}>
       <MuiThemeProvider theme ={muiTheme}>
        <SCThemeProvider theme ={muiTheme}>
      <OrganizationContext.Provider
        value={{
          organizationId,
          title,
          Languages,
          Logo,
          Favicon,
          SSOSettings,
          changeLanguage,
          defaultlang,
          defaultDir
        }}>
         <CssBaseline />
         <LoadingData loading={orgId.loading || orgData.loading}>
           {() => {
             return (<StylesProvider jss={jss as Jss}>
               <RtlProvider direction={defaultDir}>{props.children}</RtlProvider>
               </StylesProvider>)             
           }}
         </LoadingData>
       </OrganizationContext.Provider>
       </SCThemeProvider>
        </MuiThemeProvider>
    </body>
  );
};

export const useOrganiztionContext = () =>
{
  const context = useContext(OrganizationContext)
  if (context === undefined) {
    throw new Error('OrganizationContext must be used within a OrganizationContext.Provider')
  }
  return context;
}
