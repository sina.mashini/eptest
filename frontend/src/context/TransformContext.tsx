import React, {FC} from 'react';
import { createStoryBookContext } from "bp-components";
import { useTranslation } from 'react-i18next';
import { useOrganiztionContext } from './OrganizationContext'

export const TransformContext: FC = (props) => {
  const { Languages, changeLanguage, defaultlang, defaultDir} = useOrganiztionContext();
  createStoryBookContext({translate: useTranslation, changeLang: changeLanguage, Languages, defaultlang, defaultDir});
  
   return (
       <div>
          {props.children}
       </div>
   )
}

export default TransformContext;