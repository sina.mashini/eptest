export const storageKeyPrefix = 'store.';
export default {
 serverUrl: process.env.SERVER_URL,
 apiUrl: `${process.env.SERVER_URL}/api`,
 appEnv: process.env.NODE_ENV,
 appPort: process.env.APP_PORT,
 defaultLanguage: 'en',
 storage: {
 prefix: storageKeyPrefix,
 orgKey: storageKeyPrefix + 'organization',
 settingKey: storageKeyPrefix + 'setting',
 },
};