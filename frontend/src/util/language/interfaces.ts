export interface IOrganization 
{
    Title:string;
    Languages: ILanguage[];
    Domian: string;
    FavIcon: string;
    SSOSetting: any;
    Logo: string;
}

export interface ILanguage{
    name: string;
    direction: string;
} 