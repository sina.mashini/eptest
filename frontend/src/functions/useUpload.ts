import { useState } from "react";
import { keyBy, values } from "lodash";
import { useApolloClient } from "@apollo/react-hooks";
import {
  getBlobUploadSignedUrl,
  getBlobUploadSignedUrlVariables,
} from "$gqlQueryTypes/getBlobUploadSignedUrl";
import { GET_Blob_Upload_Signed_Url } from "$queries";
interface Progress {
  total: number;
  loaded: number;
}

const uploadFileToStorage = (
  url: string,
  data: any,
  onProgress: (progress: Progress) => void
): Promise<void> => {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.upload.onprogress = ({ total, loaded }) => {
      onProgress({ total, loaded });
    };
    xhr.onload = () => {
      if (xhr.status !== 204) {
        reject(xhr.responseText);
      } else {
        resolve();
      }
    };
    xhr.send(data);
  });
};

interface ImageData {
  data?: String | ArrayBuffer | null;
  name: string;
  size: number;
  type: string;
  error?: string;
  _id?: string;
}


const getFileInfo = (file): Promise<ImageData> => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = (e) => {
      const { name, size, type } = file;
      resolve({
        name,
        size,
        type,
        data: e.target?.result,
      });
    };
    reader.onerror = (err) => reject(err);
    reader.readAsDataURL(file);
  });
};

export const useUpload = (collectionId: string) => {
  
  const [progress, setProgress] = useState(0);

  const onProgress = (progress: Progress) => {
    setProgress(progress.loaded / progress.total * 100);
  };
  
  const [filesData, setFilesData] = useState<Record<string, any>>({});

  const client = useApolloClient();

  const uploadFiles = async (files) => {
    setProgress(0);
    if (!files) {
      return;
    }

    const getFilesPromises = values(files).map((file: ImageData) =>
      getFileInfo(file).catch((err) => err)
    );

    const filesInfo = await Promise.all(getFilesPromises);
    
    setFilesData(keyBy(filesInfo, "name"));

    const uploadSignedURLs = await client.query<
      getBlobUploadSignedUrl,
      getBlobUploadSignedUrlVariables
    >({
      query: GET_Blob_Upload_Signed_Url,
      variables: {
        blobinfo: filesInfo.map((fileInfo) => ({
          fileName: fileInfo.name,
          fileSize: fileInfo.size,
          mimeType: fileInfo.type,
        })),
        collectionId,
      },
      errorPolicy: "all"
    });
    
    for (const uploadSignedURL of uploadSignedURLs.data
      .getBlobUploadSignedUrl) {
      // TODO: Set error on the file if exist
      const file = values(files).find(
        (file) => file.name === uploadSignedURL.fileName
      );

      const formData = new FormData();

      for (let [key, value] of Object.entries(uploadSignedURL.formFields)) {
        const myValue: any = value;
        formData.append(key, myValue);
      }

      formData.append("file", file);


      const url = uploadSignedURL.url ? uploadSignedURL.url : "";
      // Upload file to BlobStorage
      // TODO: Create and pass onProgres
      uploadFileToStorage(url, formData, onProgress)
        .then(() => {
          // Read _id from the uploadSignedURL and set it to fileData      
          setFilesData((curFilesData) => ({
            ...curFilesData,
            [uploadSignedURL.fileName]: {
              ...curFilesData[uploadSignedURL.fileName],
              _id: uploadSignedURL._id
            }
          }));
        })
        .catch((err) => {
          // Set {_id: null, error: err} to the fileData
          setFilesData((curFilesData) => ({
            ...curFilesData,
            [uploadSignedURL.fileName]: {
              ...curFilesData[uploadSignedURL.fileName],
              _id: null,
              error: err
            }
          }));
        });
    }
  };
    
  return {
    filesData,
    uploadFiles,
    progress
  };
};
