import { GET_ORGANIZATION } from "$queries";
import {
  getOrganization,
  getOrganizationVariables,
} from "$gqlQueryTypes/getOrganization";
import { useQuery } from "@apollo/react-hooks";

export const getOrganizationData = (organizationId, idsIsLoading) => {
  const { loading, error, data } = useQuery<
    getOrganization,
    getOrganizationVariables
  >(GET_ORGANIZATION, {
    variables: {
      OrgIds: organizationId,
    },
    skip: idsIsLoading,
  });
  let orgData;
  if (loading === false && data) {
    orgData = data?.getOrganizations[0];
  }

  return { loading, error, data: orgData };
};
