import { GET_Ids } from "$queries";
import { getIds, getIdsVariables } from "$gqlQueryTypes/getIds";
import { useQuery } from "@apollo/react-hooks";

export const getOrganizationId = (hostName) => {
  let { loading, error, data } = useQuery<getIds, getIdsVariables>(GET_Ids, {
    variables: {
      query: {
        bool: {
          must: [
            {
              match: { "Organization.Domain": hostName },
            },
          ],
        },
      },
      offset: 0,
      limit: 10,
    },
  });
  const id = data?.search.items[0]._id;
  return { loading, error, id };
};
