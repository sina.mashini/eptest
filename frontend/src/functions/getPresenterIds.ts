import { getIds, getIdsVariables } from "$gqlQueryTypes/getIds";
import { useQuery } from "@apollo/react-hooks";
import { GET_Ids } from "$queries";

export const usePresentersIds = (organizationId) => {
  let { loading, error, data } = useQuery<getIds, getIdsVariables>(GET_Ids, {
    variables: {
      query: {
        bool: {
          must: [
            {
              match: { "Presenter.Organization": organizationId },
            },
          ],
          filter: [
            {
              term: { "schema.keyword": "Presenter" },
            },
          ],
        },
      },
      offset: 0,
      limit: 10,
    },
  });

  const ids = data?.search.items.map((item) => {
    return item._id;
  });

  return { loading, error, ids };
};
