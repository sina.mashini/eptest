import { getIds, getIdsVariables } from "$gqlQueryTypes/getIds";
import { GET_Ids } from "$queries";
import { useQuery } from "@apollo/react-hooks";

export const useWebinarsIds = (organizationId) => {
  let { loading, error, data } = useQuery<getIds, getIdsVariables>(GET_Ids, {
    variables: {
      query: {
        bool: {
          must: [
            { match: { "Webinar.Organization": organizationId } },
            { match: { "Webinar.PublishStatus": 2 } },
            { match: { "schema": "Webinar" } }
          ]
        }
      },
      offset: 0,
      limit: 20,
    },
  });

  const ids = data?.search.items.map((item) => {
    return item._id;
  });
  return { loading, error, ids };
};
