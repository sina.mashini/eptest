import { getIds, getIdsVariables } from "$gqlQueryTypes/getIds";
import { GET_Ids } from "$queries";
import { useQuery } from "@apollo/react-hooks";

export const useGenralServicesIds = (organizationId) => {
  let { loading, error, data } = useQuery<getIds, getIdsVariables>(GET_Ids, {
    variables: {
      query: {
        bool: {
          must: [
            {
              match: { "General.Organization": organizationId },
            },
          ],
          filter: [
            {
              term: { "schema.keyword": "General" },
            },
          ],
        },
      },
      offset: 0,
      limit: 10,
    },
  });
  const ids = data?.search.items.map((item) => {
    return item._id;
  });
  return { loading, error, ids };
};
