/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreatePresenters_PresenterItems } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: createPresenters
// ====================================================

export interface createPresenters_createPresenters_Image {
  __typename: "Blob";
  _id: GraphQLObjectId;
}

export interface createPresenters_createPresenters {
  __typename: "Presenter";
  title: string;
  _id: GraphQLObjectId;
  fieldOfStudy: string | null;
  affiliation: string | null;
  biography: string | null;
  Image: createPresenters_createPresenters_Image | null;
}

export interface createPresenters {
  /**
   * Create items of type Presenter
   */
  createPresenters: createPresenters_createPresenters[];
}

export interface createPresentersVariables {
  items: CreatePresenters_PresenterItems[];
}
