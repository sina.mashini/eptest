/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UploadBlobInfoInput, BlobTypes } from "./globalTypes";

// ====================================================
// GraphQL query operation: getBlobUploadSignedUrl
// ====================================================

export interface getBlobUploadSignedUrl_getBlobUploadSignedUrl {
  __typename: "UploadBlobInfo";
  _id: GraphQLObjectId | null;
  fileName: string;
  blobType: BlobTypes | null;
  url: string | null;
  formFields: GraphQLJSON | null;
}

export interface getBlobUploadSignedUrl {
  getBlobUploadSignedUrl: getBlobUploadSignedUrl_getBlobUploadSignedUrl[];
}

export interface getBlobUploadSignedUrlVariables {
  blobinfo: UploadBlobInfoInput[];
  collectionId: GraphQLObjectId;
}
