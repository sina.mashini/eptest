/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getOrganization
// ====================================================

export interface getOrganization_getOrganizations_Languages {
  __typename: "Language";
  title: string;
  Direction: string;
  Locale: string | null;
}

export interface getOrganization_getOrganizations_SSOSettings {
  __typename: "Organization_SSOSettings";
  Url: string;
  Realm: string;
  ClientId: string;
}

export interface getOrganization_getOrganizations {
  __typename: "Organization";
  title: string;
  _id: GraphQLObjectId;
  Languages: getOrganization_getOrganizations_Languages[];
  Domain: string;
  Logo: string;
  Favicon: string;
  SSOSettings: getOrganization_getOrganizations_SSOSettings | null;
}

export interface getOrganization {
  /**
   * Get all items of type Organization by id
   */
  getOrganizations: getOrganization_getOrganizations[];
}

export interface getOrganizationVariables {
  OrgIds: GraphQLObjectId[];
}
