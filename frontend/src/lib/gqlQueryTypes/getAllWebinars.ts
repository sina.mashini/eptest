/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getAllWebinars
// ====================================================

export interface getAllWebinars_search_items_Collection {
  __typename: "Collection" | "Organization" | "Language" | "Users" | "Presenter" | "General" | "RootCollection" | "_BaseItem";
}

export interface getAllWebinars_search_items_Webinar_WebinarImage {
  __typename: "Blob";
  _id: GraphQLObjectId;
}

export interface getAllWebinars_search_items_Webinar_Attachment_Media {
  __typename: "Blob";
  _id: GraphQLObjectId;
}

export interface getAllWebinars_search_items_Webinar_Attachment {
  __typename: "Webinar_Attachment";
  Media: getAllWebinars_search_items_Webinar_Attachment_Media | null;
  Title: string | null;
}

export interface getAllWebinars_search_items_Webinar_Presenters_Image {
  __typename: "Blob";
  _id: GraphQLObjectId;
}

export interface getAllWebinars_search_items_Webinar_Presenters {
  __typename: "Presenter";
  title: string;
  _id: GraphQLObjectId;
  Image: getAllWebinars_search_items_Webinar_Presenters_Image | null;
}

export interface getAllWebinars_search_items_Webinar_WebinarOwner {
  __typename: "Users";
  title: string;
  _id: GraphQLObjectId;
  Name: string;
  Family: string;
  UserId: string | null;
  Email: string;
}

export interface getAllWebinars_search_items_Webinar {
  __typename: "Webinar";
  title: string;
  _id: GraphQLObjectId;
  WebinarImage: getAllWebinars_search_items_Webinar_WebinarImage | null;
  keywords: string[] | null;
  presentDate: GraphQLDateTime | null;
  presentEndDate: GraphQLDateTime | null;
  Attachment: getAllWebinars_search_items_Webinar_Attachment[] | null;
  description: string | null;
  webinarLink: string | null;
  Presenters: getAllWebinars_search_items_Webinar_Presenters[] | null;
  WebinarOwner: getAllWebinars_search_items_Webinar_WebinarOwner | null;
  PublishStatus: number | null;
}

export type getAllWebinars_search_items = getAllWebinars_search_items_Collection | getAllWebinars_search_items_Webinar;

export interface getAllWebinars_search {
  __typename: "SearchResults";
  items: getAllWebinars_search_items[];
}

export interface getAllWebinars {
  search: getAllWebinars_search;
}

export interface getAllWebinarsVariables {
  query: GraphQLJSON;
}
