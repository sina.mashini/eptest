/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum BlobTypes {
  Any = "Any",
  Archive = "Archive",
  Audio = "Audio",
  EPUB = "EPUB",
  Image = "Image",
  PDF = "PDF",
  Presentation = "Presentation",
  SpreadSheet = "SpreadSheet",
  Text = "Text",
  Video = "Video",
  WordDocument = "WordDocument",
}

export interface CreatePresenters_PresenterItems {
  title: string;
  thumbnail?: string | null;
  parentId: GraphQLObjectId;
  fieldOfStudy?: string | null;
  affiliation?: string | null;
  biography?: string | null;
  profileImage?: string | null;
  Organization?: GraphQLObjectId | null;
  Image?: GraphQLObjectId | null;
}

export interface CreateWebinars_WebinarItems {
  title: string;
  WebinarImage?: GraphQLObjectId | null;
  thumbnail?: string | null;
  parentId: GraphQLObjectId;
  keywords?: string[] | null;
  presentDate?: GraphQLDateTime | null;
  presentEndDate?: GraphQLDateTime | null;
  coverImageAddress?: string | null;
  Attachment?: CreateWebinars_Webinar_AttachmentItems[] | null;
  description?: string | null;
  webinarLink?: string | null;
  Organization?: GraphQLObjectId | null;
  Presenters?: GraphQLObjectId[] | null;
  PublishStatus?: number | null;
  WebinarOwner?: GraphQLObjectId | null;
}

/**
 * Videos and files for webinar
 */
export interface CreateWebinars_Webinar_AttachmentItems {
  Title?: string | null;
  URL?: string | null;
  Thumbnail?: string | null;
  Iframe?: CreateWebinars_Webinar_Attachment_IframeItems | null;
  Description?: string | null;
  Kind?: string | null;
  Duration?: string | null;
  format?: string | null;
  Media?: GraphQLObjectId | null;
}

/**
 * Ifram for attachment
 */
export interface CreateWebinars_Webinar_Attachment_IframeItems {
  Source?: string | null;
  Width?: string | null;
  Height?: string | null;
}

export interface UploadBlobInfoInput {
  fileName: string;
  fileSize: number;
  mimeType: string;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
