/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreateWebinars_WebinarItems } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: createWeb
// ====================================================

export interface createWeb_createWebinars_Presenters {
  __typename: "Presenter";
  _id: GraphQLObjectId;
  title: string;
}

export interface createWeb_createWebinars_Organization {
  __typename: "Organization";
  _id: GraphQLObjectId;
}

export interface createWeb_createWebinars_WebinarImage {
  __typename: "Blob";
  _id: GraphQLObjectId;
}

export interface createWeb_createWebinars_Attachment_Media {
  __typename: "Blob";
  _id: GraphQLObjectId;
}

export interface createWeb_createWebinars_Attachment {
  __typename: "Webinar_Attachment";
  Title: string | null;
  Media: createWeb_createWebinars_Attachment_Media | null;
}

export interface createWeb_createWebinars {
  __typename: "Webinar";
  _id: GraphQLObjectId;
  title: string;
  Presenters: createWeb_createWebinars_Presenters[] | null;
  parentId: GraphQLObjectId;
  presentDate: GraphQLDateTime | null;
  presentEndDate: GraphQLDateTime | null;
  description: string | null;
  Organization: createWeb_createWebinars_Organization | null;
  WebinarImage: createWeb_createWebinars_WebinarImage | null;
  Attachment: createWeb_createWebinars_Attachment[] | null;
}

export interface createWeb {
  /**
   * Create items of type Webinar
   */
  createWebinars: createWeb_createWebinars[];
}

export interface createWebVariables {
  items: CreateWebinars_WebinarItems[];
}
