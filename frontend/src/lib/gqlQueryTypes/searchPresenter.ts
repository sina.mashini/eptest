/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: searchPresenter
// ====================================================

export interface searchPresenter_search_items_Collection {
  __typename: "Collection" | "Webinar" | "Organization" | "Language" | "Users" | "General" | "RootCollection" | "_BaseItem";
}

export interface searchPresenter_search_items_Presenter_Image {
  __typename: "Blob";
  _id: GraphQLObjectId;
  fileName: string;
}

export interface searchPresenter_search_items_Presenter {
  __typename: "Presenter";
  title: string;
  _id: GraphQLObjectId;
  Image: searchPresenter_search_items_Presenter_Image | null;
}

export type searchPresenter_search_items = searchPresenter_search_items_Collection | searchPresenter_search_items_Presenter;

export interface searchPresenter_search {
  __typename: "SearchResults";
  items: searchPresenter_search_items[];
}

export interface searchPresenter {
  search: searchPresenter_search;
}

export interface searchPresenterVariables {
  query: GraphQLJSON;
}
