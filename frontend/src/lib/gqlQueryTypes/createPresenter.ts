/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreatePresenters_PresenterItems } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: createPresenter
// ====================================================

export interface createPresenter_createPresenters {
  __typename: "Presenter";
  title: string;
  _id: GraphQLObjectId;
  fieldOfStudy: string | null;
  affiliation: string | null;
  biography: string | null;
  profileImage: string | null;
}

export interface createPresenter {
  /**
   * Create items of type Presenter
   */
  createPresenters: createPresenter_createPresenters[];
}

export interface createPresenterVariables {
  items: CreatePresenters_PresenterItems[];
}
