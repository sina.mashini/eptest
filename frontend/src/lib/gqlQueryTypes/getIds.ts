/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getIds
// ====================================================

export interface getIds_search_items_schema {
  __typename: "Schema";
  name: string;
}

export interface getIds_search_items {
  __typename: "Collection" | "Webinar" | "Organization" | "Language" | "Users" | "Presenter" | "General" | "RootCollection" | "_BaseItem";
  _id: GraphQLObjectId;
  title: string;
  schema: getIds_search_items_schema;
}

export interface getIds_search {
  __typename: "SearchResults";
  total: number;
  items: getIds_search_items[];
}

export interface getIds {
  search: getIds_search;
}

export interface getIdsVariables {
  query: GraphQLJSON;
  limit: number;
  offset: number;
}
