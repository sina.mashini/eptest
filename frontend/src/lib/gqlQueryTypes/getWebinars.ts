/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { BlobTypes } from "./globalTypes";

// ====================================================
// GraphQL query operation: getWebinars
// ====================================================

export interface getWebinars_getWebinars_WebinarImage {
  __typename: "Blob";
  _id: GraphQLObjectId;
}

export interface getWebinars_getWebinars_Attachment_Media {
  __typename: "Blob";
  _id: GraphQLObjectId;
  blobType: BlobTypes;
}

export interface getWebinars_getWebinars_Attachment_Iframe {
  __typename: "Webinar_Attachment_Iframe";
  Source: string | null;
  Width: string | null;
  Height: string | null;
}

export interface getWebinars_getWebinars_Attachment {
  __typename: "Webinar_Attachment";
  URL: string | null;
  Title: string | null;
  Kind: string | null;
  Duration: string | null;
  Thumbnail: string | null;
  format: string | null;
  Media: getWebinars_getWebinars_Attachment_Media | null;
  Iframe: getWebinars_getWebinars_Attachment_Iframe | null;
}

export interface getWebinars_getWebinars_Presenters_Image {
  __typename: "Blob";
  _id: GraphQLObjectId;
}

export interface getWebinars_getWebinars_Presenters {
  __typename: "Presenter";
  _id: GraphQLObjectId;
  title: string;
  Image: getWebinars_getWebinars_Presenters_Image | null;
  affiliation: string | null;
}

export interface getWebinars_getWebinars {
  __typename: "Webinar";
  title: string;
  _id: GraphQLObjectId;
  keywords: string[] | null;
  thumbnail: string | null;
  presentDate: GraphQLDateTime | null;
  presentEndDate: GraphQLDateTime | null;
  description: string | null;
  WebinarImage: getWebinars_getWebinars_WebinarImage | null;
  webinarLink: string | null;
  Attachment: getWebinars_getWebinars_Attachment[] | null;
  Presenters: getWebinars_getWebinars_Presenters[] | null;
  PublishStatus: number | null;
}

export interface getWebinars {
  /**
   * Get all items of type Webinar by id
   */
  getWebinars: getWebinars_getWebinars[];
}

export interface getWebinarsVariables {
  Ids: GraphQLObjectId[];
}
