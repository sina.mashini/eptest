import gql from "graphql-tag";

export const GET_WEBINARS = gql`
  query getWebinars($Ids: [ObjectId!]!) {
    getWebinars(ids: $Ids) {
      title
      _id
      keywords
      thumbnail
      presentDate
      presentEndDate
      description
      WebinarImage {
        _id
      }
      webinarLink
      Attachment {
        URL
        Title
        Kind
        Duration
        Thumbnail
        format
        Media {
          _id
          blobType
        }
        Iframe {
          Source
          Width
          Height
        }
      }
      Presenters {
        _id
        title
        Image {
          _id
        }
        affiliation
      }
      PublishStatus
    }
  }
`;
