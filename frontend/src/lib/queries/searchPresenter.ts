import gql from "graphql-tag";

export const SEARCH_PRESENTER = gql`
  query searchPresenter($query: JSON!) {
    search(
      query: $query,
      limit: 10,
      offset: 0
    ) {
      items {
        ... on Presenter {
          title
          _id
          Image {
            _id
            fileName
          }
        }
      }
    }
  }
`;



