import gql from "graphql-tag";

export const GET_ALL_WEBINARS = gql`
query getAllWebinars($query: JSON!) {
    search(query: $query, offset:0, limit: 50) {
      items {
        ... on Webinar {
          title
          _id
          WebinarImage {
            _id
          }
          keywords
          presentDate
          presentEndDate
          Attachment {
            Media {
              _id
            }
            Title
          }
          description
          webinarLink
          Presenters {
            title
            _id
            Image {
              _id
            }
          }
          WebinarOwner {
            title
            _id
            Name
            Family
            UserId
            Email
          }
          PublishStatus
        }
      }
    }
  }
`;