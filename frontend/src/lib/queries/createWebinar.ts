import gql from "graphql-tag";

export const CREATE_WEBINAR = gql`
  mutation createWeb($items: [CreateWebinars_WebinarItems!]!) {
    createWebinars(items: $items) {
      _id
      title
      Presenters {
        _id
        title
      }
      parentId
      presentDate
      presentEndDate
      description
      Organization {
        _id
      }
      WebinarImage {
        _id
      }
      Attachment {
        Title
        Media {
          _id
        }
      }
    }
  }
`;
