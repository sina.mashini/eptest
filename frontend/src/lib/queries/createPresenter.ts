import gql from "graphql-tag";

export const CREATE_PRESENTER = gql`
  mutation createPresenters($items: [CreatePresenters_PresenterItems!]!) {
    createPresenters(items: $items) {
      title
      _id
      fieldOfStudy
      affiliation
      biography
      Image {
        _id
      }
    }
  }
`;
