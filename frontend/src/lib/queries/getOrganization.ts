import gql from "graphql-tag";

export const GET_ORGANIZATION = gql`
  query getOrganization($OrgIds: [ObjectId!]!) {
    getOrganizations(ids: $OrgIds) {
      title
      _id
      Languages {
        title
        Direction
        Locale
      }
      Domain
      Logo
      Favicon
      SSOSettings {
        Url
        Realm
        ClientId
      }
    }
  }
`;
