import gql from "graphql-tag";

export const GET_Blob_Upload_Signed_Url = gql`
  query getBlobUploadSignedUrl(
    $blobinfo: [UploadBlobInfoInput!]!
    $collectionId: ObjectId!
  ) {
    getBlobUploadSignedUrl(blobsInfo: $blobinfo, collectionId: $collectionId) {
      _id
      fileName
      blobType
      url
      formFields
    }
  }
`;
