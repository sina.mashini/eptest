import gql from "graphql-tag";

export const GET_Ids = gql`
  query getIds($query: JSON!, $limit: Int!, $offset: Int!) {
    search(query: $query, limit: $limit, offset: $offset) {
      total
      items {
        _id
        title
        schema {
          name
        }
      }
    }
  }
`;
