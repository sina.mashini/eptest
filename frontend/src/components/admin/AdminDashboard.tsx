import React, { FC, useState, useEffect } from "react";
import styled from "styled-components";
import {
  DesctopDrawer,
  CollapseItem,
  MobileDrawer,
  PanelTitle,
  SpecificWebinars,
  LoadingData,
} from "bp-components";
import ComputerIcon from "@material-ui/icons/Computer";
import { ListItem, ListItemIcon, ListItemText, Box } from "@material-ui/core";
import PersonalVideoIcon from "@material-ui/icons/PersonalVideo";
import RemoveFromQueueIcon from "@material-ui/icons/RemoveFromQueue";
import CancelPresentationIcon from "@material-ui/icons/CancelPresentation";
import OndemandVideoIcon from "@material-ui/icons/OndemandVideo";
import { useQuery } from "@apollo/react-hooks";
import {
  getAllWebinars,
  getAllWebinarsVariables,
} from "$gqlQueryTypes/getAllWebinars";
import { GET_ALL_WEBINARS } from "$queries";
import { useOrganiztionContext } from "../../context/OrganizationContext";
import { Webinar } from "bp-components/src/AdminPanel/interfaces";
import Link from "next/link";
import { useTranslation } from "react-i18next";

const Wrapper = styled.div`
  display: flex;
  padding-top: 80px;
  height: 100%;
  flex-direction: column;
`;
const Content = styled(Box)`
  display: flex;
  overflow: hidden;
`;

interface AdminDashboardProps {}

const useGetAllWebinars = (orgId: string) => {
  let { loading, error, data } = useQuery<
    getAllWebinars,
    getAllWebinarsVariables
  >(GET_ALL_WEBINARS, {
    variables: {
      query: {
        bool: {
          must: [
            {
              match: {
                schema: "Webinar",
              },
            },
            {
              match: {
                "Webinar.Organization": orgId,
              },
            },
          ],
        },
      },
    },
  });
  let webinarData;
  if (data?.search.items) {
    webinarData = data?.search.items.map((item: any) => {
      return {
        title: item.title,
        _id: item._id,
        WebinarImage: item.WebinarImage?._id,
        keywords: item.keywords,
        presentDate: item.presentDate,
        presentEndDate: item.presentEndDate,
        Attachment: item.Attachment,
        description: item.description,
        webinarLink: item.webinarLink,
        Presenters: item.Presenters,
        WebinarOwner: item.WebinarOwner,
        PublishStatus: item.PublishStatus,
      };
    });
  }
  return { loading, error, webinarData };
};

const drawerItems = (
  openItems: boolean,
  collapseItems: JSX.Element[],
  handleClick: () => void
) => {
  return [
    <CollapseItem
      title="وبینارها"
      icon={<ComputerIcon />}
      open={openItems}
      items={collapseItems}
      handleClick={handleClick}
    />,
  ];
};

const renderWebinarLink = (children: JSX.Element, id: string) => {
  return (
    <Link href="/webinar/[webinarId]" as={`/webinar/${id}`}>
      {children}
    </Link>
  );
};

const AdminDashboard: FC<AdminDashboardProps> = (props) => {
  const [openCollapseItem, setopenCollapseItem] = useState(false);
  const [openMobileDrawer, setOpenMobileDrawer] = useState(false);

  const { organizationId } = useOrganiztionContext();
  const {t} = useTranslation();
  
  //all webinars data
  const queryData = useGetAllWebinars(organizationId);
  const allWebinarsData = queryData.webinarData;
  const queryLoading = queryData.loading;
  
  //state for webinar based on publish status
  const [webinarsList, setWebinarsList] = useState<Array<Webinar>>([]);
  
  const showCollapseItems = () => {
    setopenCollapseItem(!openCollapseItem);
  };

  const closeMobileDrawer = () => {
    setOpenMobileDrawer(false);
  };

  useEffect(() => {
    if(!queryLoading) {
      setWebinarsList(allWebinarsData)
    }
  }, [queryLoading]);

  const allWebinars = (data: Webinar[]) => {
    setWebinarsList(data);
  };

  //function for separate webinars based on publish status
  const separateWebinarsFunc = (data: Webinar[], publishStatus: number) => {
    let separatedWebinar: Webinar[] = [];
    data.map((item) => {
      if (item.PublishStatus === publishStatus) {
        separatedWebinar.push(item);
      }
    });
    setWebinarsList(separatedWebinar);
  };

  //items of webinar based on publish status
  const collapseItems = () => {
    return [
      <ListItem button onClick={() => allWebinars(allWebinarsData)}>
        <ListItemIcon>
          <PersonalVideoIcon />
        </ListItemIcon>
        <ListItemText primary={t("همه وبینارها")} />
      </ListItem>,
      <ListItem button onClick={() => separateWebinarsFunc(allWebinarsData, 2)}>
        <ListItemIcon>
          <OndemandVideoIcon />
        </ListItemIcon>
        <ListItemText primary={t("وبینارهای منتشر شده")} />
      </ListItem>,
      <ListItem button onClick={() => separateWebinarsFunc(allWebinarsData, 0)}>
        <ListItemIcon>
          <RemoveFromQueueIcon />
        </ListItemIcon>
        <ListItemText primary={t("وبینارهای در صف انتشار")} />
      </ListItem>,
      <ListItem button onClick={() => separateWebinarsFunc(allWebinarsData, 1)}>
        <ListItemIcon>
          <CancelPresentationIcon />
        </ListItemIcon>
        <ListItemText primary={t("وبینارهای منتشر نشده")} />
      </ListItem>,
    ];
  };

  return (
    <LoadingData loading={queryLoading}>
      {() => {
        return (
          <Wrapper>
            <PanelTitle setOpenMobileDrawer={setOpenMobileDrawer} />
            <Content>
              <DesctopDrawer
                items={drawerItems(
                  openCollapseItem,
                  collapseItems(),
                  showCollapseItems
                )}
              />
              <SpecificWebinars specificWebinarList={webinarsList} link={renderWebinarLink} />
            </Content>
            <MobileDrawer
              open={openMobileDrawer}
              onClose={closeMobileDrawer}
              items={drawerItems(
                openCollapseItem,
                collapseItems(),
                showCollapseItems
              )}
            />
          </Wrapper>
        );
      }}
    </LoadingData>
  );
};

export default AdminDashboard;
