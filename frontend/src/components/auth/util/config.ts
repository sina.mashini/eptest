const createConfig = (ssoConfig) => {
  return {
    defaultLoginRedirectUri: "/index",
    defaultCookieMaxAge: 30 * 24 * 60 * 60,
    homePage: "/index",
    privatePages: ["profile", "webinar/create"],
    keycloakConfig: {
      url: ssoConfig.Url,
      realm: ssoConfig.Realm,
      clientId: ssoConfig.ClientId,
    },
  };
};

export default createConfig;
