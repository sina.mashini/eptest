import { createMuiTheme } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import { create, Plugin, Jss } from 'jss';
import rtl from 'jss-rtl';
import {
  jssPreset,
  StylesProvider,
  ThemeProvider as MThemeProvider,
} from '@material-ui/styles';
import { CssBaseline } from '@material-ui/core';
import { themeOptions } from 'bp-components';

const jss = create({ plugins: [...jssPreset().plugins, rtl()] as Plugin[] });

interface IThemeProviderProps {
//   locale: string;
  direction: 'ltr' | 'rtl';
}

export const ThemeProvider: React.FunctionComponent<IThemeProviderProps> = (props) => {
  useEffect(() => {
    document.body.setAttribute('dir', props.direction);
  }, [props.direction]);

  const theme = createMuiTheme({ ...themeOptions, direction: props.direction });
  return (
    <MThemeProvider theme={theme}>
      <CssBaseline />
      <StylesProvider jss={jss as Jss}>{props.children}</StylesProvider>
    </MThemeProvider>
  );
};
