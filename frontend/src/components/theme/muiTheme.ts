import { createMuiTheme } from '@material-ui/core/styles';
import { create, Plugin } from 'jss';
import rtl from 'jss-rtl';
import { themeOptions } from 'bp-components';
import { jssPreset } from '@material-ui/styles';

export default createMuiTheme({
  ...themeOptions,
  direction: 'rtl', 
});

export const jss = create({ plugins: [...jssPreset().plugins, rtl()] as Plugin[] });
