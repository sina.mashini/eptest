import { Dispatch, SetStateAction } from "react";
import { Presenter, AttachmentFiles } from "bp-components";
import { ImageData } from "bp-components/src/interfaces";
import { createWebVariables } from "$gqlQueryTypes/createWeb";

export interface CreateWebinarProps {
  createWebinarMutation: (items: createWebVariables) => void;
}

export interface PresentersProps {
  uploadHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
  progress: number;
  setUploadState: Dispatch<SetStateAction<string>>;
  presenterImage: ImageData[];
  presenters?: Presenter[];
  setPresenters: Dispatch<SetStateAction<Array<Presenter> | undefined>>;
  error: boolean;
  setError: Dispatch<SetStateAction<boolean>>;
}

export interface AttachmentsProps {
  uploadHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
  media: ImageData[];
  setUploadState: Dispatch<SetStateAction<string>>;
  setAttachmentHandler: (media: ImageData[], title:string, description:string, showMedia: boolean) => void;
  attachments: AttachmentFiles[];
  openModal: boolean;
  setOpenModal: Dispatch<SetStateAction<boolean>>;
  error: boolean;
  progress: number;
  deleteAttachedMedia: (index: number) => void;
}
