import React, { FC, useState, useEffect } from "react";
import styled from "styled-components";
import { Box } from "@material-ui/core";
import {
  BaseInfoComponent,
  PaperItems,
  DatePickerComponent,
  CreateWebinarPaperComponent,
  WebinarRoomComponent,
  Presenter,
  ImageData,
  AttachmentFiles,
} from "bp-components";
import Presenters from "./partials/Presenters";
import { useUpload } from "../../functions/useUpload";
import { values } from "lodash";
import Attachments from "./partials/Attachments";
import { CreateWebinarProps } from "./interfaces";
import { createWebVariables } from "$gqlQueryTypes/createWeb";
import { CreateWebinarBanner } from "../banner/CreateWebinarBanner";
import { useOrganiztionContext } from "../../context/OrganizationContext";
import getBlobDuration from "get-blob-duration";
import { useTranslation } from "react-i18next";

const Wrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  margin-top: 80px;
`;

const blobDuration = async (blob) => {
  const duration = await getBlobDuration(blob);
  return secondsToTime(duration);
};

const secondsToTime = (secs) => {
  const hours = Math.floor(secs / (60 * 60));

  const divisor_for_minutes = secs % (60 * 60);
  const minutes = Math.floor(divisor_for_minutes / 60);

  const divisor_for_seconds = divisor_for_minutes % 60;
  const seconds = Math.ceil(divisor_for_seconds);
  const time = `${hours}:${minutes}:${seconds}`;
  return time;
};

const CreateWebinarComponent: FC<CreateWebinarProps> = (props) => {
  const { createWebinarMutation } = props;
  const { organizationId } = useOrganiztionContext();
  const { t } = useTranslation();
  //states of step one
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [tagInput, setTagInput] = useState("");
  const [tags, setTags] = useState<Array<string>>([]);
  const [webinarBanner, setWebinarBanner] = useState<Array<ImageData>>([]);
  const [summeryFile, setSummeryFile] = useState<Array<ImageData>>([]);

  //state of step two
  const [presenters, setPresenters] = useState<Array<Presenter>>();
  const [presenterImage, setPresenterImage] = useState<Array<ImageData>>([]);

  //states of step three
  const [startDate, setStartDate] = useState<Date>(new Date());
  const [endDate, setEndDate] = useState<Date>(new Date());

  //state for step four
  const [media, setMedia] = useState<Array<ImageData>>([]);
  const [attachments, setAttachments] = useState<AttachmentFiles[]>([]);
  const [openModal, setOpenModal] = useState(false);
  const [attachError, setAttachError] = useState(false);

  const [link, setLink] = useState("");

  const [activeStep, setActiveStep] = useState(0);
  const [error, setError] = useState(false);
  const [uploadState, setUploadState] = useState("");

  //states for upload
  const collectionId =
    uploadState === "PresenterImage"
      ? "5eb6b1ab923c300008351d9d"
      : "5ea559b3222115000aa8c02c";

  const { uploadFiles, filesData, progress } = useUpload(collectionId);
  const [uploadedFileName, setUploadedFileName] = useState<string[]>([]);

  //function for upload files
  const uploadHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setMedia([]);
    uploadFiles(event.target.files);
    const fileNames = values(event.target.files).map((file) => file.name);
    setUploadedFileName(fileNames);
  };

  // function for set attachmented file
  const setAttachmentHandler = (
    media: ImageData[],
    title,
    description,
    showMedia
  ) => {
    if (!showMedia) {
      //it means user did not upload anything or deleted the upload file
      setMedia([]);
    }
    if (title && description && media[0]._id && showMedia && progress === 100) {
      let duration: any = null;
      if (media[0].type.indexOf("video") === 0) {
        blobDuration(media[0].data).then((result) => {
          duration = result;
          setAttachmentData(media, title, description, duration);
        });
      } else {
        setAttachmentData(media, title, description);
      }
    } else {
      setAttachError(true);
    }
  };

  // function for seting the data of images and videos separately
  const setAttachmentData = (
    media: ImageData[],
    title,
    description,
    duration?
  ) => {
    const file: AttachmentFiles = {
      title: title,
      description: description,
      kind: media[0].type.substring(0, media[0].type.indexOf("/")),
      format: media[0].type.substring(
        media[0].type.indexOf("/") + 1,
        media[0].type.length
      ),
      media: media[0]._id,
      duration: duration,
      data: media[0].data,
    };
    const newArray: AttachmentFiles[] = Object.assign([], attachments);
    newArray.push(file);
    setAttachments(newArray);
    setOpenModal(false);
    setMedia([]);
  };

  //delete attached file
  const deleteAttachedMedia = (index: number) => {
    let newArray: AttachmentFiles[] = Object.assign([], attachments);
    newArray.splice(index, 1);
    setAttachments(newArray);
  };

  // upload files based on upload state
  useEffect(() => {
    if (filesData) {
      const fileUploaded = uploadedFileName.map((name) => filesData[name]);
      if (progress === 100) {
        if (uploadState === "webinarBanner") {
          setWebinarBanner(fileUploaded);
          setError(false);
        } else if (uploadState === "PresenterImage") {
          setPresenterImage(fileUploaded);
        } else if (uploadState === "summeryFile") {
          setSummeryFile(fileUploaded);
        } else if (uploadState === "attachments") {
          setMedia(fileUploaded);
        }
      }
    }
    // setProgress(0);
    // setUploadedFileName([]);
  }, [filesData, progress]);

  //steps title
  const steps = (translate: any) => {
    return [
      translate("اطلاعات پایه"),
      translate("اطلاعات ارائه دهنده"),
      translate("انتخاب تاریخ"),
      translate("بارگذاری مدیا"),
      translate("مکان وبینار"),
    ];
  };

  // function for set step one items
  const setBaseInfoItems = (
    name: "title" | "description" | "tags",
    value: string
  ) => {
    if (name === "title") {
      setTitle(value);
    } else if (name === "description") {
      setDescription(value);
    } else if (name === "tags") {
      setTagInput(value);
    }
    setError(false);
  };

  const addTag = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.charCode == 13) {
      const newTags = Object.assign([], tags);
      newTags.push(tagInput);
      setTags(newTags);
      setTagInput("");
    }
  };

  //delete specidied tag
  const deleteTag = (index: number) => {
    const newTags = Object.assign([], tags);
    newTags.splice(index, 1);
    setTags(newTags);
  };

  //components of each step
  const stepComponent = (stepIndex: number) => {
    switch (stepIndex) {
      case 0:
        return (
          <BaseInfoComponent
            title={title}
            description={description}
            tags={tags}
            tagInput={tagInput}
            setBaseInfoItems={setBaseInfoItems}
            uploadHandler={uploadHandler}
            setUploadState={setUploadState}
            webinarBanner={webinarBanner}
            summeryFile={summeryFile}
            progress={progress}
            error={error}
            addTag={addTag}
            deleteTag={deleteTag}
          />
        );
      case 1:
        return (
          <Presenters
            uploadHandler={uploadHandler}
            progress={progress}
            setUploadState={setUploadState}
            presenterImage={presenterImage}
            presenters={presenters}
            setPresenters={setPresenters}
            error={error}
            setError={setError}
          />
        );
      case 2:
        return (
          <DatePickerComponent
            startDate={startDate}
            setStartDate={setStartDate}
            endDate={endDate}
            setEndDate={setEndDate}
            error={error}
            setError={setError}
          />
        );
      case 3:
        return (
          <Attachments
            uploadHandler={uploadHandler}
            media={media}
            setUploadState={setUploadState}
            setAttachmentHandler={setAttachmentHandler}
            attachments={attachments}
            openModal={openModal}
            progress={progress}
            setOpenModal={setOpenModal}
            error={attachError}
            deleteAttachedMedia={deleteAttachedMedia}
          />
        );
      case 4:
        return (
          <WebinarRoomComponent
            link={link}
            setLink={setLink}
            error={error}
            setError={setError}
          />
        );
      default:
        return <>step invalid</>;
    }
  };

  //function for next step
  const handleNext = async () => {
    if (
      (activeStep === 0 &&
        title &&
        tags.length !== 0 &&
        description &&
        webinarBanner[0]) ||
      (activeStep === 1 && presenters?.length) ||
      (activeStep === 2 && startDate < endDate) ||
      (activeStep === 3 && attachments) ||
      (activeStep === 4 && link)
    ) {
      setError(false);
      if (activeStep !== steps(t).length - 1) {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
      }
      if (activeStep === steps(t).length - 1) {
        let attachment = attachments.map((media) => {
          return {
            Title: media.title,
            Description: media.description,
            Kind: media.kind,
            format: media.format,
            Media: media.media,
            Duration: media.duration,
          };
        });

        let items: createWebVariables = {
          items: [
            {
              parentId: "5ea559b3222115000aa8c02c",
              title: title,
              description: description,
              WebinarImage: webinarBanner[0]._id,
              Presenters: presenters?.map((presenter) => {
                return presenter.id;
              }),
              keywords: tags,
              presentDate: startDate.toISOString(),
              presentEndDate: endDate.toISOString(),
              Attachment: attachment,
              webinarLink: link,
              Organization: organizationId,
              PublishStatus: 0
            },
          ],
        };
        createWebinarMutation(items);
      }
    } else {
      setError(true);
    }
  };

  //function for previous step
  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
    setError(false);
  };

  let paperItems: PaperItems = {
    activeStep: activeStep,
    steps: steps(t),
    handleNext: handleNext,
    handleBack: handleBack,
    stepComponent: stepComponent,
  };

  // change steps and components
  useEffect(() => {
    paperItems = {
      activeStep: activeStep,
      steps: steps(t),
      handleNext: handleNext,
      handleBack: handleBack,
      stepComponent: stepComponent,
    };
  }, [activeStep]);

  return (
    <Wrapper>
      <CreateWebinarBanner />
      <CreateWebinarPaperComponent items={paperItems} />
    </Wrapper>
  );
};

export default CreateWebinarComponent;
