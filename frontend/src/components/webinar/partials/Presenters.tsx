import React, { FC, useState, useEffect } from "react";
import styled from "styled-components";
import { Box, Typography } from "@material-ui/core";
import {
  SearchPresentersComponent,
  Presenter,
  SelectedPresentersComponent,
  ModalComponent,
  CreatePresenterComponent,
  LoadingData,
} from "bp-components";
import { useQuery, useMutation } from "@apollo/react-hooks";
import {
  searchPresenter,
  searchPresenterVariables,
} from "$gqlQueryTypes/searchPresenter";
import { SEARCH_PRESENTER, CREATE_PRESENTER } from "$queries";
import { useOrganiztionContext } from "../../../context/OrganizationContext";
import { createPresenterVariables } from "$gqlQueryTypes/createPresenter";
import { PresentersProps } from "../interfaces";
import { useTranslation } from "react-i18next";

const Wrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  margin-bottom: ${(props) => props.theme.spacing(3)}px;
  &:last-child {
    margin-bottom: 0;
  }
`;
const useSearchPresenter = (inputvalue: string, organizationId: string) => {
  let { loading, error, data } = useQuery<
    searchPresenter,
    searchPresenterVariables
  >(SEARCH_PRESENTER, {
    variables: {
      query: {
        bool: {
          must: [
            {
              match: {
                title: inputvalue,
              },
            },
            {
              bool: {
                should: [
                  {
                    match: {
                      schema: "Presenter",
                    },
                  },
                ],
              },
            },
            {
              bool: {
                should: [
                  {
                    match: {
                      "Presenter.Organization": organizationId,
                    },
                  },
                ],
              },
            },
          ],
        },
      },
    },
    skip: inputvalue.length < 2,
  });
  let searchData;
  if (data?.search.items[0]) {
    searchData = data?.search.items.map((item: any) => {
      let title = item.title;
      let id = item._id;
      let image = item.Image && item.Image._id;
      return { title, id, image };
    });
  }
  if (!searchData) searchData = [];
  return { searchData, loading, error };
};

const Presenters: FC<PresentersProps> = (props) => {
  const {
    uploadHandler,
    progress,
    setUploadState,
    presenterImage,
    presenters,
    setPresenters,
    error,
    setError,
  } = props;

  const [openCreateModal, setOpenCreateModal] = useState(false);
  const { t } = useTranslation();

  //states for search presenter
  const [inputValue, setInputValue] = useState("");
  const [open, setOpen] = useState(false);

  //states for create presenter
  const [title, setTitle] = useState("");
  const [fieldOfStudy, setFieldOfStudy] = useState("");
  const [affiliation, setAffiliation] = useState("");
  const [biography, setBiography] = useState("");
  const [formError, setFormError] = useState(false);

  const { organizationId } = useOrganiztionContext();

  const searchData = useSearchPresenter(inputValue, organizationId);
  const options = searchData.searchData;
  const loading = searchData.loading;

  const addPresenter = (presenter: Presenter) => {
    let newPresenters: Presenter[] = Object.assign([], presenters);
    newPresenters.push(presenter);

    setPresenters(newPresenters);
    setError(false);
  };

  const deletePresenter = (presenter: Presenter) => {
    let newPresenters: Presenter[] = Object.assign([], presenters);
    newPresenters.some((item, index) => {
      if (item.id === presenter.id) {
        newPresenters.splice(index, 1);
      }
    });
    setPresenters(newPresenters);
  };

  // function for set items of createPresenter component
  const setPresenterItems = (name: string, value: string) => {
    if (name === "title") {
      setTitle(value);
    }
    if (name === "fieldOfStudy") {
      setFieldOfStudy(value);
    }
    if (name === "affiliation") {
      setAffiliation(value);
    }
    if (name === "biography") {
      setBiography(value);
    }
  };

  //create presenter
  const [createPresenters, { loading: mutationLoading }] = useMutation(
    CREATE_PRESENTER
  );
  const createPresenterHandle = async () => {
    if (
      title &&
      fieldOfStudy &&
      affiliation &&
      biography &&
      presenterImage[0]
    ) {
      let presenterItems: createPresenterVariables = {
        items: [
          {
            title: title,
            fieldOfStudy: fieldOfStudy,
            affiliation: affiliation,
            biography: biography,
            Image: presenterImage[0]._id,
            parentId: "5eb6b1ab923c300008351d9d",
            Organization: organizationId,
          },
        ],
      };
      const result = await createPresenters({
        variables: { items: presenterItems.items[0] },
      });
      if (!mutationLoading && result.data) {
        const item: Presenter = {
          id: result.data.createPresenters[0]._id,
          title: result.data.createPresenters[0].title,
          image: result.data.createPresenters[0].Image._id,
        };
        const newArray = Object.assign([], presenters);
        newArray.push(item);
        setPresenters(newArray);
      }
      setOpenCreateModal(false);
    } else {
      setFormError(true);
    }
  };

  //check for all of items of createPresenter component are filled
  useEffect(() => {
    if (
      title ||
      fieldOfStudy ||
      affiliation ||
      biography ||
      presenterImage[0]
    ) {
      setFormError(false);
    }
  }, [title, fieldOfStudy, affiliation, biography, presenterImage]);

  return (
    <LoadingData loading={mutationLoading}>
      {() => {
        return (
          <>
            <Wrapper>
              {error && (
                <span style={{ color: "red", marginBottom: 18 }}>
                  {t("* انتخاب حداقل یک ارائه دهنده الزامی است")}
                </span>
              )}
              <Typography style={{ color: `${error ? "red" : "inherit"}` }}>
                {t("ارائه دهنده *")}
              </Typography>
              <div style={{ display: "flex" }}>
                <SearchPresentersComponent
                  inputValue={inputValue}
                  setInputValue={setInputValue}
                  options={options}
                  loading={loading}
                  open={open}
                  setOpen={setOpen}
                  addPresenter={addPresenter}
                  presenters={presenters}
                  setOpenCreateModal={setOpenCreateModal}
                />
              </div>
              {presenters && (
                <SelectedPresentersComponent
                  presenters={presenters}
                  deletePresenter={deletePresenter}
                />
              )}
              <ModalComponent
                open={openCreateModal}
                closeModal={() => setOpenCreateModal(false)}
              >
                <CreatePresenterComponent
                  title={title}
                  fieldOfStudy={fieldOfStudy}
                  affiliation={affiliation}
                  biography={biography}
                  presenterImage={presenterImage}
                  error={formError}
                  setItems={setPresenterItems}
                  uploadHandler={uploadHandler}
                  setUploadState={setUploadState}
                  progress={progress}
                  createPresenter={createPresenterHandle}
                  setOpen={setOpenCreateModal}
                />
              </ModalComponent>
            </Wrapper>
          </>
        );
      }}
    </LoadingData>
  );
};

export default Presenters;
