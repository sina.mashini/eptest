import React, { FC, useState } from "react";
import styled from "styled-components";
import { MediaAttachments, SelectedAttachments } from "bp-components";
import { Grid } from "@material-ui/core";
import { AttachmentsProps } from "../interfaces";

const Wrapper = styled(Grid)`
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
`;

const Attachments: FC<AttachmentsProps> = (props) => {
  const {
    uploadHandler,
    media,
    setUploadState,
    attachments,
    setAttachmentHandler,
    openModal,
    progress,
    setOpenModal,
    error,
    deleteAttachedMedia,
  } = props;
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  return (
    <Wrapper>
      <MediaAttachments
        uploadHandler={uploadHandler}
        media={media}
        title={title}
        description={description}
        setTitle={setTitle}
        progress = {progress}
        setDescription={setDescription}
        setUploadState={setUploadState}
        setAttachmentHandler={setAttachmentHandler}
        openModal={openModal}
        setOpenModal={setOpenModal}
        error={error}
      />
      <SelectedAttachments
        attachments={attachments}
        deleteAttachedMedia={deleteAttachedMedia}
      />
    </Wrapper>
  );
};

export default Attachments;
