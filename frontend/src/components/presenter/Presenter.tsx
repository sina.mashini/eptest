import React, { FC } from "react";
import {
  getPresenters,
  getPresentersVariables,
} from "$gqlQueryTypes/getPresenters";
import { getIds, getIdsVariables } from "$gqlQueryTypes/getIds";
import { GET_Ids, GET_PRESENTER } from "$queries";
import { useQuery } from "@apollo/react-hooks";
import { Presnter, SimplePresenterProps, LoadingData } from "bp-components";
import Link from "next/link";
import { useOrganiztionContext } from "../../context/OrganizationContext";

const renderPresenterLink = (children: JSX.Element, id: string) => {
  return (
    <Link href="/presenter/[presenterId]" as={`/presenter/${id}`}>
      {children}
    </Link>
  );
};

const usePresentersIds = (id) => {
  let { loading, error, data } = useQuery<getIds, getIdsVariables>(GET_Ids, {
    variables: {
      query: {
        bool: {
          must: [
            {
              match: { "Presenter.Organization": id },
            },
          ],
          filter: [
            {
              term: { "schema.keyword": "Presenter" },
            },
          ],
        },
      },
      offset: 0,
      limit: 10,
    },
  });

  const ids = data?.search.items.map((item) => {
    return item._id;
  });

  return { loading, error, ids };
};

const usePresenters = (ids, idsIsLoading) => {
  const { loading, error, data } = useQuery<
    getPresenters,
    getPresentersVariables
  >(GET_PRESENTER, {
    variables: {
      PresenterIds: ids,
    },
    skip: idsIsLoading,
  });
  let presenters;
  if (loading === false) {
    presenters = createPresenterData(data?.getPresenters);
  }
  return { loading, error, data: presenters };
};

const createPresenterData = (items) => {
  let presenters: SimplePresenterProps[] = [];
  if (items) {
    presenters = items.map(function (item) {
      return {
        id: item._id,
        name: item.title,
        education: item.affiliation,
        image: item.Image?._id,
        link: renderPresenterLink,
      };
    });
  }
  return presenters;
};

export const PresenterComponet: FC = () => {
  const { organizationId } = useOrganiztionContext();
  const presentersIds = usePresentersIds(organizationId);
  const presenters = usePresenters(presentersIds.ids, presentersIds.loading);
  const loading = presentersIds.loading || presenters.loading;
  return (
    <LoadingData loading={loading}>
      {() => {
        return (
          <>
            <Presnter
              presnters={presenters.data}
              loading={presenters.loading}
            />
          </>
        );
      }}
    </LoadingData>
  );
};
