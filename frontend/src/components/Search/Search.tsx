import React, { useState } from "react";
import { useRouter } from "next/router";
import { getIds, getIdsVariables } from "$gqlQueryTypes/getIds";
import { useQuery } from "@apollo/react-hooks";
import { GET_Ids } from "$queries";
import { SearchToolbar } from "bp-components";
import { useOrganiztionContext } from "../../context/OrganizationContext";

const useSearchResult = (inputvalue, id) => {
  let { loading, error, data } = useQuery<getIds, getIdsVariables>(GET_Ids, {
    variables: {
      query: {
        bool: {
          must: [
            { match: { title: inputvalue } },
            {
              bool: {
                should: [
                  {
                    match: {
                      "Webinar.Organization": id,
                    },
                  },
                  {
                    match: {
                      "Presenter.Organization": id,
                    },
                  },
                ],
              },
            },
            {
              bool: {
                should: [
                  {
                    bool: {
                      must: [
                        { match: { schema: "Webinar" } },
                        { match: { "Webinar.PublishStatus": 2 } },
                      ],
                    },
                  },
                  { match: { schema: "Presenter" } },
                ],
              },
            },
          ],
          filter: [],
        },
      },
      offset: 0,
      limit: 40,
    },
    skip: inputvalue.length < 2,
  });
  let searchData = data?.search.items.map((item) => {
    let title = item.title;
    let id = item._id;
    let schema = item.schema.name;
    return { title, id, schema };
  });
  if (!searchData) searchData = [];
  return { searchData, loading, error };
};

export const Search = () => {
  const router = useRouter();
  const [open, setOpen] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const { organizationId } = useOrganiztionContext();
  const data = useSearchResult(inputValue, organizationId);
  const options = data.searchData;
  const loading = data.loading;

  const onChangeSearch = (event, value, reason) => {
    if (reason === "select-option") {
      const href = `/${value.schema}/${value.id}`;
      const url =
        value.schema === "Presenter"
          ? "/presenter/[presenterId]"
          : "/webinar/[webId]";
      router.push(url, href);
    }
  };

  return (
    <SearchToolbar
      searchResult={options}
      open={open}
      inputValue={inputValue}
      setInputValue={setInputValue}
      setOpen={setOpen}
      loading={loading}
      onChangeSearch={onChangeSearch}
    />
  );
};

export default Search;
