import React, { FC, useContext } from "react";
import Head from "next/head";
import { bool, node, InferProps } from "prop-types";
import { Header } from "bp-components";
import { Search } from "../Search/Search";
import Link from "next/link";
import AuthContext from "../../context/AuthContext";
import { useOrganiztionContext } from "../../context/OrganizationContext";
import { useRouter } from "next/router";

const homeLink = (childeren: JSX.Element | JSX.Element[]) => (
  <Link href="/">
    <a style={{ textDecoration: "none" }}>{childeren}</a>
  </Link>
);

const linkToSpecificPage = (
  childeren: JSX.Element | JSX.Element[],
  url: string
) => {
  return (
    <Link href={url}>
      <a style={{ textDecoration: "none" }}>{childeren}</a>
    </Link>
  );
};

type PropTypes = InferProps<typeof propTypes>;

const Page: FC<PropTypes> = (props) => {
  const { Favicon, Logo, title, } = useOrganiztionContext();
  const { login, logout, getUser } = useContext(AuthContext);
  const router = useRouter();
  const fullName = getUser()?.fullName;
  const adminPanel = () => {
    router.push("/admin/dashboard");
  }

  //TODO get role from authContext
  const role = "admin";

  return (
    <div style={{height: '100%'}} >
      {props.headless ? null : (
        <Head>
          <title>{title}</title>
          <link rel="icon" href={Favicon} />
          <link rel="stylsheet" href="./global.css" />
          <link rel="stylsheet" href="./style.css" />
        </Head>
      )}
      <Header
        src={Logo}
        homeLink={homeLink}
        fullName={fullName}
        linkToSpecificPage={linkToSpecificPage}
        login={login}
        logout={logout}
        role={role}
        adminPanel={adminPanel}
      >
        <Search />
      </Header>
      {props.children}
    </div>
  );
};

const propTypes = {
  headless: bool,
  children: node,
};

Page.propTypes = propTypes;

export default Page;
