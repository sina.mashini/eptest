import React, { useEffect, useState } from "react";
import MessageComponent from "./Message";
import { Box } from "@material-ui/core";
import styled from "styled-components";
import { text, select } from "@storybook/addon-knobs";
import { RoundedButton } from "../Buttons";

const Wrapper = styled(Box)`
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  padding: 18px;
`;

export default {
  title: "Atoms | MessageComponent / Message",
  component: MessageComponent,
};

export const Message = () => {
  const messageLabel = "Message";
  const messageDefaultValue = "پیام موفقیت برای کاربر";
  const message = text(messageLabel, messageDefaultValue);

  const typeLabel = "Type";
  const typeOptions = {
    success: "success",
    error: "error",
    warning: "warning",
    info: "info",
  };
  const typeDefaultValue = "success";
  const storyType = select(typeLabel, typeOptions, typeDefaultValue);
  const [type, setType] = useState<"success" | "error" | "warning" | "info">(
    "success"
  );

  useEffect(() => {
    if (storyType === "success") {
      setType("success");3
    } else if (storyType === "error") {
      setType("error");
    } else if (storyType === "warning") {
      setType("warning");
    } else setType('info');
  }, [storyType]);

  const buttons = (
    <>
      <RoundedButton
        variant="contained"
        color="primary"
        onClick={() => {
          alert("going to home page");
        }}
        label="خانه"
      />
      <RoundedButton
        variant="contained"
        color="primary"
        onClick={() => {
          alert("going to my webinars page");
        }}
        label="وبینارهای من"
      />
    </>
  );
  return (
    <Wrapper>
      <MessageComponent message={message} buttons={buttons} type={type} />
    </Wrapper>
  );
};
