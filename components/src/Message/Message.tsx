import React, { FC } from "react";
import styled from "styled-components";
import { Grid, Typography, Box } from "@material-ui/core";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";

const Wrapper = styled(Grid)`
  display: flex;
  flex-direction: column;
  height: 100%;
  align-items: center;
  justify-content: center;
`;
const Title = styled(Typography)`
  margin-bottom: ${(props) => props.theme.spacing(3)}px !important;
`;
const IconWrap = styled(Box)`
  position: relative;
  width: 55px;
  height: 55px;
  margin-bottom: ${(props) => props.theme.spacing(3)}px !important;
  & > * {
    position: absolute;
    width: 100% !important;
    height: 100% !important;
    top: 0;
    left: 0;
  }
`;
const ButtonsWrap = styled(Box)`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  width: 100%;
  & > button {
    width: 120px;
  }
  @media all and (max-width: 600px) {
    flex-direction: column;
    & > button {
      margin-bottom: ${(props) => props.theme.spacing(2)}px;
    }
  }
`;
const WarningIcon = styled(ErrorOutlineIcon)`
  color: #ffbe00;
`
const InfoIcon = styled(ErrorOutlineIcon)`
  color: #069fea;
  transform: rotate(180deg);
`

export interface MessageProps {
  message: string;
  type?: "success" | "error" | "warning" | "info";
  buttons: JSX.Element;
}

export const MessageComponent: FC<MessageProps> = (props) => {
  const { message, buttons, type } = props;

  return (
    <Wrapper item xs={12}>
      <Title align="center">{message}</Title>
      <IconWrap>
        {type ? (
          type === "success" ? (
            <CheckCircleOutlineIcon color="primary" />
          ) : type === "error" ? (
            <HighlightOffIcon color="error" />
          ) : type === "warning" ? (
            <WarningIcon />
          ) : (
            <InfoIcon />
          )
        ) : (
          <CheckCircleOutlineIcon color="primary" />
        )}
      </IconWrap>
      <ButtonsWrap>{buttons}</ButtonsWrap>
    </Wrapper>
  );
};

export default MessageComponent;
