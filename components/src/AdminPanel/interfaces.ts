import { Presenter } from '../CreateWebinar/interfaces';

export interface WebinarAttachment {
    title: string;
    id: string;
  }
  
  export interface User {
    username: string;
    _id: string;
    Name: string;
    Family: string;
    UserId: string | null;
    Email: string;
  }
  
  export interface Webinar {
    title: string;
    _id: string;
    WebinarImage: string;
    keywords: string[] | null;
    presentDate: Date | null;
    presentEndDate: Date | null;
    Attachment: WebinarAttachment[] | null;
    description: string | null;
    webinarLink: string | null;
    Presenters: Presenter[] | null;
    WebinarOwner: User | null;
    PublishStatus: number;
  }