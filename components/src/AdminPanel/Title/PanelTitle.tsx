import React, { FC, Dispatch, SetStateAction, useContext } from "react";
import styled from "styled-components";
import { Hidden, Button, Typography } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import { context } from '../../Context/StoryBookContext';

const TitleWrap = styled.div`
  display: flex;
  min-height: 80px;
  background-color: ${(props) => props.theme.palette.primary.main};
  align-items: center;
  padding: ${(props) => props.theme.spacing(3)}px;
`;

export interface PanelTitleProps {
  setOpenMobileDrawer: Dispatch<SetStateAction<boolean>>;
}

export const PanelTitle: FC<PanelTitleProps> = (props) => {
  const { setOpenMobileDrawer } = props;
  const { translate } = useContext(context);
  const {t} = translate();

  return (
    <TitleWrap>
      <Hidden smUp implementation="css">
        <Button onClick={() => setOpenMobileDrawer(true)}>
          <MenuIcon />
        </Button>
      </Hidden>
      <Typography align="center" style={{ flex: 1 }} variant="h5">
        {t("پنل ادمین")}
      </Typography>
    </TitleWrap>
  );
};

export default PanelTitle;
