import React, { FC, useContext } from "react";
import styled from "styled-components";
import { Webinar } from "../interfaces";
import {
  Box,
  Typography,
  Select,
  MenuItem,
  IconButton,
  TableCell,
  TableRow,
} from "@material-ui/core";
import { context } from "../../Context/StoryBookContext";
import VisibilityIcon from "@material-ui/icons/Visibility";

const ImageWrap = styled(Box)`
  position: relative;
  width: 70px;
  height: 35px;
  border-radius: ${(props) => props.theme.spacing()}px;
  overflow: hidden;
`;
const Image = styled.img`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
`;
const Title = styled(Typography)`
  padding: 0 ${(props) => props.theme.spacing()}px;
`;
const PresentersName = styled(Box)`
  display: flex;
  max-width: 150px;
  padding: ${(props) => props.theme.spacing()}px;
  flex-wrap: wrap;
`;
const PresenterRow = styled(TableCell)`
  @media all and (max-width: 768px) {
    display: none !important;
  }
`;

export interface TableBodyItemProps {
  item: Webinar;
  link: (children: JSX.Element, id: string) => JSX.Element;
}

export const TableBodyItem: FC<TableBodyItemProps> = (props) => {
  const { item, link } = props;

  const { translate } = useContext(context);
  const { t } = translate();

  return (
    <TableRow>
      <TableCell>
        <ImageWrap>
          <Image
            src={`https://edupack.nightly.repo.avidcloud.io/api/v1/blob/${item.WebinarImage}`}
          />
        </ImageWrap>
        <Title>{item.title}</Title>
      </TableCell>
      <PresenterRow>
        <PresentersName>
          <Typography>
            {item.Presenters?.map((presenter, index) => {
              if (item.Presenters && index === item.Presenters.length - 1) {
                return presenter.title;
              } else {
                return presenter.title + ",";
              }
            })}
          </Typography>
        </PresentersName>
      </PresenterRow>
      <TableCell>
        <Select value={item.PublishStatus}>
          <MenuItem value={2}>{t("منتشر شده")}</MenuItem>
          <MenuItem value={1}>{t("برگشت داده شده")}</MenuItem>
          <MenuItem value={0} disabled>
            {t("در صف انتشار")}
          </MenuItem>
        </Select>
      </TableCell>
      <TableCell>
        {link(
          <IconButton>
            <VisibilityIcon color="primary" />
          </IconButton>,
          item._id
        )}
      </TableCell>
    </TableRow>
  );
};

export default TableBodyItem;
