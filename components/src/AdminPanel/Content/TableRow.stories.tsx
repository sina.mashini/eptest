import React from "react";
import TableRowComponent from "./TableBodyItem";
import { Webinar } from "../interfaces";
import styled from "styled-components";
import { Box } from '@material-ui/core';

const Wrapper = styled(Box)`    
    width: 100%;
    display: table;
`

export default {
  title: "Section Components | AdminPanel / TableRow",
  Components: TableRowComponent,
};

const item: Webinar = {
  title: "وبینار ۱",
  description:
    "من وبینار ۱ هستم من وبینار ۱ هستم من وبینار ۱ هستم من وبینار ۱ هستم",
  keywords: ["react", "storybook", "nextjs"],
  Presenters: [
    {
      id: "123",
      image: "profile.jpg",
      title: "سینا ماشینی",
    },
    {
      id: "123",
      image: "profile.jpg",
      title: "سینا ماشینی",
    },
    {
      id: "123",
      image: "profile.jpg",
      title: "سینا ماشینی",
    },
  ],
  WebinarImage: "5f16da7be9666f0008880aa5",
  presentDate: new Date(),
  presentEndDate: new Date(),
  _id: "234",
  webinarLink: "www.google.com",
  WebinarOwner: {
    Email: "sina.mashini@gmail.com",
    Name: "سینا",
    Family: "ماشینی",
    _id: "3456",
    username: "سینا ماشینی",
    UserId: "4567",
  },
  Attachment: [
    {
      title: "فایل 1",
      id: "create.jpg",
    },
  ],
  PublishStatus: 2,
};

const renderWebinarLink = (children: JSX.Element) => <a href="#">{children}</a>;

export const TableRow = () => {
  return (
    <Wrapper>
      <TableRowComponent item={item} link={renderWebinarLink} />
    </Wrapper>
  );
};
