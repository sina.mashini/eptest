import React, { FC, useContext } from "react";
import styled from "styled-components";
import { Webinar } from "../interfaces";
import {
  Box,
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@material-ui/core";
import { context } from "../../Context/StoryBookContext";
import SettingsIcon from "@material-ui/icons/Settings";
import TableBodyItem from "./TableBodyItem";

const Wrapper = styled(Box)`
  display: flex;
  width: 100%;
`;
const PresenterRow = styled(TableCell)`
  @media all and (max-width: 768px) {
    display: none !important;
  }
`

export interface SpecificWebinarsProps {
  specificWebinarList: Webinar[];
  link: (children: JSX.Element, id: string) => JSX.Element;
}

export const SpecificWebinars: FC<SpecificWebinarsProps> = (props) => {
  const { specificWebinarList, link } = props;

  const { translate } = useContext(context);
  const { t } = translate();

  console.log(specificWebinarList);

  return (
    <Wrapper>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>{t("نام")}</TableCell>
              <PresenterRow>{t("ارائه دهندگان")}</PresenterRow>
              <TableCell>{t("وضعیت")}</TableCell>
              <TableCell>
                <SettingsIcon />
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {specificWebinarList.map((webinar) => (
              <TableBodyItem item={webinar} link={link} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Wrapper>
  );
};

export default SpecificWebinars;
