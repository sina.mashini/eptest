import React from "react";
import SpecificWebinarsComponent from './SpecificWebinars';
import { Webinar } from '../interfaces';

export default {
    title: "Section Components | AdminPanel / SpecificWebinar",
    Components: SpecificWebinarsComponent,
  };

  const items: Webinar[] = [{
    title: "من وبینار ۱ هستم من وبینار ۱ هستم من وبینار ۱ هستم من وبینار ۱ هستم",
    description: "من وبینار ۱ هستم من وبینار ۱ هستم من وبینار ۱ هستم من وبینار ۱ هستم",
    keywords: ["react", "storybook", "nextjs"],
    Presenters: [
        {
            id: '123',
            image: "profile.jpg",
            title: "سینا ماشینی"
        },
        {
            id: '123',
            image: "profile.jpg",
            title: "سینا ماشینی"
        },
        {
            id: '123',
            image: "profile.jpg",
            title: "سینا ماشینی"
        },
    ],
    WebinarImage: "5f16da7be9666f0008880aa5",
    presentDate: new Date(),
    presentEndDate: new Date(),
    _id: '234',
    webinarLink: 'www.google.com',
    WebinarOwner: {
        Email: 'sina.mashini@gmail.com',
        Name: 'سینا',
        Family: 'ماشینی',
        _id: '3456',
        username: 'سینا ماشینی',
        UserId: '4567'
    },
    Attachment: [
        {
            title: 'فایل 1',
            id: 'create.jpg'
        }
    ],
    PublishStatus: 2
}]

const renderWebinarLink = (children: JSX.Element) => (
    <a href="#">{children}</a>
  );

export const SpecificWebinars = () => {
    return (
        <SpecificWebinarsComponent specificWebinarList={items} link={renderWebinarLink} />
    )
}