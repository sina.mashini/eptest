import React, { useContext, useState } from "react";
import DesctopDrawerComponent from "./DesctopDrawer";
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Hidden,
  Button,
} from "@material-ui/core";
import CancelPresentationIcon from "@material-ui/icons/CancelPresentation";
import { context } from "../../Context/StoryBookContext";
import MenuIcon from "@material-ui/icons/Menu";
import { MobileDrawer } from "./MobileDrawer";

export default {
  title: "Section Components | AdminPanel / Drawer",
  Components: { DesctopDrawerComponent, MobileDrawer },
};

export const PanelDrawer = () => {
  const { translate } = useContext(context);
  const { t } = translate();
  const [openMobileDrawer, setOpenMobileDrawer] = useState(false);

  const items: JSX.Element[] = [
    <ListItem button>
      <ListItemIcon>
        <CancelPresentationIcon />
      </ListItemIcon>
      <ListItemText primary={t("وبینارهای منتشر نشده")} />
    </ListItem>,
  ];
  return (
    <>
      <Hidden smUp implementation="css">
        <Button onClick={() => setOpenMobileDrawer(true)}>
          <MenuIcon />
        </Button>
        <MobileDrawer
          items={items}
          open={openMobileDrawer}
          onClose={() => setOpenMobileDrawer(false)}
        />
      </Hidden>
      <DesctopDrawerComponent items={items} />
    </>
  );
};
