import React, { FC, useContext } from "react";
import { Drawer, List } from "@material-ui/core";
import { context } from "../../Context/StoryBookContext";
import styled from "styled-components";

const Wrapper = styled(Drawer)`
  min-width: 225px;
`;
const DrawerItem = styled(List)`
  border-bottom: 1px solid #eee;
`;

export interface MobileDrawer {
  items: JSX.Element[];
  open: boolean;
  onClose: () => void;
}

export const MobileDrawer: FC<MobileDrawer> = (props) => {
  const { items, open, onClose } = props;

  const { defaultDir } = useContext(context);
  let dir: "right" | "left" = "left";
  if (defaultDir === "ltr") dir = "right";

  return (
    <Wrapper
      anchor={dir}
      open={open}
      onClose={onClose}
      PaperProps={{ style: {minWidth: 225} }}
    >
      <DrawerItem>{items.map((item) => item)}</DrawerItem>
    </Wrapper>
  );
};
