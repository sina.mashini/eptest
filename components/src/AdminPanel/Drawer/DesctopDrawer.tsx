import React, { FC, useContext } from "react";
import { Drawer, List, Hidden } from "@material-ui/core";
import styled from "styled-components";
import { context } from "../../Context/StoryBookContext";
const Wrapper = styled(Drawer)`
  min-width: 260px;
  flex-shrink: 0;
  height: 100%;
  & > div {
    position: unset;
    height: 100%;
  }
`;
const DrawerList = styled(List)`
  min-width: 260px;
  border-bottom: 1px solid #eee;
  padding: 8px !important;
`;

export interface DesctopDrawerProps {
  items: JSX.Element[];
}

export const DesctopDrawer: FC<DesctopDrawerProps> = (props) => {
  const { items } = props;

  const { defaultDir } = useContext(context);
  let dir: "right" | "left" = "left";
  if (defaultDir === "ltr") dir = "right";

  return (
    <Hidden xsDown implementation="css">
      <Wrapper anchor={dir} variant="permanent">
        <DrawerList>{items.map((item) => item)}</DrawerList>
      </Wrapper>
    </Hidden>
  );
};

export default DesctopDrawer;
