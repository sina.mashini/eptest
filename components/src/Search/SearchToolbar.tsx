import React, { FC, useContext } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { CircularProgress, Grid } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import InputAdornment from "@material-ui/core/InputAdornment";
import styled from "styled-components";
import { context } from "../Context/StoryBookContext";

const TextSearch = styled(TextField)`
  width: 313px;
  font-family: "IRANSans";
  @media (max-width: 767px) {
    width: 190px !important;
  }
`;

const ResultGrid = styled(Grid)`
  font-family: "IRANSans";
  font-size: 15px;
  color: black;
`;

const GroupGrid = styled(Grid)`
  font-family: "IRANSans";
  margin: 0px 10px 5px 0px;
  font-size: 13px;
  color: gray;
`;
const AutoSearch = styled.div`
  & .MuiAutocomplete-inputRoot {
    font-family: "IRANSans";
  }
`;

export interface SearchResult {
  title: string;
  id: string;
  schema: string;
}

export interface SearchProps {
  open: boolean;
  inputValue: string;
  setInputValue: (string) => any;
  onChangeSearch: (event, value, reason) => any;
  searchResult: SearchResult[];
  loading: boolean;
  setOpen: (boolean) => any;
}

export const SearchToolbar: FC<SearchProps> = ({
  open,
  inputValue,
  setInputValue,
  searchResult,
  loading,
  setOpen,
  onChangeSearch,
}) => {
  const options = searchResult;
  const { translate } = useContext(context);
  const { t } = translate();
  return (
    <AutoSearch>
      <Autocomplete
        freeSolo
        id="asynchronous-demo"
        style={{ width: 313 }}
        open={open}
        inputValue={inputValue}
        onChange={onChangeSearch}
        onInputChange={(event, newInputValue) => {
          setInputValue(newInputValue);
        }}
        onOpen={() => {
          setOpen(options.length > 0);
        }}
        onClose={() => {
          setOpen(false);
        }}
        getOptionSelected={(option, value) => option.title === value.title}
        getOptionLabel={(option) => option.title}
        options={options}
        groupBy={(option) => {
          console.log("option:", option)
          let group = t("وبینار");
          console.log("group:", group)
          if (option.schema == "Presenter") group = t("ارایه دهنده‌");
          console.log("group:", group)
          return group;
        }}
        loading={loading}
        renderInput={(params) => (
          <TextSearch
            {...params}
            autoComplete="on"
            variant="outlined"
            placeholder={t("جستجو")}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <>
                  <React.Fragment>
                    {loading ? (
                      <CircularProgress color="inherit" size={20} />
                    ) : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                </>
              ),
            }}
          />
        )}
        renderOption={(option, state) => {
          return <ResultGrid>{option.title}</ResultGrid>;
        }}
        renderGroup={(params) => (
          <GroupGrid>
            {params.group} {params.children}
          </GroupGrid>
        )}
      />
    </AutoSearch>
  );
};
