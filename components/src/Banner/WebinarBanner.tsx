import React, { FC } from "react";
import styled from "styled-components";
import { Box, Typography, Grid } from "@material-ui/core";
import { imageAddress } from "../ImageAdress/Address";

const BannerWrap = styled(Box)`
  position: relative;
  display: flex;
  width: 100%;
  padding-top: 43.1%;
  & > img {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
  }
`;
const BannerTitle = styled(Grid)`
  position: absolute;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 0 ${(props) => props.theme.spacing(5)}px;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
`;
const TitleGrid = styled(Grid)`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export interface WebinarBanner {
  image: string;
  title: string;
  description: string;
}

export const WebinarBannerComponent: FC<WebinarBanner> = (props) => {
  const { image, title, description } = props;
  return (
    <BannerWrap>
      <img src={`${imageAddress}${image}`} />
      <BannerTitle item>
        <TitleGrid item xs={12} sm={6}>
          <Typography variant="h1">{title}</Typography>
          <Typography variant="caption">{description}</Typography>
        </TitleGrid>
      </BannerTitle>
    </BannerWrap>
  );
};

export default WebinarBannerComponent;
