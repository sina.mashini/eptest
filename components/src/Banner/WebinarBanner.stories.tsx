import React from "react";
import WebinarBanner from "./WebinarBanner";
import styled from "styled-components";
import { Box } from "@material-ui/core";
import { text } from '@storybook/addon-knobs';

export default {
  title: "Section Components | Banner / WebinarBanner",
  component: WebinarBanner,
};

const Wrapper = styled(Box)`
  display: flex;
  width: 100%;
  height: 100%;
`;

export const CreateWebinarBanner = () => {

  const titleLabel = 'Title';
const titleDefaultValue = 'ایجاد وبینار';
const titleValue = text(titleLabel, titleDefaultValue);

const descriptionLabel = 'Description';
const descriptionDefaultValue = 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،';
const descriptionValue = text(descriptionLabel, descriptionDefaultValue);

  return (
    <Wrapper>
      <WebinarBanner image="create.jpg" title={titleValue} description={descriptionValue} />
    </Wrapper>
  );
};
