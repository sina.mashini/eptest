import React, { FC } from "react";
import styled from "styled-components";
import { Text } from "../Text/Text";
import Grid from "@material-ui/core/Grid";
import { LoadingData } from "../Progress/LoadingData";
import { imageAddress } from "../ImageAdress/Address";

const Root = styled(Grid)`
  background-image: linear-gradient(
      -239deg,
      rgba(19, 90, 128, 0.6) 25%,
      rgba(0, 37, 63, 0.92) 82%
    ),
    url(${(props) => props.theme});
  height: 434px;
  background-size: cover;
  background-position: right bottom, left top;
  margin-top: 80px;
`;

export interface BannerProps {
  title: string;
  description: string;
  linktitle: string;
  image?: string;
  linkWrapper: (children: JSX.Element) => JSX.Element;
  loading: boolean;
}

export const Banner: FC<BannerProps> = ({
  title,
  description,
  linktitle,
  linkWrapper,
  image,
  loading,
}) => {
  return (
    <LoadingData loading={loading}>
      {() => {
        return (
          <>
            <Root
              theme={`${imageAddress}${image}`}
              container
              direction="row"
              justify="flex-start"
              alignItems="center"
            >
              <Grid item xs={1}></Grid>
              <Grid item xs={10} sm={6} md={3}>
                <Text
                  title={title}
                  description={description}
                  linktitle={linktitle}
                  linkWrapper={linkWrapper}
                />
              </Grid>
              <Grid item xs={1} sm={5} md={8}></Grid>
            </Root>
          </>
        );
      }}
    </LoadingData>
  );
};
export default Banner;
