import React, { FC, useContext } from "react";
import { AppBar, Toolbar } from "@material-ui/core";
import styled from "styled-components";
import Grid from "@material-ui/core/Grid";
import { ProfileIcon } from "../Menu/ProfileMenu/ProfileIcon";
import { EduPackBtn } from "../Buttons/EduPackBtn";
import { ProfilePaper } from "../Menu";
import { TemporaryDrawer } from "./Drawer";
import { LanguageIconComp } from "../Menu/LanguageMenu/LanguageIcon";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { context } from "../Context/StoryBookContext";
import DashboardIcon from '@material-ui/icons/Dashboard';

const ToolBar = styled(Toolbar)`
  background-color: #ffffff;
  height: 80px;
`;

const Drawer = styled.div`
  @media (min-width: 767px) {
    display: none;
  }
`;

const ProfileIconPart = styled.div`
  @media (max-width: 767px) {
    display: none;
  }
`;

const LogoDiv = styled.div`
  @media (max-width: 767px) {
    display: none;
  }
`;

const Img = styled.img`
  width: 90px;
  margin-right: 60px;
  margin-left: 60px;
  @media (max-width: 767px) {
    width: 110px;
    margin: unset;
  }
`;

const DrawerLogo = styled.div`
  margin: 0 auto;
`;

const DrawerPart = styled.div`
  * {
    font-family: "IranSans";
  }
  display: flex;
  font-size: 17px;
  margin: 0 22px;
`;
const DrawerLink = styled.a`
  font-size: 15px;
  font-family: "IranSans";
  margin: 0 20px;
`;

const Lang = styled.span`
  font-family: "IranSans";
  font-size: 15px;
`;

export interface HeaderProps {
  src: string;
  homeLink: (children: JSX.Element) => JSX.Element;
  children: JSX.Element[] | JSX.Element;
  login: () => void;
  logout: () => void;
  linkToSpecificPage: (children: JSX.Element, url: string) => JSX.Element;
  fullName: string | null | undefined;
  role: string;
  adminPanel: () => void;
}

const profilePart = (fullName, login, logout, logo: JSX.Element, t, adminPanel) => {
  if (fullName) {
    return [
      <DrawerLogo>{logo}</DrawerLogo>,
      <ProfilePaper name={fullName} />,
      <>
        <LanguageIconComp />
        <Lang>{t("زبان")}</Lang>
      </>,
      <DrawerPart>
      <DashboardIcon />
      <DrawerLink onClick={adminPanel}>{t("پنل ادمین")}</DrawerLink>
    </DrawerPart>,
      <DrawerPart>
        <ExitToAppIcon />
        <DrawerLink onClick={logout}>{t("خروج")}</DrawerLink>
      </DrawerPart>,
    ];
  } else {
    return [
      <DrawerLogo>{logo}</DrawerLogo>,
      <EduPackBtn
        variant="contained"
        label={t("ورود / ثبت نام")}
        onClick={login}
      />,
      <>
        <LanguageIconComp />
        <Lang>{t("زبان")}</Lang>
      </>,
    ];
  }
};

export const Header: FC<HeaderProps> = ({
  src,
  homeLink,
  children,
  login,
  logout,
  linkToSpecificPage,
  fullName,
  role,
  adminPanel
}) => {
  const { translate, defaultDir } = useContext(context);
  const { t } = translate();
  let dir: "right" | "left" = "left";
  if (defaultDir === "ltr") dir = "right";

  return (
    <AppBar>
      <ToolBar>
        <Drawer>
          <TemporaryDrawer
            dirAnchor={dir}
            items={profilePart(
              fullName,
              login,
              logout,
              homeLink(<Img src={src} />),
              t,
              adminPanel
            )}
          />
        </Drawer>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          spacing={0}
        >
          <Grid item lg={8} xs={11} sm={8} md={8} xl={10}>
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="center"
              spacing={0}
            >
              <Grid item lg={4} xs={2} sm={4} md={4} xl={2}>
                <LogoDiv>{homeLink(<Img src={src} />)}</LogoDiv>
              </Grid>
              <Grid item lg={4} xs={10} sm={8} md={8} xl={8}>
                {children}
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={4} xs={1} sm={4} md={4} xl={1}>
            <ProfileIconPart>
              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={0}
              >
                <Grid item lg={2} xs={1} sm={2} md={2} xl={1}>
                  <ProfileIcon
                    fullName={fullName}
                    login={login}
                    logout={logout}
                    linkToSpecificPage={linkToSpecificPage}
                    role={role}
                    adminPanel={adminPanel}
                  ></ProfileIcon>
                </Grid>
                <Grid item lg={2} xs={1} sm={2} md={2} xl={1}>
                  <LanguageIconComp />
                </Grid>
              </Grid>
            </ProfileIconPart>
          </Grid>
        </Grid>
      </ToolBar>
    </AppBar>
  );
};
export default Header;
