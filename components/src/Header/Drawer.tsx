import React, { FC } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles({
  list: {
    width: 'auto',
    display: 'inline'
  },
  fullList: {
    width: 'auto',
  },
});

type Anchor = 'top' | 'left' | 'bottom' | 'right';

export interface DrawerProps {
   items: JSX.Element[]
   dirAnchor : 'top' | 'left' | 'bottom' | 'right';
}

export const TemporaryDrawer:FC<DrawerProps> = ({items, dirAnchor}) => {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor: Anchor, open: boolean) => (
    event: React.KeyboardEvent | React.MouseEvent,
  ) => {
    if (
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor: Anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      })}
      role="presentation"
    >
      <List>
         {items.map((item ,index) => (
          <ListItem button key={index}>
                {item}
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <div>
        <React.Fragment key={dirAnchor}>        
          <Button onClick={toggleDrawer(dirAnchor, true)}><MenuIcon /></Button>
          <Drawer anchor={dirAnchor} open={state[dirAnchor]} onClose={toggleDrawer(dirAnchor, false)}>
            {list(dirAnchor)}
          </Drawer>
        </React.Fragment>
    </div>
  );
}

export default TemporaryDrawer;