import React from "react";
import { Header } from "./Header";
import { SearchComponent } from "../Search/SearchToolbar.stories";
export default {
  title: "Layout Components | NavBar / Header",
  component: Header,
};

const homeLink = (childeren: JSX.Element) => <a href="#">{childeren}</a>;
const login = () => alert("Login");
const logout = () => alert("logout");
const linkToSpecificPage = (childeren: JSX.Element, url: string = "#") => (
  <a href={url}>{childeren}</a>
);
export const HeaderComponent = () => {
  return (
    <Header
      src="Logo.png"
      homeLink={homeLink}
      fullName="Sina Mashini"
      login={login}
      logout={logout}
      linkToSpecificPage={linkToSpecificPage}
      role="admin"
      adminPanel={() => {}}
    >
      <SearchComponent />
    </Header>
  );
};
