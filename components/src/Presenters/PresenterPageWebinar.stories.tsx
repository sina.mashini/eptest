import React from "react";
import { PresenterPageWebinars } from "./PresenterPageWebinar";
import { WebinarCardProps } from "../Card/WebinarCard/WebinarCard";

export default {
  title: "Section Components | Presenters / PresenterPageWebinars ",
  component: PresenterPageWebinars,
};

const renderWebinarLink = (children: JSX.Element) => <a href="#">{children}</a>;

export const PresenterWebinars = () => {
  const webinars: WebinarCardProps[] = [
    {
      id: "6545645646565",
      name: "وبینار برای گراف کیوال",
      image: "gph.png",
      presenter: "سینا ماشینی",
      presenterImage: "profile.jpg",
      keywords: ["OOP", "Grphql", "Programmig", "برنامه نویسی"],
      date: "2019-12-24T19:52:49",
      link: renderWebinarLink,
      endDate: "2019-12-24T19:52:49",
    },
    {
      id: "6545645646565",
      name: "وبینار برای گراف کیوال",
      image: "gph.png",
      presenter: "سینا ماشینی",
      presenterImage: "profile.jpg",
      keywords: ["OOP", "Grphql", "Programmig", "برنامه نویسی"],
      date: "2019-12-24T19:52:49",
      link: renderWebinarLink,
      endDate: "2019-12-24T19:52:49",
    },
    {
      id: "6545645646565",
      name: "وبینار برای گراف کیوال",
      image: "gph.png",
      presenter: "سینا ماشینی",
      presenterImage: "profile.jpg",
      keywords: ["OOP", "Grphql", "Programmig", "برنامه نویسی"],
      date: "2019-12-24T19:52:49",
      link: renderWebinarLink,
      endDate: "2019-12-24T19:52:49",
    },
    {
      id: "6545645646565",
      name: "وبینار برای گراف کیوال",
      image: "gph.png",
      presenter: "سینا ماشینی",
      presenterImage: "profile.jpg",
      keywords: ["OOP", "Grphql", "Programmig", "برنامه نویسی"],
      date: "2019-12-24T19:52:49",
      link: renderWebinarLink,
      endDate: "2019-12-24T19:52:49",
    },
    {
      id: "6545645646565",
      name: "وبینار برای گراف کیوال",
      image: "gph.png",
      presenter: "سینا ماشینی",
      presenterImage: "profile.jpg",
      keywords: ["OOP", "Grphql", "Programmig", "برنامه نویسی"],
      date: "2019-12-24T19:52:49",
      link: renderWebinarLink,
      endDate: "2019-12-24T19:52:49",
    },
    {
      id: "6545645646565",
      name: "وبینار برای گراف کیوال",
      image: "gph.png",
      presenter: "سینا ماشینی",
      presenterImage: "profile.jpg",
      keywords: ["OOP", "Grphql", "Programmig", "برنامه نویسی"],
      date: "2019-12-24T19:52:49",
      link: renderWebinarLink,
      endDate: "2019-12-24T19:52:49",
    },
  ];
  return <PresenterPageWebinars webinars={webinars} />;
};
