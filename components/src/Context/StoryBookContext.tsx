import React, {createContext} from 'react';

const defaultTranslate = () =>
{
    const t = (term:string) => term
    return {t};
}

const defaultChangeLanguage = (lng, direction) =>
{
    console.log(lng, direction);
}

const defaultContext = {
    translate: (): string | any => defaultTranslate(),
    changeLang: (lng: string, direction:string): any => defaultChangeLanguage("en", "ltr"),
    Languages: [{Locale:"fa", Direction:"rtl", title:"فارسی"} , {Locale:"en",Direction:"ltr", title:"English"}],
    defaultlang: "fa",
    defaultDir: "rtl"
  };

export let context = createContext(defaultContext);

export const createStoryBookContext = (props) => {
    context = createContext(props)
};

export const StoryBookContext = (props: any) =>
{
    return (
        <div>
             {props.children}
        </div>
    )
}

export default StoryBookContext;