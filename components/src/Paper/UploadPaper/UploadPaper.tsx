import React, { FC, useState, Dispatch, SetStateAction } from "react";
import {
  Box,
  Button,
  Typography,
  TextField,
  TextareaAutosize,
  IconButton,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import styled from "styled-components";
import HighlightOffRoundedIcon from "@material-ui/icons/HighlightOffRounded";
import LinearProgress from '@material-ui/core/LinearProgress';
import { ImageData } from "../../interfaces";


const Wrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  padding: ${(props) => props.theme.spacing(2)}px;
`;
const Content = styled(Box)`
  display: flex;
  flex-direction: column;
  padding: 0 ${(props) => props.theme.spacing(4)}px;
  & > * {
    margin-bottom: ${(props) => props.theme.spacing(3)}px;
  }
`;
const ButtonWrapper = styled(Box)`
  position: relative;
  display: flex;
  width: 100%;
  padding-top: 50%;
  border-radius: 6px;
  background-color: ${(props) => props.theme.palette.primary.main};
  overflow: hidden;
`;
const PreviewBox = styled(Box)`
  position: absolute;
  display: flex;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  border: 2px solid
    ${(props: theme) => (props.error && !props.media ? "red" : "unset")};
  box-sizing: border-box;
`;
const Label = styled.label`
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
`;
const Image = styled.img`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
`;
const Video = styled.video`
  position: absolute;
  width: 100%;
  height: 100%;
`;
const DeleteIcon = styled(IconButton)`
  position: absolute !important;
  width: 24px;
  height: 24px;
  top: 0;
  left: 0;
  background-color: #fff !important;
`;
const DescInput = styled(TextareaAutosize)`
  width: 100%;
  box-sizing: border-box;
  padding: ${(props) => props.theme.spacing(2)}px;
  border: 2px solid
    ${(props: theme) =>
      props.error && !props.value
        ? props.theme.palette.error.main
        : props.theme.palette.primary.main} !important;
  border-radius: ${(props) => props.theme.spacing()}px;
  resize: none;
  margin-top: ${(props) => props.theme.spacing(2)}px;
  transition: all 0.1s linear;
  &:hover {
    border: 2px solid rgba(0, 0, 0, 0.87);
  }
  &:focus {
    border: 2px solid rgba(0, 0, 0, 0.87);
  }
`;

const ButtonWrap = styled(Box)`
  display: flex;
  justify-content: space-between;
  & > * {
    flex-basis: 45%;
  }
`;


interface theme {
  error?: boolean;
  media?: ImageData;
  value?: string;
  theme: any;
}

export interface UploadPaperProps {
  uploadHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
  media: ImageData[];
  title: string;
  description: string;
  setTitle: Dispatch<SetStateAction<string>>;
  setDesc: Dispatch<SetStateAction<string>>;
  setUploadState: Dispatch<SetStateAction<string>>;
  setAttachmentHandler: (media: ImageData[], title:string, description:string, showMedia: boolean) => void;
  setOpenModal: Dispatch<SetStateAction<boolean>>;
  error: boolean;
  progress: number;
}

const UploadPaper: FC<UploadPaperProps> = ({ children, ...props }) => {
  const {
    uploadHandler,
    media,
    title,
    description,
    setTitle,
    setDesc,
    setUploadState,
    setAttachmentHandler,
    setOpenModal,
    error,
    progress
  } = props;
  const [_showMedia, setShowMedia] = useState(false);
  return (
    <Wrapper>
      <Content>
        <ButtonWrapper>
          <PreviewBox error={error} media={media[0]}>
            <input
              style={{ display: "none" }}
              accept={"video/*,image/*"}
              id="input-img"
              type="file"
              onChange={(e) => {
                setUploadState("attachments");
                uploadHandler(e);
                setShowMedia(true);
              }}
            />
            {/* TODO set progress when a file was uploaded */}
            
            <Label htmlFor="input-img">
              {!_showMedia && <AddIcon style={{ width: 50, height: 50 }} />}
            </Label>
            {media[0] &&
              _showMedia &&
              (media[0].type.indexOf("image") === 0 ? (
                <Image src={String(media[0].data)} />
              ) : (
                <Video
                  style={{ width: "100%" }}
                  src={String(media[0].data)}
                  controls
                ></Video>
              ))}
            {_showMedia && (
              <DeleteIcon onClick={() => setShowMedia(false)}>
                <HighlightOffRoundedIcon />
              </DeleteIcon>
            )}
          </PreviewBox>
        </ButtonWrapper>
        {progress > 0 && progress < 100 && <LinearProgress variant="determinate" value={progress} />}
        <TextField
          label="عنوان"
          value={title}
          onChange={(e) => {
            setTitle(e.target.value);
          }}
          error={error && !title ? true : false}
        />
        <Box style={{ paddingTop: 12 }}>
          <Typography>توضیحات</Typography>
          <DescInput
            value={description}
            rowsMin={3}
            onChange={(e) => setDesc(e.target.value)}
            error={error}
          />
        </Box>
      </Content>
      <ButtonWrap>
        <Button
          color="primary"
          variant="contained"
          onClick={() => {if(_showMedia) setAttachmentHandler(media, title, description, _showMedia)}}
        >
          آپلود
        </Button>
        <Button
          color="primary"
          variant="outlined"
          onClick={() => setOpenModal(false)}
        >
          لغو
        </Button>
      </ButtonWrap>
    </Wrapper>
  );
};

export default UploadPaper;
