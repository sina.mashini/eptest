import React, { useState } from "react";
import UploadPaperComponent from "./UploadPaper";
import { radios } from "@storybook/addon-knobs";
import { Grid } from "@material-ui/core";
import { ImageData } from "../../interfaces";

export default {
  title: "Section Components | Paper / UploadPaper",
  component: UploadPaperComponent,
};

export const UploadPaper = () => {
  
  const label = "Type";
  const option = {
    image: "image",
    video: "video",
  };
  const defaultValue = "image";
  const value = radios(label, option, defaultValue);

  const errorLabel = "Error";
  const errorOptions = {
    true: "1",
    false: "0",
  };
  const errorDefaultValue = "0";
  const errorValue = radios(errorLabel, errorOptions, errorDefaultValue);

  const [media, setMedia] = useState<Array<ImageData>>([]);
  const [mediaTitle, setMediaTitle] = useState("");
  const [mediaDescription, setMediaDescription] = useState("");

  const uploadHandler = (e) => {
    if (e.target.files && e.target.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        let item: ImageData = {
          _id: "1234",
          data: String(e.target?.result),
          name: "file-1",
          size: 1234,
          type: value,
        };
        let newArray: ImageData[] = Object.assign([], media);
        newArray = [item];
        setMedia(newArray);
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const setUploadState = () => {}
  const uploadAttachment = (media: ImageData[]) => {
    alert("media uploaded");
  };

  return (
    <Grid item xs={12} sm={6}>
      <UploadPaperComponent
        type={value}
        uploadHandler={uploadHandler}
        media={media}
        title={mediaTitle}
        description={mediaDescription}
        setTitle={setMediaTitle}
        setDesc={setMediaDescription}
        setUploadState={setUploadState}
        setAttachmentHandler={uploadAttachment}
        setOpenModal={() => {alert('modal will close')}}
        error={Boolean(Number(errorValue))}
      />
    </Grid>
  );
};
