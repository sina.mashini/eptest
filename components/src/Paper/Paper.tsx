import React, { FC } from "react";
import { Card, Box, Typography } from "@material-ui/core";
import styled from "styled-components";

const Wrapper = styled(Card)`
  flex-direction: column;
  padding: ${(props) => props.theme.spacing(4)}px
    ${(props) => props.theme.spacing(8)}px;
  background-color: #fff;
  @media all and (max-width: 728px) {
    padding: ${(props) => props.theme.spacing(2)}px;
  }
`;
const Content = styled(Box)`
  display: flex;
  flex: 1;
`;

export interface PaperProps {
  title?: string;
}

export const Paper: FC<PaperProps> = ({ children, ...props }) => {
  const { title } = props;
  return (
    <Wrapper>
      {title && (
        <Typography style={{ marginBottom: 18 }} variant="h5" align="center">
          {title}
        </Typography>
      )}
      <Content>{children}</Content>
    </Wrapper>
  );
};

export default Paper;
