import { Dispatch, SetStateAction } from "react";

export interface CreateWebinarPaper {
  items: PaperItems;
}

export interface PaperItems {
  activeStep: number;
  steps: string[];
  stepComponent: (stepIndex: number) => JSX.Element;
  handleNext: () => void;
  handleBack: () => void;
}

export interface Steper {
  activeStep: number;
  steps: string[];
}

export interface SteperButtons {
  activeStep: number;
  steps: string[];
  handleNext: () => void;
  handleBack: () => void;
}

export interface BaseInfoItems {
  title: string;
  description: string;
  tags: string[];
}

export interface ImageData {
  data: string | null;
  name: string;
  size: number;
  type: string;
  _id: string;
}

export interface BaseInfo {
  title: string;
  description: string;
  tags: string[];
  tagInput: string;
  setBaseInfoItems: (
    name: "title" | "description" | "tags",
    value: string
  ) => void;
  uploadHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
  setUploadState: Dispatch<SetStateAction<string>>;
  webinarBanner: ImageData[];
  summeryFile: ImageData[];
  progress: number;
  error: boolean;
  addTag: (event: React.KeyboardEvent<HTMLDivElement>) => void;
  deleteTag: (index: number) => void;
}

export interface UploadSummeryFileProps {
  summeryFile: ImageData[];
  uploadHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
  progress: number;
  setUploadState: Dispatch<SetStateAction<string>>;
}

export interface DatePicker {
  startDate: Date;
  setStartDate: Dispatch<SetStateAction<Date>>;
  endDate: Date;
  setEndDate: Dispatch<SetStateAction<Date>>;
  error: boolean;
  setError: Dispatch<SetStateAction<boolean>>;
}

export interface DateModal {
  open: boolean;
  closeModal: () => void;
  value: Date;
  onChange: (date: Date | null) => void;
}

export interface Presenter {
  title: string;
  id: string;
  image: string;
}

export interface SearchPresenters {
  inputValue: string;
  setInputValue: Dispatch<SetStateAction<string>>;
  options: Presenter[];
  loading: boolean;
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  addPresenter: (presenter: Presenter) => void;
  presenters?: Presenter[];
  setOpenCreateModal: Dispatch<SetStateAction<boolean>>;
}

export interface PresenterItem {
  presenter: Presenter;
  addPresenter: (presenter: Presenter) => void;
  presenters?: Presenter[];
}

export interface SelectedPresenter {
  presenter: Presenter;
  deletePresenter: (presenter: Presenter) => void;
}

export interface SelectedPresenters {
  presenters: Presenter[];
  deletePresenter: (presenter: Presenter) => void;
}

export interface ThemeProps {
  show?: boolean;
  error?: boolean;
  value?: string;
}

export interface CreatePresenter {
  title: string;
  fieldOfStudy: string;
  affiliation: string;
  biography: string;
  presenterImage: ImageData[];
  error: boolean;
  setItems: (name: string, value: string) => void;
  uploadHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
  setUploadState: Dispatch<SetStateAction<string>>;
  progress: number;
  createPresenter: () => void;
  setOpen: Dispatch<SetStateAction<boolean>>;
}

export interface MediaAttachmentsProps {
  uploadHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
  media: ImageData[];
  title: string;
  description: string;
  setTitle: Dispatch<SetStateAction<string>>;
  setDescription: Dispatch<SetStateAction<string>>;
  setUploadState: Dispatch<SetStateAction<string>>;
  setAttachmentHandler: (media: ImageData[], title:string, description:string, showMedia: boolean) => void;
  openModal: boolean;
  setOpenModal: Dispatch<SetStateAction<boolean>>;
  error: boolean;
  progress: number;
}

export interface AttachmentFiles {
  title: string;
  description: string;
  kind: string;
  format: string;
  media: string;
  data: string | null;
  duration: string | null;
}

export interface SelectedAttachmentsProps {
  attachments: AttachmentFiles[];
  deleteAttachedMedia: (index: number) => void;
}

export interface WebinarRoom {
  link: string;
  setLink: Dispatch<SetStateAction<string>>;
  error: boolean;
  setError: Dispatch<SetStateAction<boolean>>;
}
