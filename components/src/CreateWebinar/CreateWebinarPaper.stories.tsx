import React, { useState } from "react";
import CreateWebinarPaper from "./CreateWebinarPaper";
import { PaperItems } from "./interfaces";

export default {
  title: "Section Components | CreateWebinar / CreateWebinarPaper",
  component: CreateWebinarPaper,
};

const steps = [
  "اطلاعات پایه",
  "انتخاب تاریخ",
  "اطلاعات ارائه دهنده",
  "بارگذاری مدیا",
  "مکان وبینار",
];

const stepComponent = (index: number) => {
  switch (index) {
    case 0:
      return <p>step 1</p>;
    case 1:
      return <p>step 2</p>;
    case 2:
      return <p>step 3</p>;
    case 3:
      return <p>step 4</p>;
    case 4:
      return <p>step 5</p>;
    default:
      return <p>no step</p>;
  }
};

export const CreateWebinar = () => {
  const [activeStep, setActiveStep] = useState(0);

  const handleNext = () => {
    if (activeStep < steps.length - 1) {
      setActiveStep(activeStep + 1);
    }
    if(activeStep === steps.length - 1) {
      alert('Webinar created')
    }
  };

  const handleBack = () => {
    if (activeStep >= 0) {
      setActiveStep(activeStep - 1);
    }
  };

  const items: PaperItems = {
    activeStep: activeStep,
    stepComponent: stepComponent,
    steps: steps,
    handleNext: handleNext,
    handleBack: handleBack,
  };

  return <CreateWebinarPaper items={items} />;
};
