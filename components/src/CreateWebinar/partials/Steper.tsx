import React, { FC } from "react";
import { Stepper, Step, StepLabel } from "@material-ui/core";
import { Steper } from '../interfaces';

export const SteperComponent: FC<Steper> = (props) => {
    const {activeStep, steps} = props;
  return (
    <Stepper activeStep={activeStep} alternativeLabel style={{width: '100%'}}>
      {steps.map((label) => (
        <Step key={label}>
          <StepLabel>{label}</StepLabel>
        </Step>
      ))}
    </Stepper>
  );
};

export default SteperComponent;
