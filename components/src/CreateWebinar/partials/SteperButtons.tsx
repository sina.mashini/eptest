import React, { FC } from "react";
import styled from "styled-components";
import { Grid, Button } from "@material-ui/core";
import { SteperButtons } from "../interfaces";

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
`;
const ButtonsGrid = styled(Grid)`
  display: flex;
  width: 100%;
  justify-content: space-between;
  & > * {
    flex-basis: 50%;
    &:first-child {
      margin-left: 6px;
    }
    &:last-child {
      margin-right: 6px;
    }
  }
`;

export const SteperButtonsComponent: FC<SteperButtons> = (props) => {
  const { activeStep, steps, handleNext, handleBack } = props;

  return (
    <ButtonsWrapper>
      <ButtonsGrid item sm={6} xs={12}>
        <Button
          color="primary"
          variant="outlined"
          disabled={activeStep === 0}
          onClick={handleBack}
        >
          قبلی
        </Button>
        <Button variant="contained" color="primary" onClick={handleNext}>
          {activeStep === steps.length - 1 ? "ثبت" : "بعدی"}
        </Button>
      </ButtonsGrid>
    </ButtonsWrapper>
  );
};

export default SteperButtonsComponent;
