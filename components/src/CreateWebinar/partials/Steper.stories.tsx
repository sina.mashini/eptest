import React from "react";
import WebinarSteper from "./Steper";
import { radios } from "@storybook/addon-knobs";
import { text } from "@storybook/addon-knobs";
import { useEffect } from "@storybook/addons";

export default {
  title: "Atoms | Steper / Steper",
  component: WebinarSteper,
};

export const Steper = () => {
  //knobs for select step of steper
  const stepLabel = "Active Step?";
  const stepOptions = {
    one: "0",
    two: "1",
    three: "2",
    four: "3",
    five: "4"
  };
  const stepDefaultValue = "1";
  const activeStep = radios(stepLabel, stepOptions, stepDefaultValue);

  //change title of step one
  const stepOneLabel = "Step One:";
  const stepOneDefaultValue = "اطلاعات پایه";
  const stepOnevalue = text(stepOneLabel, stepOneDefaultValue);

  //change title of step two
  const stepTwoLabel = "Step Two:";
  const stepTwoDefaultValue = "انتخاب تاریخ";
  const stepTwovalue = text(stepTwoLabel, stepTwoDefaultValue);

  //change title of step three
  const stepThreeLabel = "Step Three:";
  const stepThreeDefaultValue = "اطلاعات ارائه دهنده";
  const stepThreevalue = text(stepThreeLabel, stepThreeDefaultValue);

  //change title of step four
  const stepFourLabel = "Step Four:";
  const stepFourDefaultValue = "بارگذاری مدیا";
  const stepFourvalue = text(stepFourLabel, stepFourDefaultValue);

  //change title of step five
  const stepFiveLabel = "Step Five:";
  const stepFiveDefaultValue = "مکان وبینار";
  const stepFivevalue = text(stepFiveLabel, stepFiveDefaultValue);

  let steps: string[] = [
    stepOnevalue,
    stepTwovalue,
    stepThreevalue,
    stepFourvalue,
    stepFivevalue,
  ];

  useEffect(() => {
    steps = [
      stepOnevalue,
      stepTwovalue,
      stepThreevalue,
      stepFourvalue,
      stepFivevalue,
    ];
  }, [
    stepOnevalue,
    stepTwovalue,
    stepThreevalue,
    stepFourvalue,
    stepFivevalue,
  ]);

  return <WebinarSteper activeStep={Number(activeStep)} steps={steps} />;
};
