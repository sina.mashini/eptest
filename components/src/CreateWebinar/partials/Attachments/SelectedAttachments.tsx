import React, { FC, useContext } from "react";
import { SelectedAttachmentsProps } from "../../interfaces";
import { Typography, Grid, Box, Paper, IconButton } from "@material-ui/core";
import styled from "styled-components";
import { context } from "../../../Context/StoryBookContext";
import HighlightOffRoundedIcon from "@material-ui/icons/HighlightOffRounded";

const Wrapper = styled(Grid)`
  display: flex;
  flex-direction: column;
  margin-bottom: ${props => props.theme.spacing(4)}px;
`;
const MediaWrap = styled(Grid)`
  display: flex;
  flex-direction: column;
`;
const MediaList = styled(Grid)`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  justify-content: space-evenly;
`;
const MediaPaper = styled(Paper)`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 125px;
  overflow: hidden;
  margin-bottom: ${props => props.theme.spacing(2)}px;
`;
const MediaLayout = styled(Box)`
  display: flex;
  width: 100%;
  padding-top: 100%;
  position: relative;
  & > * {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
  }
`;
const DeleteIcon = styled(IconButton)`
  position: absolute !important;
  width: 24px;
  height: 24px;
  top: 0;
  left: 0;
  background-color: #fff !important;
`;

export const SelectedAttachments: FC<SelectedAttachmentsProps> = (props) => {
  const { attachments, deleteAttachedMedia } = props;
  const { translate } = useContext(context);
  const { t } = translate();

  return (
    <Wrapper>
      <MediaWrap>
        <Typography style={{margin: "18px 0"}}>{t("لیست عکس ها")}</Typography>
        <MediaList>
          {attachments.map(
            (image, index) =>
              image.kind === "image" && image.data && (
                <MediaPaper>
                  <MediaLayout>
                    <img src={image.data} />
                    <DeleteIcon onClick={() => deleteAttachedMedia(index)}>
                      <HighlightOffRoundedIcon />
                    </DeleteIcon>
                  </MediaLayout>
                </MediaPaper>
              )
          )}
        </MediaList>
      </MediaWrap>
      <MediaWrap>
        <Typography style={{margin: "18px 0"}}>{t("لیست فیلم ها")}</Typography>
        <MediaList>
          {attachments.map(
            (video, index) =>
              video.kind === "video" && (
                <MediaPaper>
                  <MediaLayout>
                    <video src={String(video.data)} controls></video>
                    <DeleteIcon onClick={() => deleteAttachedMedia(index)}>
                      <HighlightOffRoundedIcon />
                    </DeleteIcon>
                  </MediaLayout>
                </MediaPaper>
              )
          )}
        </MediaList>
      </MediaWrap>
    </Wrapper>
  );
};

export default SelectedAttachments;
