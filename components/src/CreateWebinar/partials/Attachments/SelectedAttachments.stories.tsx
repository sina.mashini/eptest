import React from "react";
import SelectedAttachmentsComponent from "./SelectedAttachments";
import { AttachmentFiles } from '../../interfaces';

export default {
  title: "Section Components | CreateWebinar / Step Four / SelectedAttachments",
  component: SelectedAttachmentsComponent,
};

export const SelectedAttachments = () => {
  const deleteItem = (index) => {
    alert(`file index:${index} deleted`);
  };

  const attachments : AttachmentFiles[] = [
    {
      title: "فایل ۱",
      description: "حاوی فایل ۱",
      kind: "image",
      format: "jpg",
      media: '1234',
      data: "profile.jpg",
    },
    {
      title: "فایل ۱",
      description: "حاوی فایل ۱",
      kind: "video",
      format: "mp4",
      media: '12345',
      data:
        "https://hw5.cdn.asset.aparat.com/aparat-video/9d7d7dd88af07786ba90406c743048b414501458-144p.mp4",
    },
  ];

  return (
    <SelectedAttachmentsComponent
      deleteAttachedMedia={deleteItem}
      attachments={attachments}
    />
  );
};
