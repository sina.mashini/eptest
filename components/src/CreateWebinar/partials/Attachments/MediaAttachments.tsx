import React, { FC, useContext } from "react";
import styled from "styled-components";
import { Grid } from "@material-ui/core";
import UploadButton from "../../../Buttons/UploadButton/UploadButton";
import { context } from "../../../Context/StoryBookContext";
import AddPhotoAlternateOutlinedIcon from "@material-ui/icons/AddPhotoAlternateOutlined";
import { MediaAttachmentsProps } from "../../interfaces";

const UploadBtnWrap = styled(Grid)`
  display: flex;
  justify-content: space-evenly;
`;

export const MediaAttachments: FC<MediaAttachmentsProps> = (props) => {
  const {
    media,
    uploadHandler,
    title,
    description,
    setTitle,
    setDescription,
    setUploadState,
    setAttachmentHandler,
    openModal,
    setOpenModal,
    error,
    progress
  } = props;
  const { translate } = useContext(context);
  const { t } = translate();

  return (
    <UploadBtnWrap>
      <UploadButton
        title={t("آپلود عکس یا ویدیو")}
        icon={<AddPhotoAlternateOutlinedIcon />}
        uploadHandler={uploadHandler}
        media={media}
        mediaTitle={title}
        mediaDesc={description}
        progress = {progress}
        setMediaTitle={setTitle}
        setMediaDesc={setDescription}
        setUploadState={setUploadState}
        setAttachmentHandler={setAttachmentHandler}
        openModal={openModal}
        setOpenModal={setOpenModal}
        error={error}
      />
    </UploadBtnWrap>
  );
};

export default MediaAttachments;
