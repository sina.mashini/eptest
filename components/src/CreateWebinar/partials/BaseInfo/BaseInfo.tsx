import React, { FC, useContext } from "react";
import {
  TextField,
  Box,
  Typography,
  TextareaAutosize,
  Chip,
  Grid,
} from "@material-ui/core";
import { BaseInfo } from "../../interfaces";
import styled from "styled-components";
import UploadSummeryFile from "./UploadSummeryFile";
import UploadImageComponent from "../../../Upload/UploadImage/UploadImage";
import { context } from "../../../Context/StoryBookContext";

const BaseInfoWrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  width: 100%;
  & > * {
    margin-bottom: 6px !important;
  }
`;
const TagsWrap = styled(Grid)`
  display: flex;
  flex-wrap: wrap;
`;
const Tags = styled(Grid)`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  align-items: center;
  padding: ${(props) => props.theme.spacing(2)}px;
`;

interface props {
  error: boolean;
  title: string;
}

const ItemsWrapper = styled.div`
  padding-top: 18px;
  & > p {
    color: ${(props: props) => {
      if (props.error && !props.title) {
        return "red";
      } else return "inherit";
    }};
  }
  & > textarea {
    border: ${(props: props) => {
      if (props.error && !props.title) return "2px solid red";
    }};
    &:hover {
      border: ${(props: props) => {
        if (props.error && !props.title) return "2px solid red";
      }};
    }
    &:focus {
      border: ${(props: props) => {
        if (props.error && !props.title) return "2px solid red";
      }};
    }
  }
`;

const DescriptionInput = styled(TextareaAutosize)`
  font-family: "IRANSans";
  width: 100%;
  padding: 12px;
  border: 1px solid ${(props) => props.theme.palette.secondary.main};
  border-radius: 6px;
  resize: none;
  margin-top: 12px;
  outline: none;
  transition: all 0.1s linear;
  box-sizing: border-box;
  &:hover {
    border: 2px solid rgba(0, 0, 0, 0.87);
  }
  &:focus {
    border: 2px solid ${(props) => props.theme.palette.primary.main};
  }
`;

export const BaseInfoComponent: FC<BaseInfo> = (props) => {
  const {
    title,
    description,
    tags,
    tagInput,
    setBaseInfoItems,
    uploadHandler,
    setUploadState,
    webinarBanner,
    summeryFile,
    progress,
    error,
    addTag,
    deleteTag,
  } = props;

  const { translate } = useContext(context);
  const { t } = translate();

  return (
    <BaseInfoWrapper>
      {error && (
        <span style={{ color: "red" }}>
          {t("* پر کردن موارد ستاره دار الزامی است")}
        </span>
      )}
      <TextField
        required
        label={t("عنوان")}
        value={title}
        name="title"
        onChange={(e) => {
          setBaseInfoItems("title", e.target.value);
        }}
        error={error && !title ? true : false}
      />
      <ItemsWrapper error={error} title={title}>
        <Typography>{t("توضیحات*")}</Typography>
        <DescriptionInput
          value={description}
          rowsMin={6}
          onChange={(e) => setBaseInfoItems("description", e.target.value)}
        />
      </ItemsWrapper>
      <TagsWrap>
        <Grid item sm={12} md={6}>
          <TextField
            required
            error={error && tags.length === 0 ? true : false}
            label={t("کلیدواژه ها")}
            value={tagInput}
            onChange={(e) => setBaseInfoItems("tags", e.target.value)}
            onKeyPress={addTag}
            style={{ width: "100%" }}
          />
        </Grid>
        <Tags item sm={12} md={6}>
          {tags.map((tag, index) => (
            <Chip
              color="primary"
              label={tag}
              variant="outlined"
              onDelete={() => deleteTag(index)}
              key={index}
              style={{marginBottom: 6}}
            />
          ))}
        </Tags>
      </TagsWrap>
      <UploadImageComponent
        title={t("کاور وبینار")}
        image={webinarBanner}
        uploadHandler={uploadHandler}
        setUploadState={setUploadState}
        state="webinarBanner"
        progress={progress}
        error={error}
        required={true}
      />
      <UploadSummeryFile
        summeryFile={summeryFile}
        uploadHandler={uploadHandler}
        progress={progress}
        setUploadState={setUploadState}
      />
    </BaseInfoWrapper>
  );
};

export default BaseInfoComponent;
