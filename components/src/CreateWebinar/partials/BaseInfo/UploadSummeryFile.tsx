import React, { FC, useState, useEffect } from "react";
import { Typography, Box, CircularProgress } from "@material-ui/core";
import AddPhotoAlternateOutlinedIcon from "@material-ui/icons/AddPhotoAlternateOutlined";
import styled from "styled-components";
import { Document, Page } from "react-pdf";
import { UploadSummeryFileProps } from "../../interfaces";

const Wrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  margin-top: ${(props) => props.theme.spacing(3)}px;
`;
const TitleWrap = styled.div`
  display: flex;
`;
const Label = styled.label`
  display: flex;
  justify-content: center;
  align-items: center;
`;
const MyDiv = styled.div`
  position: relative;
  width: 135px;
  height: 191px;
  margin-top: 18px;
  & > div {
    height: 100%;
    & > div {
      height: 100%;
      & > * {
        position: absolute;
        width: 100% !important;
        height: 100% !important;
        top: 0;
        left: 0;
      }
    }
  }
`;
const Loading = styled(CircularProgress)`
  color: ${(props) => props.theme.palette.primary.dark} !important;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%) rotate(-90deg) !important;
`;

export const UploadSummeryFile: FC<UploadSummeryFileProps> = (props) => {
  const {
    summeryFile,
    uploadHandler,
    progress,
    setUploadState,
  } = props;
  const [_loading, setLoading] = useState(false);

  useEffect(() => {
    if (progress === 100) {
      setLoading(false);
    }
  }, [progress]);

  return (
    <Wrapper>
      <TitleWrap>
        <Typography style={{ marginLeft: 16 }}>فایل خلاصه وبینار:</Typography>
        <input
          style={{ display: "none" }}
          accept=".pdf"
          id="input-file"
          type="file"
          multiple
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            uploadHandler(e);
            setUploadState("summeryFile");
            setLoading(true);
          }}
        />
        <Label htmlFor="input-file">
          <AddPhotoAlternateOutlinedIcon />
        </Label>
      </TitleWrap>
      <MyDiv>
        {progress === 100 && summeryFile[0] && (
          <Document file={summeryFile[0].data}>
            <Page pageNumber={1} />
          </Document>
        )}
        {_loading && (
          <Loading value={progress} variant="static" />
        )}
      </MyDiv>
    </Wrapper>
  );
};

export default UploadSummeryFile;
