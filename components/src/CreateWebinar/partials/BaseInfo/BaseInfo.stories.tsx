import React, { useState } from "react";
import WebinarBaseInfo from "./BaseInfo";
import { radios } from "@storybook/addon-knobs";
import styled from "styled-components";
import { Grid } from "@material-ui/core";
import { ImageData } from "../../../interfaces";

export default {
  title: "Section Components | CreateWebinar / Step One",
  component: WebinarBaseInfo,
};

const Wrapper = styled(Grid)`
  display: flex;
  flex-direction: column;
  flex: 1;
  align-items: center;
`;

export const BaseInfo = () => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [tagInput, setTagInput] = useState("");
  const [tags, setTags] = useState<Array<string>>(["React", "storyBook", "Next"]);
  const img = [
    {
      _id: "1234",
      data: "profile.jpg",
      name: "file-1",
      size: 1234,
      type: "image",
    },
  ];
  const [pdf, setPdf] = useState<Array<ImageData>>([]);

  const ErrorLabel = "Error ?";
  const ErrorOptions = {
    true: "1",
    false: "0",
  };
  const ErrorDefaultValue = "0";
  const error = radios(ErrorLabel, ErrorOptions, ErrorDefaultValue);

  const progressLabel = "Progress";
  const progressOptions = {
    "0%": "0",
    "100%": "100",
  };
  const progressDefaultValue = "0";
  const progressValue = radios(
    progressLabel,
    progressOptions,
    progressDefaultValue
  );

  const setBaseInfoItems = (
    name: "title" | "description" | "tags",
    value: string
  ) => {
    if (typeof value === "string") {
      if (name === "title") {
        setTitle(value);
      } else if (name === "description") {
        setDescription(value);
      }
    } else if (name === "tags") {
      setTagInput(value);
    }
  };

  const uploadHandler = (e) => {
    if (e.target.files && e.target.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        let item: ImageData[] = [{
          _id: '1234',
          data: String(e.target?.result),
          name: 'file-1',
          size: 1234,
          type: "pdf"
        }];
        setPdf(item);
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  const setUploadState = () => {};
  const addTag = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.charCode == 13) {
      const newTags = Object.assign([], tags);
      newTags.push(tagInput);
      setTags(newTags);
      setTagInput("");
    }
  };
  const deleteTag = (index: number) => {
    const newTags = Object.assign([], tags);
    newTags.splice(index, 1);
    setTags(newTags);
  };

  return (
    <Wrapper xs={12} sm={6}>
      <WebinarBaseInfo
        title={title}
        description={description}
        tags={tags}
        tagInput={tagInput}
        setBaseInfoItems={setBaseInfoItems}
        uploadHandler={uploadHandler}
        setUploadState={setUploadState}
        webinarBanner={img}
        summeryFile={pdf}
        progress={Number(progressValue)}
        error={Boolean(Number(error))}
        addTag={addTag}
        deleteTag={deleteTag}
      />
    </Wrapper>
  );
};
