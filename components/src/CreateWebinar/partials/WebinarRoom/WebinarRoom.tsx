import React, { FC, useContext } from "react";
import { WebinarRoom } from "../../interfaces";
import { Box, TextField, Grid } from "@material-ui/core";
import { context } from "../../../Context/StoryBookContext";

export const WebinarRoomComponent: FC<WebinarRoom> = (props) => {
  const { link, setLink, error, setError } = props;

  const { translate } = useContext(context);
  const { t } = translate();

  return (
    <Box>
      {error && (
        <span style={{ color: "red", marginBottom: 18 }}>
          {t("*  پر کردن این فیلد الزامی است")}
        </span>
      )}
      <Grid item sm={12} md={8}>
        <TextField
          required
          label={t("محل وبینار")}
          value={link}
          style={{ width: "100%" }}
          onChange={(e) => {
            setLink(e.target.value);
            setError(false);
          }}
          error={error && !link ? true : false}
        />
      </Grid>
    </Box>
  );
};

export default WebinarRoomComponent;
