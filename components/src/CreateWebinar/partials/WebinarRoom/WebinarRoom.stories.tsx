import React, { useState } from "react";
import WebinarRoomComponent from "./WebinarRoom";
import { radios } from '@storybook/addon-knobs';

export default {
  title: "Section Components | CreateWebinar / Step Five",
  component: WebinarRoomComponent,
};

export const WebinarRoom = () => {
  const [link, setLink] = useState("");
  const errorLabel = "Error?";
  const options = {
    true: "1",
    false: "0",
  };
  const errorDefaultValue = "0";
  const error = radios(errorLabel, options, errorDefaultValue);
  return (
    <WebinarRoomComponent
      link={link}
      setLink={setLink}
      error={Boolean(Number(error))}
      setError={() => {}}
    />
  );
};
