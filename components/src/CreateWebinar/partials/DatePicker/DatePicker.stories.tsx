import React, { useState } from "react";
import WebinarDatePicker from "./DatePicker";
import { Grid, makeStyles } from '@material-ui/core';
import { radios } from '@storybook/addon-knobs';

const useStyles = makeStyles((theme) => ({
  columnDir: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
  },
}))

export default {
  title: "Section Components | CreateWebinar / Step Two",
  component: WebinarDatePicker,
};

export const DatePicker = () => {
  const classes = useStyles();
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const ErrorLabel = "Error ?";
  const ErrorOptions = {
    true: "1",
    false: "0",
  };
  const ErrorDefaultValue = "0";
  const error = radios(ErrorLabel, ErrorOptions, ErrorDefaultValue);

  return (
    <Grid
      item
      className={classes.columnDir}
      style={{ flex: 1 }}
      sm={6}
      xs={12}
    >
      <WebinarDatePicker
        startDate={startDate}
        setStartDate={setStartDate}
        endDate={endDate}
        setEndDate={setEndDate}
        error={Boolean(Number(error))}
        setError={() => {}}
      />
    </Grid>
  );
};
