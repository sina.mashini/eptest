import React, {FC} from "react";
import { ModalComponent } from '../../../ModalComponent';
import { DateTimePicker } from '@material-ui/pickers';
import { DateModal } from '../../interfaces';

const DateModalComponent: FC<DateModal> = (props) => {
    const {open, closeModal, value, onChange} = props;
    return (
        <ModalComponent open={open} closeModal={closeModal}>
            <DateTimePicker
            ampm={false}
            autoOk
            value={value}
            onChange={onChange}
            disablePast
            open={open}
            onClose={closeModal}
          />
        </ModalComponent>
    )
}

export default DateModalComponent;