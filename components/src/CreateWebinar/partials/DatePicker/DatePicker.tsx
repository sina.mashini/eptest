import React, { FC, useState, useContext } from "react";
import { Box, Typography } from "@material-ui/core";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import DateModalComponent from "./DateModal";
import styled from "styled-components";
import { DatePicker } from "../../interfaces";
import { context } from "../../../Context/StoryBookContext";

const DateWrap = styled(Box)`
  display: flex;
  flex-direction: column;
  margin-bottom: ${(props) => props.theme.spacing()}px;
`;
const DateItem = styled(Box)`
  display: flex;
  align-items: center;
  margin-bottom: ${(props) => props.theme.spacing(2)}px;
  & > * {
    &:first-child {
      margin-right: ${(props) => props.theme.spacing(2)}px;
    }
  }
`;
const DateText = styled(Typography)`
  margin-right: ${(props) => props.theme.spacing(2)}px !important;
  padding: ${(props) => props.theme.spacing(2)}px;
  border: 1px solid ${(props) => props.theme.palette.secondary.main};
  border-radius: ${(props) => props.theme.spacing()}px;
  min-width: 50;
  cursor: pointer;
`;

export const DatePickerComponent: FC<DatePicker> = (props) => {
  const {
    startDate,
    setStartDate,
    endDate,
    setEndDate,
    error,
    setError,
  } = props;
  const [_open, setOpen] = useState(false);
  const [_dayValue, setDayValue] = useState(startDate);
  const [_dateName, setDateName] = useState("startDate");

  const { translate } = useContext(context);
  const { t } = translate();

  const openDateModalFunc = (name: "startDate" | "endDate") => {
    if (name === "startDate") {
      setDateName("startDate");
      setDayValue(startDate);
    } else {
      setDateName("endDate");
      setDayValue(endDate);
    }
    setOpen(true);
  };

  const changeDatehandler = (day: Date | null, dateName: string) => {
    if (day) {
      if (dateName === "startDate") {
        setStartDate(day);
        setDayValue(day);
      } else {
        if (day > startDate) {
          setEndDate(day);
          setError(false);
        }
      }
    }
  };

  const closeModal = () => {
    setOpen(false);
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Box>
        {error && (
          <span style={{ color: "red", marginBottom: 18 }}>
            {t("* تاریخ پایان باید از تاریخ شروع بیشتر باشد")}
          </span>
        )}
        <Typography style={{ marginBottom: 12 }}>تاریخ وبینار</Typography>
        <DateWrap>
          <DateItem>
            <Typography>تاریخ شروع :</Typography>
            <DateText
              align="center"
              onClick={() => openDateModalFunc("startDate")}
            >
              {startDate.toLocaleString()}
            </DateText>
          </DateItem>
          <DateItem>
            <Typography>تاریخ پایان :</Typography>
            <DateText
              align="center"
              onClick={() => openDateModalFunc("endDate")}
            >
              {endDate.toLocaleString()}
            </DateText>
          </DateItem>
        </DateWrap>
        <DateModalComponent
          open={_open}
          closeModal={closeModal}
          value={_dayValue}
          onChange={(date: Date | null) => changeDatehandler(date, _dateName)}
        />
      </Box>
    </MuiPickersUtilsProvider>
  );
};

export default DatePickerComponent;
