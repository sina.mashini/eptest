import React from "react";
import PresenterItemComponent from "./PresenterItem";
import { Presenter } from "../../interfaces";
import styled from "styled-components";

const ItemWrap = styled.div`
  width: 300px;
`;

export default {
  title: "Section Components | CreateWebinar / Step Three / PresenterItem",
  component: PresenterItemComponent,
};

export const SearchPresenters = () => {
  const presenter: Presenter = {
    title: "سینا ماشینی",
    id: "1234",
    image: "profile.jpg",
  };
  const addPresenter = () => {alert('presenter added')};
  return (
    <ItemWrap>
      <PresenterItemComponent
        presenter={presenter}
        addPresenter={addPresenter}
      />
    </ItemWrap>
  );
};
