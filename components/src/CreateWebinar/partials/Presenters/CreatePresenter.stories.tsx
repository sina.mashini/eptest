import React, { useState } from "react";
import CreatePresenterComponent from "./CreatePresenter";
import { radios } from "@storybook/addon-knobs";
import { ModalComponent } from "../../../ModalComponent/ModalComponent";

export default {
  title: "Section Components | CreateWebinar / Step Three / CreatePresenter",
  component: CreatePresenterComponent,
};

export const CreatePresenter = () => {
  const [title, setTitle] = useState("");
  const [fieldOfStudy, setFieldOfStudy] = useState("");
  const [affiliation, setAffiliation] = useState("");
  const [biography, setBiography] = useState("");
  const profileImage = [
    {
      _id: "1234",
      data: "profile.jpg",
      name: "file-1",
      size: 1234,
      type: "image",
    },
  ];
  const progressLabel = "Progress";
  const progressOptions = {
    "0%": "0",
    "100%": "100",
  };
  const progressDefaultValue = "0";
  const progressValue = radios(
    progressLabel,
    progressOptions,
    progressDefaultValue
  );

  const setItems = (name: string, value: string | ArrayBuffer) => {
    if (name === "title" && typeof value === "string") {
      setTitle(value);
    }
    if (name === "fieldOfStudy" && typeof value === "string") {
      setFieldOfStudy(value);
    }
    if (name === "affiliation" && typeof value === "string") {
      setAffiliation(value);
    }
    if (name === "biography" && typeof value === "string") {
      setBiography(value);
    }
  };

  const errorLabel = "Error?";
  const options = {
    true: "1",
    false: "0",
  };
  const errorDefaultValue = "0";
  const error = radios(errorLabel, options, errorDefaultValue);

  const uploadHandler = (e) => {};

  return (
    <ModalComponent
      style={{ direction: "rtl" }}
      open={true}
      closeModal={() => {}}
    >
      <CreatePresenterComponent
        title={title}
        fieldOfStudy={fieldOfStudy}
        affiliation={affiliation}
        biography={biography}
        presenterImage={profileImage}
        error={Boolean(Number(error))}
        setItems={setItems}
        uploadHandler={uploadHandler}
        setUploadState={() => {}}
        progress={Number(progressValue)}
        createPresenter={() => {
          alert("presenter created");
        }}
        setOpen={() => {alert("Modal will close");}}
      />
    </ModalComponent>
  );
};
