import React, { FC, useState } from "react";
import styled from "styled-components";
import { SelectedPresenter, ThemeProps } from "../../interfaces";
import {
  Card,
  CardMedia,
  CardContent,
  Typography,
  IconButton,
  Box,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

const ItemWrap = styled(Card)`
  width: 100px;
  position: relative;
  margin-left: ${props => props.theme.spacing(3)}px;
  &:last-child {
      margin-left: 0;
  }
`;
const DeleteWrap = styled(Box)`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  opacity: ${(props: ThemeProps) => (props.show ? 1 : 0)};
  visibility: ${(props) => (props.show ? "visible" : "hidden")};
  transition: all 0.25s linear;
`;
const Backdrop = styled(Box)`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  opacity: 0.3;
  background-color: #000;
`;
const ButtonWrap = styled(IconButton)`
    border-radius: 50% !important;
    border: 2px solid ${props => props.theme.palette.error.main} !important;
    & > span svg {
        color: ${props => props.theme.palette.error.main} !important;
    }
`
const ItemTitle = styled(CardContent)`
    display: flex;
    justify-content: center;
    padding: ${props => props.theme.spacing()}px !important;
`

export const SelectedPresenterItemComponent: FC<SelectedPresenter> = (
  props
) => {
  const { presenter, deletePresenter } = props;
  const [show, setShow] = useState(false);

  return (
    <ItemWrap
      onMouseEnter={() => setShow(true)}
      onMouseLeave={() => setShow(false)}
    >
      {open && (
        <DeleteWrap show={show}>
          <Backdrop></Backdrop>
          <ButtonWrap onClick={() => deletePresenter(presenter)}>
            <DeleteIcon />
          </ButtonWrap>
        </DeleteWrap>
      )}
      <CardMedia
        component="img"
        alt={`presenter${presenter.id}`}
        height="140"
        image={`https://edupack.nightly.repo.avidcloud.io/api/v1/blob/${presenter.image}`}
        title={presenter.title}
      />
      <ItemTitle>
        <Typography gutterBottom noWrap variant="body1" >
          {presenter.title}
        </Typography>
      </ItemTitle>
    </ItemWrap>
  );
};

export default SelectedPresenterItemComponent;
