import React, { useState } from "react";
import SearchPresentersComponent from "./SearchPresenters";
import { Presenter } from "../../interfaces";
import { text } from "@storybook/addon-knobs";
import styled from "styled-components";
import { Box } from '@material-ui/core';

const Wrapper = styled(Box)`
  display: flex;
`

export default {
  title: "Section Components | CreateWebinar / Step Three / SearchPresenter",
  component: SearchPresentersComponent,
};

export const SearchPresenters = () => {
  const [open, setOpen] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const [presenters, setPresenters] = useState<Array<Presenter> | undefined>();
  const addPresenter = (presenter: Presenter) => {
    let newPresenters: Presenter[] = Object.assign([], presenters);
    newPresenters.push(presenter);
    setPresenters(newPresenters);
  };

  const label = "Name of Presenters To search";
  const defaultValue = "سینا ماشینی";
  const value = text(label, defaultValue);

  const searchResults: Presenter[] = [
    {
      title: value,
      id: "1234",
      image: "profile.jpg",
    },
    {
      title: value,
      id: "12345",
      image: "profile.jpg",
    },
  ];

  return (
    <Wrapper>
    <SearchPresentersComponent
    inputValue={inputValue}
    setInputValue={setInputValue}
    options={searchResults}
    loading={false}
      open={open}
      setOpen={setOpen}
      addPresenter={addPresenter}
      presenters={presenters}
      setOpenCreateModal={() => {alert('Modal will open to create presenter')}}
    />
    </Wrapper>
  );
};
