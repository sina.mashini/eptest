import React, { FC, useContext } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { SearchPresenters } from "../../interfaces";
import { TextField, CircularProgress, Box, IconButton } from "@material-ui/core";
import { context } from "../../../Context/StoryBookContext";
import styled from "styled-components";
import PresenterItemComponent from "./PresenterItem";
import PersonAddIcon from "@material-ui/icons/PersonAdd";

const SearchBar = styled(Box)`
  border-bottom: 1px solid #bbb;
  max-width: 350px;
  flex: 1;
  &:hover {
    border-bottom: 2px solid ${(props) => props.theme.palette.primary.main};
  }
  &:focus {
    border-bottom: 2px solid ${(props) => props.theme.palette.primary.main};
  }
  & > div div div fieldset {
    border: 0;
  }
  & > div div div div.MuiAutocomplete-endAdornment {
    top: 0;
  }
`;

export const SearchPresentersComponent: FC<SearchPresenters> = (props) => {
  const {
    inputValue,
    setInputValue,
    options,
    loading,
    open,
    setOpen,
    addPresenter,
    presenters,
    setOpenCreateModal,
  } = props;

  const { translate } = useContext(context);
  const { t } = translate();

  return (
    <>
    <SearchBar id="presenter-search-bar">
      <Autocomplete
        id="asynchronous-demo"
        open={open}
        inputValue={inputValue}
        onInputChange={(event, newInputValue) => {
          setInputValue(newInputValue);
        }}
        onOpen={() => {
          setOpen(options.length > 0);
        }}
        onClose={() => {
          setOpen(false);
        }}
        getOptionLabel={(option) => option.title}
        options={options}
        loading={loading}
        renderInput={(params) => (
          <TextField
            {...params}
            autoComplete="on"
            variant="outlined"
            placeholder={t("جستجو")}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <>
                  <React.Fragment>
                    {loading ? (
                      <CircularProgress color="inherit" size={20} />
                    ) : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                </>
              ),
            }}
          />
        )}
        renderOption={(option, state) => {
          return (
            <PresenterItemComponent
              presenter={option}
              addPresenter={addPresenter}
              presenters={presenters}
            />
          );
        }}
      />
    </SearchBar>
      <IconButton onClick={() => setOpenCreateModal(true)}>
        <PersonAddIcon />
      </IconButton>
      </>
  );
};

export default SearchPresentersComponent;
