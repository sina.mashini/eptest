import React from "react";
import { Presenter } from "../../interfaces";
import SelectedPresenterItemComponent from "./SelectedPresenterItem";

export default {
  title:
    "Section Components | CreateWebinar / Step Three / SelectedPresenterItem",
  component: SelectedPresenterItemComponent,
};

const presenter: Presenter = {
  id: "1234",
  title: "سینا ماشینی",
  image: "profile.jpg",
};

const deletePresenter = () => {
  alert("presenter will delete");
};

export const SelectedPresenterItem = () => {
  return (
    <SelectedPresenterItemComponent
      presenter={presenter}
      deletePresenter={deletePresenter}
    />
  );
};
