import React, { FC, useContext } from "react";
import styled from "styled-components";
import { context } from "../../../Context/StoryBookContext";
import {
  Box,
  TextField,
  Typography,
  TextareaAutosize,
  Button,
} from "@material-ui/core";
import { CreatePresenter, ThemeProps } from "../../interfaces";
import UploadImageComponent from "../../../Upload/UploadImage/UploadImage";

const Wrapper = styled(Box)`
  display: flex;
  width: 100%;
  flex-direction: column;
  & > * {
    margin-bottom: ${(props) => props.theme.spacing(2)}px !important;
    &:last-child {
      margin-bottom: 0 !important;
    }
  }
`;
const ItemsWrapper = styled.div`
  padding-top: 18px;
  & > p {
    color: ${(props: ThemeProps) => {
      if (props.error && !props.value) {
        return "red";
      } else return "inherit";
    }};
  }
  & > textarea {
    border: ${(props: ThemeProps) => {
      if (props.error && !props.value) return "2px solid red";
    }};
    &:hover {
      border: ${(props: ThemeProps) => {
        if (props.error && !props.value) return "2px solid red";
      }};
    }
    &:focus {
      border: ${(props: ThemeProps) => {
        if (props.error && !props.value) return "2px solid red";
      }};
    }
  }
`;
const DescriptionInput = styled(TextareaAutosize)`
  font-family: "IRANSans";
  width: 100%;
  padding: 12px;
  border: 1px solid ${(props) => props.theme.palette.secondary.main};
  border-radius: 6px;
  resize: none;
  margin-top: 12px;
  outline: none;
  transition: all 0.1s linear;
  &:hover {
    border: 2px solid rgba(0, 0, 0, 0.87);
  }
  &:focus {
    border: 2px solid ${(props) => props.theme.palette.primary.main};
  }
`;
const ButtonWrap = styled(Box)`
  display: flex;
  justify-content: center;
  & > * {
    min-width: 80px !important;
    &:first-child {
      margin-left: ${props => props.theme.spacing()}px;
    }
    &:last-child {
      margin-left: 0;
      margin-right: ${props => props.theme.spacing()}px;
    }
  }
`

export const CreatePresenterComponent: FC<CreatePresenter> = (props) => {
  const {
    title,
    fieldOfStudy,
    affiliation,
    biography,
    presenterImage,
    error,
    setItems,
    uploadHandler,
    setUploadState,
    progress,
    createPresenter,
    setOpen
  } = props;
  const { translate } = useContext(context);
  const { t } = translate();
  return (
    <Wrapper>
      <Typography style={{ marginBottom: 18 }} variant="h5">
        {t("ایجاد ارائه دهنده")}
      </Typography>
      <TextField
      required
        name="title"
        value={title}
        label={t("نام و نام خانوادگی")}
        onChange={(e) => setItems("title", e.target.value)}
        error={(error && !title) ? true : false}
      />
      <TextField
      required
        name="fieldOfStudy"
        value={fieldOfStudy}
        label={t("رشته تحصیلی")}
        onChange={(e) => setItems("fieldOfStudy", e.target.value)}
        error={(error && !fieldOfStudy) ? true : false}
      />
      <TextField
      required
        name="affiliation"
        value={affiliation}
        label={t("دانشگاه")}
        onChange={(e) => setItems("affiliation", e.target.value)}
        error={(error && !affiliation) ? true : false}
      />
      <ItemsWrapper error={(error && !biography) ? true : false} value={biography}>
        <Typography>توضیحات</Typography>
        <DescriptionInput
          value={biography}
          rowsMin={4}
          style={{ boxSizing: "border-box" }}
          name="biography"
          onChange={(e) => setItems("biography", e.target.value)}
        />
      </ItemsWrapper>
      <UploadImageComponent
        title={t("عکس پروفایل")}
        image={presenterImage}
        uploadHandler={uploadHandler}
        setUploadState={setUploadState}
        state="PresenterImage"
        progress={progress}
        error={error}
      />
      <ButtonWrap>
        <Button onClick={createPresenter} variant="contained" color="primary">{t("ایجاد")}</Button>
        <Button onClick={() => setOpen(false)} variant="outlined" color="primary">{t("لغو")}</Button>
      </ButtonWrap>
    </Wrapper>
  );
};

export default CreatePresenterComponent;
