import React, { FC } from "react";
import styled from "styled-components";
import { SelectedPresenters } from "../../interfaces";
import { Box } from "@material-ui/core";
import SelectedPresenterItemComponent from "./SelectedPresenterItem";

const Wrapper = styled(Box)`
  width: 100%;
  display: flex;
  justify-content: center;
  padding: ${(props) => props.theme.spacing()}px;
  margin-top: ${props => props.theme.spacing(5)}px;
`;

export const SelectedPresentersComponent: FC<SelectedPresenters> = (props) => {
  const { presenters, deletePresenter } = props;

  return (
    <Wrapper>
      {presenters.map((presenter) => (
        <SelectedPresenterItemComponent presenter={presenter} deletePresenter={deletePresenter} />
      ))}
    </Wrapper>
  );
};

export default SelectedPresentersComponent;
