import React, {useState} from "react";
import { Presenter } from "../../interfaces";
import SelectedPresentersComponent from "./SelectedPresenters";
import { Grid } from "@material-ui/core";

export default {
  title: "Section Components | CreateWebinar / Step Three / SelectedPresenters",
  component: SelectedPresentersComponent,
};

export const SelectedPresenters = () => {
  const [presenters, setPresenters] = useState([
    {
      id: "1234",
      title: "سینا ماشینی",
      image: "profile.jpg",
    },
    {
      id: "12345",
      title: "محسن لطفی",
      image: "profile.jpg",
    },
  ]);
  const deletePresenter = (presenter: Presenter) => {
    let newPresenters: Presenter[] = Object.assign([], presenters);
    newPresenters.some((item, index) => {
      if (item.id === presenter.id) {
        newPresenters.splice(index, 1);
      }
    });
    setPresenters(newPresenters);
  };
  return (
    <Grid xs={12} sm={8}>
      <SelectedPresentersComponent
        presenters={presenters}
        deletePresenter={deletePresenter}
      />
    </Grid>
  );
};
