import React, { FC } from "react";
import { PresenterItem, Presenter } from "../../interfaces";
import styled from "styled-components";
import { Box, Typography, IconButton } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";

const Wrapper = styled(Box)`
  display: flex;
  width: 100%;
  padding: ${(props) => props.theme.spacing()}px;
  align-items: center;
  cursor: pointer;
`;
const ImageWrap = styled(Box)`
  position: relative;
  width: 25px;
  height: 25px;
  margin-left: ${(props) => props.theme.spacing(2)}px;
  & > img {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
  }
`;

export const PresenterItemComponent: FC<PresenterItem> = (props) => {
  const { presenter, addPresenter, presenters } = props;

  const isExist = (presenter: Presenter) => {
    let check = false;
    if (presenters) {
      presenters.map((item) => {
        if (item.id === presenter.id) {
          check = true;
        }
      });
    }
    return check;
  };

  return (
    <Wrapper>
      <ImageWrap>
        <img src={`https://edupack.nightly.repo.avidcloud.io/api/v1/blob/${presenter.image}`} />
      </ImageWrap>
      <Typography style={{ flex: 1 }}>{presenter.title}</Typography>
      {isExist(presenter) ? (
        <CheckCircleOutlineIcon style={{ color: "#77c788" }} />
      ) : (
        <IconButton
          style={{ padding: 0 }}
          onClick={() => addPresenter(presenter)}
        >
          <AddIcon />
        </IconButton>
      )}
    </Wrapper>
  );
};

export default PresenterItemComponent;
