import React, { FC } from "react";
import { Paper } from "../Paper/Paper";
import styled, { css } from "styled-components";
import { Box, Grid } from "@material-ui/core";
import { CreateWebinarPaper } from "./interfaces";
import Steper from "./partials/Steper";
import SteperButtons from "./partials/SteperButtons";

const columnDirection = css`
  display: flex;
  flex-direction: column;
  flex: 1 !important;
`;

const Wrapper = styled(Box)`
  min-height: 500px;
  align-items: center;
  ${columnDirection};
`;

const StepsContainer = styled(Grid)`
  width: 100%;
  ${columnDirection};
`;


export const CreateWebinarPaperComponent: FC<CreateWebinarPaper> = (props) => {
  const { items } = props;

  return (
    <Paper>
      <Wrapper>
        <Steper activeStep={items.activeStep} steps={items.steps} />
        <StepsContainer item xs={12} sm={6}>
          {items.stepComponent(items.activeStep)}
        </StepsContainer>
          <SteperButtons
            activeStep={items.activeStep}
            steps={items.steps}
            handleNext={items.handleNext}
            handleBack={items.handleBack}
          />
      </Wrapper>
    </Paper>
  );
};

export default CreateWebinarPaperComponent;
