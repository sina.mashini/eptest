import React, { FC, useState, useContext } from "react";
import styled from "styled-components";
import { Divider, Menu, MenuItem } from "@material-ui/core";
import ModalContentComponent from "../../ModalComponent/ModalContentComponent";
import { EduPackBtn } from "../../Buttons/EduPackBtn";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { context } from "../../Context/StoryBookContext";

const Email = styled(MenuItem)`
  padding-left: 24px;
  justify-content: center;
`;

const MenuButton = styled(MenuItem)`
  justify-content: center;
`;

const Root = styled(Menu)`
  margin-top: 12px;
`;

const ProfilePlace = styled.div`
  margin: 0 auto;
  display: flex;
`;

const ProfileIcon = styled(AccountCircleIcon)`
  margin-top: 5px;
`;

export interface MenuProps {
  fullName?: string | null | undefined;
  anchorEl: null | HTMLElement;
  handleClose: (boolean) => any;
  login: () => void;
  logout: () => void;
  linkToSpecificPage: (children: JSX.Element, url: string) => JSX.Element;
  role: string;
  adminPanel: () => void;
}

export const ProfileMenu: FC<MenuProps> = ({
  fullName,
  anchorEl,
  handleClose,
  login,
  logout,
  linkToSpecificPage,
  role,
  adminPanel
}) => {
  const { translate } = useContext(context);
  const { t } = translate();
  const open = Boolean(anchorEl);
  const [openModal, setOpenModal] = useState(false);

  const menuItems: JSX.Element | (false | JSX.Element)[] = fullName ? (
    [
      <MenuItem onClick={handleClose} key="1">
        <ProfilePlace>
          <ProfileIcon />
          <Email>{fullName}</Email>
        </ProfilePlace>
      </MenuItem>,
      <Divider key="32" />,
      <MenuButton key="2">
        <EduPackBtn onClick={logout} label={t("خروج")} variant="contained" />
      </MenuButton>,
      <MenuButton>
        <EduPackBtn
          onClick={() => {
            setOpenModal(true);
          }}
          variant="contained"
          label={t("ایجاد وبینار")}
        />
      </MenuButton>,
      role === "admin" && (
        <MenuButton>
          <EduPackBtn
            onClick={adminPanel}
            variant="contained"
            label={t("پنل ادمین")}
          />
        </MenuButton>
      ),
    ]
  ) : (
    <MenuButton key="4324">
      <EduPackBtn
        onClick={login}
        label={t("ورود / ثبت نام")}
        variant="contained"
      />
    </MenuButton>
  );

  return (
    <Root
      getContentAnchorEl={null}
      id="menu-appbar"
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center",
      }}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "center",
      }}
      open={open}
      onClose={handleClose}
    >
      <ModalContentComponent
        open={openModal}
        setOpen={setOpenModal}
        linkToSpecificPage={linkToSpecificPage}
      />
      {menuItems}
    </Root>
  );
};
