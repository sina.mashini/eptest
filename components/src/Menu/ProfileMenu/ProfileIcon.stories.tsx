import React from "react";
import { ProfileIcon } from "./ProfileIcon";

export default {
  title: "ATOMS | Menu / Profile / ProfileIcon",
  component: ProfileIcon,
};

const login = () => alert("Login");
const logout = () => alert("logout");
const linkToSpecificPage = (children: JSX.Element, url: string = "#") => (
  <a href="#">{children}</a>
);

export const LogIn = () => {
  return (
    <ProfileIcon
      fullName=""
      login={login}
      logout={logout}
      linkToSpecificPage={linkToSpecificPage}
      role="admin"
      adminPanel={() => {}}
    ></ProfileIcon>
  );
};

export const LogOut = () => {
  return (
    <ProfileIcon
      fullName="Narges Haghighi"
      login={login}
      logout={logout}
      linkToSpecificPage={linkToSpecificPage}
      role="admin"
      adminPanel={() => {}}
    ></ProfileIcon>
  );
};
