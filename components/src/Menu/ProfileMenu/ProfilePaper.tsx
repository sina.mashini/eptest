import React, { FC } from "react";
import styled from "styled-components";
// import { Paper } from "@material-ui/core";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

const Name = styled.span`
  color: black;
  font-size: 14px;
  margin: 0 10px 0 10px;
`;
const MyPaper = styled.div`
  * {
    font-family: "IRANSans";
  }
  display: flex;
  padding: ${(props) => props.theme.spacing(3)}px;
  background-color: ${(props) => props.theme.palette.primary.main};
  border-radius: ${(props) => props.theme.spacing(2)}px;
  align-items: center;
`;
const ProfileIcon = styled(AccountCircleIcon)`
  color: black;
`;

export interface ProfilePaperProps {
  name: string;
}

export const ProfilePaper: FC<ProfilePaperProps> = ({ name }) => {
  return (
    <MyPaper>
      <ProfileIcon />
      <Name>{name}</Name>
    </MyPaper>
  );
};
