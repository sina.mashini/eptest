import React, { FC, useContext } from "react";
import styled from "styled-components";
import { Menu, MenuItem } from "@material-ui/core";
import { context } from '../../Context/StoryBookContext'

const Lang = styled(MenuItem)`
  padding-left: 24px;
  justify-content: center;
`;

const Root = styled(Menu)`
  margin-top: 12px;
`;

const languagesPart = (langs, changeLang, handleClose) =>
{
   return langs.map((item ,index) => {
      return(         
        <Lang key={`${item}${index}`}  onClick={() => handleSelectLang(item.Locale, item.Direction, changeLang, handleClose)}>{item.title}</Lang> 
      )
   });
}

const handleSelectLang = (locale, direction, changeLang, handleClose) => 
{
  changeLang(locale, direction);
  handleClose();
}

export interface LangProps {
  anchorEl: null | HTMLElement;
  handleClose: (boolean) => any;
}

export const LanguageMenu: FC<LangProps> = ({
  anchorEl,
  handleClose,
}) => {
  const open = Boolean(anchorEl);
  const { changeLang, Languages } = useContext(context)
  return (
    <Root
      getContentAnchorEl={null}
      id="menu-appbar"
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center",
      }}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "center",
      }}
      open={open}
      onClose={handleClose}
    >
      {languagesPart(Languages, changeLang, handleClose)}
    </Root>
  );
};
