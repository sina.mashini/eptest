import React from "react";
import { LanguageIconComp } from "./LanguageIcon";

export default {
  title: "ATOMS | Menu / Language / LanguageIcon",
  component: LanguageIconComp,
};

export const LanguageIconStories = () => {
  return <LanguageIconComp></LanguageIconComp>;
};
