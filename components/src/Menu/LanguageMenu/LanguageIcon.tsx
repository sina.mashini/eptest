import React, { FC } from "react";
import IconButton from "@material-ui/core/IconButton";
import LanguageIcon from '@material-ui/icons/Language';
import { LanguageMenu } from "./LanguageMenu";

export const LanguageIconComp: FC = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  return (
    <div>
      <IconButton
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleMenu}
        color="inherit"
      >
        <LanguageIcon
          style={{ width: "48px", height: "35px", color: "#7a7a7a" }}
        />
      </IconButton>
      <LanguageMenu
        anchorEl={anchorEl}
        handleClose={handleClose}
      ></LanguageMenu>
    </div>
  );
};

export default LanguageIconComp;