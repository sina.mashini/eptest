import React, { FC, useEffect, useState } from "react";
import { Typography, Box, CircularProgress } from "@material-ui/core";
import AddPhotoAlternateOutlinedIcon from "@material-ui/icons/AddPhotoAlternateOutlined";
import styled from "styled-components";
import { UploadImage, ThemeProps } from "../interfaces";

const Wrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  margin-top: ${(props) => props.theme.spacing(3)}px;
`;
const Title = styled(Typography)`
  margin-left: ${(props) => props.theme.spacing(3)}px;
  color: ${(props: ThemeProps) =>
    props.error && props.value.length === 0 ? "red" : "inherit"} !important;
`;
const Loading = styled(CircularProgress)`
  color: ${(props) => props.theme.palette.primary.dark} !important;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%) rotate(-90deg) !important;
`;
const Label = styled.label`
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  z-index: 1;
`;
const ButtonWrap = styled(Box)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 120px;
  height: 75px;
  border-radius: 6px;
  background-color: ${(props) => props.theme.palette.primary.main};
  margin: 12px 12px 0;
  overflow: hidden;
`;
const Image = styled.img`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
`;

export const UploadImageComponent: FC<UploadImage> = (props) => {
  const {
    title,
    image,
    setUploadState,
    uploadHandler,
    state,
    progress,
    error,
    required,
  } = props;

  const [_loading, setLoading] = useState(false);

  useEffect(() => {
    if (image.length) {
      setLoading(false);
    }
  }, [image]);

  return (
    <Wrapper>
      <Title error={error} value={image}>
        {title}
        {required && "*"}
      </Title>
      <ButtonWrap>
        <input
          style={{ display: "none" }}
          accept="image/*"
          id="input-img"
          type="file"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setUploadState(state);
            uploadHandler(e);
            setLoading(true);
          }}
        />
        <Label htmlFor="input-img">
          {!_loading && progress !== 100 && <AddPhotoAlternateOutlinedIcon />}
        </Label>
        {progress === 100 && image[0] && <Image src={String(image[0].data)} />}
        {_loading && <Loading value={progress} variant="static" />}
      </ButtonWrap>
    </Wrapper>
  );
};

export default UploadImageComponent;
