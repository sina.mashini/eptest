import React, { useState } from "react";
import UploadImageComponent from "./UploadImage";
import { text, radios } from "@storybook/addon-knobs";
import { ImageData } from '../../interfaces';

export default {
  title: "Atoms | Upload / UploadImage",
  component: UploadImageComponent,
};

export const UploadImage = () => {

  const label = "Title";
  const defaultValue = "عکس پروفایل";
  const value = text(label, defaultValue);

  const [image, setImage] = useState<Array<ImageData>>([]);

  const progressLabel = "Progress";
  const progressOptions = {
    '0%': '0',
    '100%': '100'
  };
  const progressDefaultValue = '0';
  const progressValue = radios(progressLabel, progressOptions, progressDefaultValue);

  const errorLabel = "Error";
  const errorOptions = {
    true: '1',
    false: '0',
  };
  const errorDefaultValue = '0';
  const errorValue = radios(errorLabel, errorOptions, errorDefaultValue);

  const setUploadState = () => {};
  const uploadHandler = (e) => {
    if (e.target.files && e.target.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        let item: ImageData = {
          _id: '1234',
          data: String(e.target?.result),
          name: 'file-1',
          size: 1234,
          type: value
        }
        let newArray: ImageData[] = Object.assign([], image);
        newArray = [item];
        setImage(newArray);
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  

  return (
    <UploadImageComponent
      title={value}
      image={image}
      setUploadState={setUploadState}
      uploadHandler={uploadHandler}
      state="upload"
      progress={Number(progressValue)}
      error={Boolean(Number(errorValue))}
      required={true}
    />
  );
};
