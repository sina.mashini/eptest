import { ImageData } from '../CreateWebinar/interfaces';
import { Dispatch, SetStateAction } from 'react';

export interface UploadImage {
  title: string;
  image: ImageData[];
  uploadHandler: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
  setUploadState: Dispatch<SetStateAction<string>>;
  state: string;
  progress: number;
  error: boolean;
  required?: boolean;
}

export interface ThemeProps {
  error: boolean;
  value: ImageData[] | any;
}
