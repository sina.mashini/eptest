import React, { FC, Dispatch, SetStateAction, useEffect } from "react";
import { Box, Typography, Button } from "@material-ui/core";
import UploadPaper from "../../Paper/UploadPaper/UploadPaper";
import ModalComponent from "../../ModalComponent/ModalComponent";
import { ImageData } from "../../interfaces";
import styled from "styled-components";

const UploadButtonWrap = styled(Button)`
  display: flex;
  flex-direction: column;
  min-width: 85px !important;
  justify-content: center;
  align-items: center;
  border-radius: ${(props) => props.theme.spacing()}px !important;
  background-color: ${(props) => props.theme.palette.primary.main} !important;
  padding: ${(props) => props.theme.spacing()}px !important;
`;
const UploadIcon = styled(Box)`
  & > * {
    color: ${(props) => props.theme.palette.primary.contrastText};
  }
`;

export interface UploadButtonProps {
  icon: JSX.Element;
  title: string;
  uploadHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
  media: ImageData[];
  mediaTitle: string;
  mediaDesc: string;
  setMediaTitle: Dispatch<SetStateAction<string>>;
  setMediaDesc: Dispatch<SetStateAction<string>>;
  setUploadState: Dispatch<SetStateAction<string>>;
  setAttachmentHandler: (media: ImageData[], title:string, description:string, showMedia: boolean) => void;
  openModal: boolean;
  setOpenModal: Dispatch<SetStateAction<boolean>>;
  error: boolean;
  progress: number
}

const UploadButton: FC<UploadButtonProps> = (props) => {
  const {
    icon,
    title,
    uploadHandler,
    media,
    mediaTitle,
    mediaDesc,
    setMediaTitle,
    setMediaDesc,
    setUploadState,
    setAttachmentHandler,
    openModal,
    setOpenModal,
    error,
    progress
  } = props;

  return (
    <>
      <ModalComponent open={openModal} closeModal={() => setOpenModal(false)}>
        <UploadPaper
          uploadHandler={uploadHandler}
          media={media}
          title={mediaTitle}
          description={mediaDesc}
          progress = {progress}
          setTitle={setMediaTitle}
          setDesc={setMediaDesc}
          setUploadState={setUploadState}
          setAttachmentHandler={setAttachmentHandler}
          setOpenModal={setOpenModal}
          error={error}
        />
      </ModalComponent>
      <UploadButtonWrap onClick={() => setOpenModal(true)}>
        <UploadIcon>{icon}</UploadIcon>
        <Typography variant="caption">{title}</Typography>
      </UploadButtonWrap>
    </>
  );
};

export default UploadButton;
