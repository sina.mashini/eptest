import React, { useState } from "react";
import UploadButtonComponent from "./UploadButton";
import { text, radios } from "@storybook/addon-knobs";
import AddPhotoAlternateOutlinedIcon from "@material-ui/icons/AddPhotoAlternateOutlined";
import { ImageData } from "../../interfaces";

export default {
  title: "Atoms | Buttons / UploadButton",
  component: UploadButtonComponent,
};

export const UploadButton = () => {
  const title = text("Title", "آپلود عکس");

  const label = "Type";
  const options = {
    image: "image",
    video: "video",
    file: "file",
  };
  const defaultValue = "image";
  const value = radios(label, options, defaultValue);

  const errorLabel = "Error";
  const errorOptions = {
    true: "1",
    false: "0",
  };
  const errorDefaultValue = "0";
  const errorValue = radios(errorLabel, errorOptions, errorDefaultValue);

  const [mediaTitle, setMediaTitle] = useState("");
  const [mediaDescription, setMediaDescription] = useState("");
  const [openModal, setOpenModal] = useState(false);
  const [media, setMedia] = useState<Array<ImageData>>([]);
  const setUploadState = () => {};

  const uploadHandler = (e) => {
    if (e.target.files && e.target.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        let item: ImageData = {
          _id: '1234',
          data: String(e.target?.result),
          name: 'file-1',
          size: 1234,
          type: value
        }
        let newArray: ImageData[] = Object.assign([], media);
        newArray = [item];
        setMedia(newArray);
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const uploadAttachment = (media: ImageData[]) => {
    alert("media uploaded");
  };

  return (
    <UploadButtonComponent
      title={title}
      icon={<AddPhotoAlternateOutlinedIcon />}
      type={value}
      uploadHandler={uploadHandler}
      mediaTitle={mediaTitle}
      mediaDesc={mediaDescription}
      setMediaDesc={setMediaDescription}
      setMediaTitle={setMediaTitle}
      openModal={openModal}
      setOpenModal={setOpenModal}
      error={Boolean(Number(errorValue))}
      setUploadState={setUploadState}
      media={media}
      setAttachmentHandler={uploadAttachment}
    />
  );
};
