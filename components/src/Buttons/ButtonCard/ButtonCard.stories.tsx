import React from "react";
import ButtonCard from "./ButtonCard";
import { text, boolean } from "@storybook/addon-knobs";
import QueuePlayNextIcon from "@material-ui/icons/QueuePlayNext";


export default {
  title: "ATOMS | Buttons / Button Card",
  component: ButtonCard,
};

export const ButtonCardStory = () => {
  const title = text("Title", "ایجاد وبینار");

  const typeLabel = "Disabled?";
  const typeDefaultValue = false;
  const type = boolean(typeLabel, typeDefaultValue);

  const linkToSpecificPage = (children: JSX.Element) => <a href="#">{children}</a>;

  return (
    <ButtonCard
      title={title}
      icon={<QueuePlayNextIcon />}
      disabled={type}
      linkToSpecificPage={linkToSpecificPage}
      url="#"
    />
  );
};
