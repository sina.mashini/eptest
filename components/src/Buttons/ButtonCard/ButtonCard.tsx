import React, { FC } from "react";
import { Paper, Button, makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  paperWrap: {
    display: "flex",
    width: 100,
    '& > a': {
      width: '100%',
      height: '100%',
      textDecoration: 'none',
    }
  },
  button: {
    width: '100%',
    height: '100%',
    flex: 1,
    padding: 8,
    fontFamily: "IRANSans",
    backgroundColor: "#fff",
    "& > span": {
      flexDirection: "column",
      height: "100%",
    },
  },
  buttonTitle: {
    display: "flex",
    alignItems: "center",
    flex: 1,
    fontFamily: "IRANSans",
  },
  icon: {
    "& > *": {
      "& > *": {
        width: 40,
        height: 40,
        color: "#91bfd9",
      },
    },
  },
  disabledBtn: {
    backgroundColor: "#eee",
  },
  disabledIcon: {
    "& > *": {
      "& > *": {
        color: "rgba(0, 0, 0, 0.26)",
      },
    },
  },
}));

export interface ButtonCardProps {
  title: string;
  icon: JSX.Element;
  disabled?: boolean;
  linkToSpecificPage: (children: JSX.Element, url: string) => JSX.Element;
  url: string;
}

const ButtonCard: FC<ButtonCardProps> = (props) => {
  const classes = useStyles();
  const { title, icon, disabled, linkToSpecificPage, url } = props;

  return (
    <Paper className={classes.paperWrap}>
      {linkToSpecificPage(
        <Button
          className={`${classes.button} ${disabled && classes.disabledBtn}`}
          disabled={disabled}
        >
          <div
            className={`${classes.icon} ${disabled && classes.disabledIcon}`}
          >
            {icon}
          </div>
          <Typography className={classes.buttonTitle}>{title}</Typography>
        </Button>,
        url
      )}
    </Paper>
  );
};

export default ButtonCard;
