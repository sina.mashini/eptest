import React from 'react';
import Button from '@material-ui/core/Button';

export interface RoundedButtonProps {
  onClick: () => void;
  label: string;
  variant: 'contained' | 'outlined';
  color?: 'primary' | 'secondary';
}

export const RoundedButton = ({label, onClick, variant, color}: RoundedButtonProps) => {
  return (
    <Button onClick={onClick} variant={variant} color={color ? color : 'primary'}>
      {label}
    </Button>
  );
}
