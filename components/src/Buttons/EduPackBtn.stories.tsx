import React from 'react';
import { EduPackBtn } from './EduPackBtn';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Atoms | Buttons / EduPackButton',
  component: EduPackBtn,
};

export const ContainedButton = () => {
  const onClick = action('onClick');
  return <EduPackBtn onClick={onClick} label="ورود" variant="contained"/>;
};

export const OutlinedButton = () => {
  const onClick = action('onClick');
  return <EduPackBtn onClick={onClick} label="خروج" variant="outlined"/>;
}