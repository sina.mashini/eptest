import React, { FC } from "react";
import styled from "styled-components";
import { Button } from "@material-ui/core";

const ButtonStyle = styled(Button)`
  font-family: "IRANSans";
  border-radius: 17px;
  width: 154px;
  height: 30px;
  margin: 4px 20px;
  cursor: pointer;
  background-color: #91bfd9 !important;
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #ffffff;
`;

export interface EduPackBtnProps {
  onClick: () => any;
  label: string;
  variant: "contained" | "outlined";
}

export const EduPackBtn: FC<EduPackBtnProps> = ({
  onClick,
  label,
  variant,
}) => {
  return (
    <ButtonStyle onClick={onClick} variant={variant}>
      {label}
    </ButtonStyle>
  );
};

export default EduPackBtn;
