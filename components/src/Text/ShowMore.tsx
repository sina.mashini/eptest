import React, { useState, FC, useContext } from "react";
import Typography from "@material-ui/core/Typography";
import styled from "styled-components";
import { context } from '../Context/StoryBookContext';

const ShowMoreButton = styled.a`
  color: #91bfd9;
  cursor: pointer;
  &:hover {
    color: black;
  }
`;

const TextArea = styled.div`
  font-family: "IRANSans";
`;

export interface ShowMoreTextProps {
  text?: string | undefined | null;
}

const createShorterText = (text) => {
  const { translate } = useContext(context)
  const { t } = translate();
  const [showMore, setShowMore] = useState(false);
  let newText = <TextArea>{text}</TextArea>;
  if (text.length > 400 && showMore === false) {
    let shortText = text.substring(0, 400);
    newText = (
      <TextArea>
        <span>{shortText}</span>...
        <ShowMoreButton onClick={() => setShowMore(true)}>{t("بیشتر")}</ShowMoreButton>
      </TextArea>
    );
  }
  return newText;
};

export const ShowMoreText: FC<ShowMoreTextProps> = ({ text }) => {
  return (
    <Typography variant="body1" gutterBottom>
      {createShorterText(text)}
    </Typography>
  );
};

export default ShowMoreText;
