import React from "react";
import ModalContentComponent from './ModalContentComponent';

export default {
    title: "Section Components | ModalContent / ModalContent",
    component: ModalContentComponent,
  };

export const ModalContent = () => {
    const linkToSpecificPage = (Children: JSX.Element, url: string) => <a href="#">{Children}</a>
    return <ModalContentComponent open={true} setOpen={() => {}} linkToSpecificPage={linkToSpecificPage} dir="rtl" />
}
