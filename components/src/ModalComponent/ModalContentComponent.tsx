import React, { FC, Dispatch, SetStateAction } from "react";
import ModalComponent from "./ModalComponent";
import ButtonCard from "../Buttons/ButtonCard/ButtonCard";
import { Theme, Box } from "@material-ui/core";
import QueuePlayNextIcon from "@material-ui/icons/QueuePlayNext";
import OndemandVideoIcon from "@material-ui/icons/OndemandVideo";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme: Theme) => ({
  buttonWrap: {
    display: "flex",
    width: "100%",
    justifyContent: 'center',
    "& > *": {
      marginRight: theme.spacing(),
      "&:first-child": {
        marginRight: 10,
        marginLeft: theme.spacing(),
      },
    },
  },
}));

export interface ModalContentComponentProps {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  linkToSpecificPage: (children: JSX.Element, url: string) => JSX.Element;
  dir?: 'rtl' | 'ltr';
}

const ModalContentComponent: FC<ModalContentComponentProps> = (props) => {
  const { open, setOpen, linkToSpecificPage, dir } = props;
  const classes = useStyles();

  return (
    <ModalComponent
      open={open}
      closeModal={() => {
        setOpen(false);
      }}
    >
      <Box className={classes.buttonWrap} style={{direction : dir ? dir : 'unset'}}>
        <ButtonCard
          linkToSpecificPage={linkToSpecificPage}
          url={"/webinar/create"}
          title="ایجاد وبینار"
          icon={<QueuePlayNextIcon />}
        />
        <ButtonCard
          linkToSpecificPage={linkToSpecificPage}
          url={"/learning-carousel/create"}
          title="ایجاد دوره آموزشی"
          icon={<OndemandVideoIcon />}
          disabled={true}
        />
      </Box>
    </ModalComponent>
  );
};

export default ModalContentComponent;
