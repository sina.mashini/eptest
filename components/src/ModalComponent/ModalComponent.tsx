import React, { FC } from "react";
import {
  Modal,
  Grid,
  Typography,
  Box,
  Backdrop,
  Fade,
  Theme,
} from "@material-ui/core";
import { makeStyles, CSSProperties } from "@material-ui/styles";

const useStyles = makeStyles((theme: Theme) => ({
  modal: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#fff",
    borderRadius: 8,
    padding: theme.spacing(5),
    "&:focus": {
      outline: "unset",
    },
  },
  content: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
}));

export interface ModalComponentProps {
  open: boolean;
  closeModal: () => void;
  title?: string;
  style?: CSSProperties;
}

export const ModalComponent: FC<ModalComponentProps> = ({
  children,
  ...props
}) => {
  const classes = useStyles();
  const { open, closeModal, title, style } = props;

  return (
    <Modal
      className={classes.modal}
      style={style}
      open={open}
      onClose={closeModal}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <Grid item sm={12} md={6} className={classes.paper}>
          {title && <Typography style={{ fontSize: 15 }}>{title}</Typography>}
          <Box className={classes.content}>{children}</Box>
        </Grid>
      </Fade>
    </Modal>
  );
};

export default ModalComponent;
