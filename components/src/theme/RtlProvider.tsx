import React, {FC} from 'react';
import rtlPlugin from "stylis-plugin-rtl";
import { StyleSheetManager } from 'styled-components'

export interface rtlProviderProps {
  direction: string;
}

export const RtlProvider:FC<rtlProviderProps> = ({direction, children}) =>
{
   if(direction === "ltr")
      return (<StyleSheetManager stylisPlugins={[rtlPlugin]}>
          {children}
      </StyleSheetManager>)
    else 
     return (
      <>
        {children}
      </>
     )
}

export default RtlProvider;