import React , { FC } from 'react'
import styled from 'styled-components'

const MyDiv = styled.div`
  color: red;
  height: 200px;
  background-color: ${props => {
      return props.theme.palette.primary.main;
}};
  padding-right: ${props => props.theme.spacing(4)}px;
  /* @noflip */ margin-left:80px 
`

export const ThemeTest:FC = () =>
{
    return (
        <MyDiv>
            <p>Hello World</p>
        </MyDiv>
    )
}

export default ThemeTest;