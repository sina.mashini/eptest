import { ThemeOptions } from "@material-ui/core/styles/createMuiTheme";
import { getContrastRatio } from "@material-ui/core/styles";
import {
  dark,
  light,
  SimplePaletteColorOptions,
} from "@material-ui/core/styles/createPalette";
import { Color } from "@material-ui/core";

const contrastThreshold = 3;
const primary: SimplePaletteColorOptions & Partial<Color> = {
  main: "#91bfd9",
  contrastText: "rgba(8, 46, 73, 0.81)",
  dark: "rgba(0, 37, 63, 0.92)",
  light: "rgba(19, 90, 128, 0.6)",
  "600": "#007575",
};
const secondary: SimplePaletteColorOptions | Partial<Color> = {
  main: "#c2c2c2",
  dark: "#7a7a7a",
  light: "#d8d8d8",
  contrastText: "#ededed",
};

const themeOptions: ThemeOptions = {
  spacing: 6,
  typography: {
    fontFamily: "IRANSans",
    h1: {
      fontSize: "1.5rem",
      fontWeight: 500,
      fontStretch: "normal",
      fontStyle: "normal",
      lineHeight: "normal",
      letterSpacing: "-0.36px",
      color: "#ffffff",
    },
    body1: {
      color: "#323232",
      fontSize: '0.875rem'
    },
    h6: {
      color: '#fff',
      fontSize: '1.5rem'
    },
    caption: {
      fontSize: "1rem",
      fontWeight: 'normal',
      fontStretch: "normal",
      fontStyle: "normal",
      lineHeight: "normal",
      letterSpacing: "-0.24px",
      color: "#ededed",
    },
  },
  palette: {
    primary,
    secondary,
    contrastThreshold,
    getContrastText: (background) => {
      if (background === primary.dark) {
        return "#a4c4c4";
      }
      const contrastText =
        getContrastRatio(background, dark.text.primary) >= contrastThreshold
          ? dark.text.primary
          : light.text.primary;
      return contrastText;
    },
  },
  overrides: {
    MuiPaper: {
      rounded: {
        borderRadius: 12,
      },
    },
    MuiChip: {
      label: {
        color: secondary.dark
      },
      deleteIcon: {
        color: secondary.light
      }
    },
    MuiButton: {
      root: {
        '&:hover': {
          backgroundColor: primary.contrastText
        }
      },
      label: {
        flexDirection: 'inherit'
      },
      containedPrimary: {
        color: '#fff',
        borderRadius: 24
      },
      outlinedPrimary: {
        borderWidth: 2,
        borderRadius: 24,
        '&:hover': {
          borderWidth: 2,
          borderColor: primary.dark,
        }
      }
    },
    MuiStepLabel: {
      labelContainer: {
        '@media all and (max-width: 450px)': {
          display: 'none',
        }
      }
    }
  },
};
export default themeOptions;
