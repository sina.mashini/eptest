export interface ImageData {
  data: string | null;
  name: string;
  size: number;
  type: string;
  _id: string;
}

export interface ThemeProps {
  active: boolean;
  theme: any;
}