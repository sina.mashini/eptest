import React, { FC } from "react";
import styled from "styled-components";
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
  List,
} from "@material-ui/core";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import { ThemeProps } from "../interfaces";

const Title = styled(ListItem)`
  padding: 8px 16px !important;
  background-color: ${(props: ThemeProps) =>
    props.active ? props.theme.palette.primary.main : "inherit"} !important;
  border-radius: 8px !important;
`;

export interface CollapseItemProps {
  open: boolean;
  title: string;
  icon: JSX.Element;
  items: JSX.Element[];
  handleClick: () => void;
}

export const CollapseItem: FC<CollapseItemProps> = (props) => {
  const { open, title, icon, items, handleClick } = props;

  return (
    <>
      <Title button onClick={handleClick} active={open}>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={title} />
        {open ? <ExpandLess /> : <ExpandMore />}
      </Title>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div">{items.map((item) => item)}</List>
      </Collapse>
    </>
  );
};

export default CollapseItem;
