import React, { useState } from "react";
import CollapseItemComponent from "./CollapseItem";
import ComputerIcon from "@material-ui/icons/Computer";
import CancelPresentationIcon from "@material-ui/icons/CancelPresentation";
import OndemandVideoIcon from "@material-ui/icons/OndemandVideo";
import { text } from "@storybook/addon-knobs";
import { ListItem, ListItemIcon, ListItemText } from "@material-ui/core";

export default {
  title: "Atoms | CollapseItem / CollapseItem",
  Components: CollapseItemComponent,
};

export const CollapseItem = () => {
  const [open, setOpen] = useState(false);

  const label = "Title";
  const defaultValue = "وبینارها";
  const title = text(label, defaultValue);

  const items: JSX.Element[] = [
    <ListItem button>
      <ListItemIcon>
        <OndemandVideoIcon />
      </ListItemIcon>
      <ListItemText primary="وبینارهای منتشر شده" />
    </ListItem>,
    <ListItem button>
      <ListItemIcon>
        <CancelPresentationIcon />
      </ListItemIcon>
      <ListItemText primary="وبینارهای منتشر نشده" />
    </ListItem>,
  ];

  const handleClick = () => {
      setOpen(!open);
  }

  return (
    <CollapseItemComponent
      open={open}
      title={title}
      icon={<ComputerIcon />}
      items={items}
      handleClick={handleClick}
    />
  );
};
