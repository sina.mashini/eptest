import React from "react";
import { addDecorator, addParameters } from "@storybook/react";
import { withKnobs } from "@storybook/addon-knobs";
import { themes } from "@storybook/theming";
import { createMuiTheme, ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import "@iin/typeface-iran-sans";
import "@iin/typeface-iran";
import "@iin/typeface-iran-kharazmi";
import "@iin/typeface-iran-rounded";
import "@iin/typeface-iran-sharp";
import "@iin/typeface-iran-yekan";
import { ThemeProvider as SCThemeProvider } from "styled-components";
import { StoryFn } from '@storybook/addons';
import { StoryFnReactReturnType } from '@storybook/react/dist/client/preview/types';
import { create, Plugin, Jss } from 'jss';
import rtl from 'jss-rtl';
import {
  jssPreset,
  StylesProvider
} from '@material-ui/styles';
import themeOptions from '../src/theme/themeOptions';
import RtlProvider from '../src/theme/RtlProvider';


const jss = create({ plugins: [...jssPreset().plugins, rtl()] as Plugin[] });
const theme = createMuiTheme({ ...themeOptions, direction: "rtl" });

addDecorator(withKnobs);
addDecorator((StoryFn:StoryFn<StoryFnReactReturnType>) => {
  return (
    <body dir={theme.direction}>
      <StylesProvider jss={jss as Jss}>
      <MuiThemeProvider theme={theme}>
      <SCThemeProvider  theme={theme}>
       <RtlProvider direction={theme.direction}>
        {StoryFn()}
       </RtlProvider>
      </SCThemeProvider>
      </MuiThemeProvider>
      </StylesProvider>
     </body>
  );
});

const ordering = ["Application", "Atoms"];
const getDirectoryDepth = (path: string) => path.match(/\//g)?.length ?? 0;
// Option defaults.
addParameters({
  options: {
    theme: themes.light,
    storySort: ([, a], [, b]) => {
      if (a.kind === b.kind) {
        return 0;
      }
      // order root categories
      const aKind = a.kind.match(/(\w*)(\s*)|/)[1];
      const bKind = b.kind.match(/(\w*)(\s*)|/)[1];
      if (aKind !== bKind) {
        return ordering.indexOf(aKind) - ordering.indexOf(bKind);
      }
      // this sorts folders on top of everything
      if (getDirectoryDepth(a.kind) > getDirectoryDepth(b.kind)) {
        return -1;
      }
      return a.kind.localeCompare(b.kind);
    },
  },
});